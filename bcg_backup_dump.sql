--
-- PostgreSQL database dump
--

-- Dumped from database version 10.14 (Ubuntu 10.14-0ubuntu0.18.04.1)
-- Dumped by pg_dump version 12.8 (Ubuntu 12.8-0ubuntu0.20.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

--
-- Name: api_customer; Type: TABLE; Schema: public; Owner: narayanac
--

CREATE TABLE public.api_customer (
    id bigint NOT NULL,
    customer_id character varying(255) NOT NULL,
    gender character varying(255) NOT NULL,
    region character varying(255) NOT NULL,
    income_group character varying(255) NOT NULL,
    marital_status boolean NOT NULL,
    created_on timestamp with time zone NOT NULL
);


ALTER TABLE public.api_customer OWNER TO narayanac;

--
-- Name: api_customer_id_seq; Type: SEQUENCE; Schema: public; Owner: narayanac
--

CREATE SEQUENCE public.api_customer_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.api_customer_id_seq OWNER TO narayanac;

--
-- Name: api_customer_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: narayanac
--

ALTER SEQUENCE public.api_customer_id_seq OWNED BY public.api_customer.id;


--
-- Name: api_customerinsurancedata; Type: TABLE; Schema: public; Owner: narayanac
--

CREATE TABLE public.api_customerinsurancedata (
    id bigint NOT NULL,
    policy_id character varying(255) NOT NULL,
    fuel character varying(255) NOT NULL,
    vehicle_segment character varying(255) NOT NULL,
    premium integer NOT NULL,
    bodily_injury_liability boolean NOT NULL,
    personal_injury_protection boolean NOT NULL,
    purchase_date date NOT NULL,
    created_on timestamp with time zone NOT NULL,
    customer_id bigint NOT NULL,
    collision boolean NOT NULL,
    comprehensive boolean NOT NULL,
    property_damage_liability boolean NOT NULL
);


ALTER TABLE public.api_customerinsurancedata OWNER TO narayanac;

--
-- Name: api_customerinsurancedata_id_seq; Type: SEQUENCE; Schema: public; Owner: narayanac
--

CREATE SEQUENCE public.api_customerinsurancedata_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.api_customerinsurancedata_id_seq OWNER TO narayanac;

--
-- Name: api_customerinsurancedata_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: narayanac
--

ALTER SEQUENCE public.api_customerinsurancedata_id_seq OWNED BY public.api_customerinsurancedata.id;


--
-- Name: auth_group; Type: TABLE; Schema: public; Owner: narayanac
--

CREATE TABLE public.auth_group (
    id integer NOT NULL,
    name character varying(150) NOT NULL
);


ALTER TABLE public.auth_group OWNER TO narayanac;

--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: narayanac
--

CREATE SEQUENCE public.auth_group_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_id_seq OWNER TO narayanac;

--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: narayanac
--

ALTER SEQUENCE public.auth_group_id_seq OWNED BY public.auth_group.id;


--
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: narayanac
--

CREATE TABLE public.auth_group_permissions (
    id bigint NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_group_permissions OWNER TO narayanac;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: narayanac
--

CREATE SEQUENCE public.auth_group_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_permissions_id_seq OWNER TO narayanac;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: narayanac
--

ALTER SEQUENCE public.auth_group_permissions_id_seq OWNED BY public.auth_group_permissions.id;


--
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: narayanac
--

CREATE TABLE public.auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE public.auth_permission OWNER TO narayanac;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: narayanac
--

CREATE SEQUENCE public.auth_permission_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_permission_id_seq OWNER TO narayanac;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: narayanac
--

ALTER SEQUENCE public.auth_permission_id_seq OWNED BY public.auth_permission.id;


--
-- Name: auth_user; Type: TABLE; Schema: public; Owner: narayanac
--

CREATE TABLE public.auth_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    username character varying(150) NOT NULL,
    first_name character varying(150) NOT NULL,
    last_name character varying(150) NOT NULL,
    email character varying(254) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL
);


ALTER TABLE public.auth_user OWNER TO narayanac;

--
-- Name: auth_user_groups; Type: TABLE; Schema: public; Owner: narayanac
--

CREATE TABLE public.auth_user_groups (
    id bigint NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE public.auth_user_groups OWNER TO narayanac;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: narayanac
--

CREATE SEQUENCE public.auth_user_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_groups_id_seq OWNER TO narayanac;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: narayanac
--

ALTER SEQUENCE public.auth_user_groups_id_seq OWNED BY public.auth_user_groups.id;


--
-- Name: auth_user_id_seq; Type: SEQUENCE; Schema: public; Owner: narayanac
--

CREATE SEQUENCE public.auth_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_id_seq OWNER TO narayanac;

--
-- Name: auth_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: narayanac
--

ALTER SEQUENCE public.auth_user_id_seq OWNED BY public.auth_user.id;


--
-- Name: auth_user_user_permissions; Type: TABLE; Schema: public; Owner: narayanac
--

CREATE TABLE public.auth_user_user_permissions (
    id bigint NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_user_user_permissions OWNER TO narayanac;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: narayanac
--

CREATE SEQUENCE public.auth_user_user_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_user_permissions_id_seq OWNER TO narayanac;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: narayanac
--

ALTER SEQUENCE public.auth_user_user_permissions_id_seq OWNED BY public.auth_user_user_permissions.id;


--
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: narayanac
--

CREATE TABLE public.django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE public.django_admin_log OWNER TO narayanac;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: narayanac
--

CREATE SEQUENCE public.django_admin_log_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_admin_log_id_seq OWNER TO narayanac;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: narayanac
--

ALTER SEQUENCE public.django_admin_log_id_seq OWNED BY public.django_admin_log.id;


--
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: narayanac
--

CREATE TABLE public.django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE public.django_content_type OWNER TO narayanac;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: narayanac
--

CREATE SEQUENCE public.django_content_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_content_type_id_seq OWNER TO narayanac;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: narayanac
--

ALTER SEQUENCE public.django_content_type_id_seq OWNED BY public.django_content_type.id;


--
-- Name: django_migrations; Type: TABLE; Schema: public; Owner: narayanac
--

CREATE TABLE public.django_migrations (
    id bigint NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE public.django_migrations OWNER TO narayanac;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: narayanac
--

CREATE SEQUENCE public.django_migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_migrations_id_seq OWNER TO narayanac;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: narayanac
--

ALTER SEQUENCE public.django_migrations_id_seq OWNED BY public.django_migrations.id;


--
-- Name: django_session; Type: TABLE; Schema: public; Owner: narayanac
--

CREATE TABLE public.django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE public.django_session OWNER TO narayanac;

--
-- Name: api_customer id; Type: DEFAULT; Schema: public; Owner: narayanac
--

ALTER TABLE ONLY public.api_customer ALTER COLUMN id SET DEFAULT nextval('public.api_customer_id_seq'::regclass);


--
-- Name: api_customerinsurancedata id; Type: DEFAULT; Schema: public; Owner: narayanac
--

ALTER TABLE ONLY public.api_customerinsurancedata ALTER COLUMN id SET DEFAULT nextval('public.api_customerinsurancedata_id_seq'::regclass);


--
-- Name: auth_group id; Type: DEFAULT; Schema: public; Owner: narayanac
--

ALTER TABLE ONLY public.auth_group ALTER COLUMN id SET DEFAULT nextval('public.auth_group_id_seq'::regclass);


--
-- Name: auth_group_permissions id; Type: DEFAULT; Schema: public; Owner: narayanac
--

ALTER TABLE ONLY public.auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_group_permissions_id_seq'::regclass);


--
-- Name: auth_permission id; Type: DEFAULT; Schema: public; Owner: narayanac
--

ALTER TABLE ONLY public.auth_permission ALTER COLUMN id SET DEFAULT nextval('public.auth_permission_id_seq'::regclass);


--
-- Name: auth_user id; Type: DEFAULT; Schema: public; Owner: narayanac
--

ALTER TABLE ONLY public.auth_user ALTER COLUMN id SET DEFAULT nextval('public.auth_user_id_seq'::regclass);


--
-- Name: auth_user_groups id; Type: DEFAULT; Schema: public; Owner: narayanac
--

ALTER TABLE ONLY public.auth_user_groups ALTER COLUMN id SET DEFAULT nextval('public.auth_user_groups_id_seq'::regclass);


--
-- Name: auth_user_user_permissions id; Type: DEFAULT; Schema: public; Owner: narayanac
--

ALTER TABLE ONLY public.auth_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_user_user_permissions_id_seq'::regclass);


--
-- Name: django_admin_log id; Type: DEFAULT; Schema: public; Owner: narayanac
--

ALTER TABLE ONLY public.django_admin_log ALTER COLUMN id SET DEFAULT nextval('public.django_admin_log_id_seq'::regclass);


--
-- Name: django_content_type id; Type: DEFAULT; Schema: public; Owner: narayanac
--

ALTER TABLE ONLY public.django_content_type ALTER COLUMN id SET DEFAULT nextval('public.django_content_type_id_seq'::regclass);


--
-- Name: django_migrations id; Type: DEFAULT; Schema: public; Owner: narayanac
--

ALTER TABLE ONLY public.django_migrations ALTER COLUMN id SET DEFAULT nextval('public.django_migrations_id_seq'::regclass);


--
-- Data for Name: api_customer; Type: TABLE DATA; Schema: public; Owner: narayanac
--

COPY public.api_customer (id, customer_id, gender, region, income_group, marital_status, created_on) FROM stdin;
1431	400	Male	North	0- $25K	f	2021-09-16 09:15:45.626798+00
1432	401	Male	South	$25-$70K	t	2021-09-16 09:15:45.634103+00
1433	402	Male	East	>$70K	f	2021-09-16 09:15:45.638686+00
1434	403	Male	West	$25-$70K	t	2021-09-16 09:15:45.647939+00
1435	404	Male	North	$25-$70K	t	2021-09-16 09:15:45.662746+00
1436	405	Male	North	$25-$70K	f	2021-09-16 09:15:45.678968+00
1437	406	Male	North	$25-$70K	f	2021-09-16 09:15:45.692137+00
1438	407	Male	North	$25-$70K	f	2021-09-16 09:15:45.704946+00
1439	408	Male	North	$25-$70K	t	2021-09-16 09:15:45.712103+00
1440	409	Male	North	$25-$70K	f	2021-09-16 09:15:45.720295+00
1441	410	Male	North	$25-$70K	t	2021-09-16 09:15:45.726754+00
1442	411	Male	North	$25-$70K	f	2021-09-16 09:15:45.733146+00
1443	412	Male	North	$25-$70K	f	2021-09-16 09:15:45.739921+00
1444	413	Male	North	$25-$70K	f	2021-09-16 09:15:45.746088+00
1445	414	Male	North	$25-$70K	t	2021-09-16 09:15:45.754567+00
1446	415	Male	North	$25-$70K	f	2021-09-16 09:15:45.75996+00
1447	416	Male	North	$25-$70K	t	2021-09-16 09:15:45.764672+00
1448	417	Male	North	$25-$70K	f	2021-09-16 09:15:45.768869+00
1449	418	Male	North	$25-$70K	f	2021-09-16 09:15:45.773254+00
1450	419	Male	North	$25-$70K	f	2021-09-16 09:15:45.778711+00
1451	420	Male	North	$25-$70K	f	2021-09-16 09:15:45.783477+00
1452	755	Male	North	$25-$70K	f	2021-09-16 09:15:46.944616+00
1453	756	Male	North	$25-$70K	t	2021-09-16 09:15:46.950054+00
1454	757	Male	North	$25-$70K	f	2021-09-16 09:15:46.956232+00
1455	758	Male	North	$25-$70K	f	2021-09-16 09:15:46.97047+00
1456	759	Male	North	$25-$70K	f	2021-09-16 09:15:46.97966+00
1457	760	Male	North	$25-$70K	t	2021-09-16 09:15:46.98781+00
1458	761	Male	North	$25-$70K	f	2021-09-16 09:15:46.994064+00
1459	762	Male	North	$25-$70K	f	2021-09-16 09:15:46.999835+00
1460	763	Male	South	$25-$70K	t	2021-09-16 09:15:47.004593+00
1461	764	Male	South	$25-$70K	f	2021-09-16 09:15:47.008987+00
1462	765	Male	South	$25-$70K	f	2021-09-16 09:15:47.013722+00
1463	766	Male	South	$25-$70K	f	2021-09-16 09:15:47.019003+00
1464	767	Male	South	$25-$70K	t	2021-09-16 09:15:47.024001+00
1465	768	Male	South	$25-$70K	t	2021-09-16 09:15:47.02848+00
1466	769	Male	South	$25-$70K	t	2021-09-16 09:15:47.033042+00
1467	770	Male	South	$25-$70K	f	2021-09-16 09:15:47.03744+00
1468	771	Male	South	$25-$70K	f	2021-09-16 09:15:47.041996+00
1469	772	Male	South	$25-$70K	t	2021-09-16 09:15:47.046451+00
1470	773	Male	South	$25-$70K	f	2021-09-16 09:15:47.051971+00
1471	774	Male	South	$25-$70K	t	2021-09-16 09:15:47.056885+00
1472	775	Male	South	$25-$70K	t	2021-09-16 09:15:47.065114+00
1473	776	Male	South	$25-$70K	f	2021-09-16 09:15:47.073368+00
1474	777	Male	South	$25-$70K	f	2021-09-16 09:15:47.08194+00
1475	778	Male	South	$25-$70K	f	2021-09-16 09:15:47.088479+00
1476	779	Male	South	$25-$70K	t	2021-09-16 09:15:47.093231+00
1477	780	Male	South	$25-$70K	f	2021-09-16 09:15:47.097169+00
1478	781	Male	South	$25-$70K	t	2021-09-16 09:15:47.104291+00
1479	782	Male	South	$25-$70K	f	2021-09-16 09:15:47.110231+00
1480	783	Male	South	$25-$70K	f	2021-09-16 09:15:47.116157+00
1481	784	Male	South	$25-$70K	f	2021-09-16 09:15:47.121027+00
1482	785	Male	South	$25-$70K	t	2021-09-16 09:15:47.125097+00
1483	786	Male	South	$25-$70K	t	2021-09-16 09:15:47.129181+00
1484	787	Male	South	$25-$70K	f	2021-09-16 09:15:47.133951+00
1485	788	Male	South	$25-$70K	f	2021-09-16 09:15:47.140037+00
1486	789	Male	South	$25-$70K	f	2021-09-16 09:15:47.14797+00
1487	790	Male	South	$25-$70K	f	2021-09-16 09:15:47.153302+00
1488	791	Male	South	$25-$70K	t	2021-09-16 09:15:47.157937+00
1489	792	Male	South	$25-$70K	t	2021-09-16 09:15:47.163173+00
1490	793	Male	South	$25-$70K	t	2021-09-16 09:15:47.167841+00
1491	794	Male	South	$25-$70K	t	2021-09-16 09:15:47.172694+00
1492	795	Male	South	$25-$70K	t	2021-09-16 09:15:47.177825+00
1493	796	Male	South	$25-$70K	t	2021-09-16 09:15:47.182491+00
1494	797	Male	South	$25-$70K	t	2021-09-16 09:15:47.187758+00
1495	798	Male	South	$25-$70K	f	2021-09-16 09:15:47.192812+00
1496	799	Male	South	$25-$70K	t	2021-09-16 09:15:47.197406+00
1497	800	Male	South	$25-$70K	f	2021-09-16 09:15:47.203044+00
1498	801	Male	South	$25-$70K	t	2021-09-16 09:15:47.208139+00
1499	802	Male	South	$25-$70K	t	2021-09-16 09:15:47.213102+00
1500	803	Male	South	$25-$70K	f	2021-09-16 09:15:47.218194+00
1501	804	Male	South	$25-$70K	f	2021-09-16 09:15:47.222984+00
1502	805	Male	South	$25-$70K	t	2021-09-16 09:15:47.22759+00
1503	806	Male	South	$25-$70K	f	2021-09-16 09:15:47.232628+00
1504	807	Male	South	$25-$70K	f	2021-09-16 09:15:47.237506+00
1505	808	Male	South	$25-$70K	f	2021-09-16 09:15:47.242868+00
1506	809	Male	South	$25-$70K	f	2021-09-16 09:15:47.247888+00
1507	810	Male	South	$25-$70K	t	2021-09-16 09:15:47.25338+00
1508	811	Male	South	$25-$70K	f	2021-09-16 09:15:47.258032+00
1509	812	Male	South	$25-$70K	t	2021-09-16 09:15:47.262595+00
1510	813	Male	South	$25-$70K	t	2021-09-16 09:15:47.267894+00
1511	814	Male	South	$25-$70K	f	2021-09-16 09:15:47.273533+00
1512	815	Male	South	$25-$70K	f	2021-09-16 09:15:47.278029+00
1513	816	Male	South	$25-$70K	t	2021-09-16 09:15:47.282762+00
1514	817	Male	South	$25-$70K	f	2021-09-16 09:15:47.287635+00
1515	818	Male	South	$25-$70K	f	2021-09-16 09:15:47.292089+00
1516	819	Male	South	$25-$70K	t	2021-09-16 09:15:47.297164+00
1517	820	Male	South	$25-$70K	f	2021-09-16 09:15:47.30223+00
1518	821	Male	South	$25-$70K	f	2021-09-16 09:15:47.306688+00
1519	822	Male	South	$25-$70K	t	2021-09-16 09:15:47.310946+00
1520	823	Male	South	$25-$70K	t	2021-09-16 09:15:47.3153+00
1521	824	Male	South	$25-$70K	f	2021-09-16 09:15:47.319934+00
1522	825	Male	South	$25-$70K	f	2021-09-16 09:15:47.325062+00
1523	826	Male	South	$25-$70K	f	2021-09-16 09:15:47.329951+00
1524	827	Male	South	$25-$70K	f	2021-09-16 09:15:47.334829+00
1525	828	Male	South	$25-$70K	f	2021-09-16 09:15:47.339692+00
1526	829	Male	South	$25-$70K	f	2021-09-16 09:15:47.344318+00
1527	830	Male	South	$25-$70K	t	2021-09-16 09:15:47.349071+00
1528	831	Male	South	$25-$70K	t	2021-09-16 09:15:47.354377+00
1529	832	Male	South	$25-$70K	f	2021-09-16 09:15:47.359465+00
1530	833	Male	South	$25-$70K	f	2021-09-16 09:15:47.364828+00
1531	834	Male	South	$25-$70K	t	2021-09-16 09:15:47.369857+00
1532	835	Male	South	$25-$70K	f	2021-09-16 09:15:47.377684+00
1533	836	Male	South	$25-$70K	f	2021-09-16 09:15:47.384808+00
1534	837	Male	South	$25-$70K	f	2021-09-16 09:15:47.392266+00
1535	838	Male	South	$25-$70K	f	2021-09-16 09:15:47.398415+00
1536	839	Male	South	$25-$70K	t	2021-09-16 09:15:47.403673+00
1537	840	Male	South	$25-$70K	f	2021-09-16 09:15:47.408698+00
1538	995	Male	South	>$70K	f	2021-09-16 09:15:48.145513+00
1539	996	Male	South	>$70K	t	2021-09-16 09:15:48.150468+00
1540	997	Male	South	>$70K	t	2021-09-16 09:15:48.156119+00
1541	998	Male	South	>$70K	f	2021-09-16 09:15:48.161356+00
1542	999	Male	South	>$70K	f	2021-09-16 09:15:48.167046+00
1543	1000	Male	South	>$70K	t	2021-09-16 09:15:48.172213+00
1544	1001	Male	South	>$70K	t	2021-09-16 09:15:48.176853+00
1545	1002	Male	South	>$70K	t	2021-09-16 09:15:48.181625+00
1546	1003	Male	South	>$70K	t	2021-09-16 09:15:48.186638+00
1547	1004	Male	South	>$70K	f	2021-09-16 09:15:48.191463+00
1548	1005	Male	South	>$70K	f	2021-09-16 09:15:48.196541+00
1549	1006	Male	South	>$70K	f	2021-09-16 09:15:48.201632+00
1550	1007	Male	South	>$70K	t	2021-09-16 09:15:48.206588+00
1551	1008	Male	South	>$70K	f	2021-09-16 09:15:48.211638+00
1552	1009	Male	South	>$70K	t	2021-09-16 09:15:48.216807+00
1553	1010	Male	South	>$70K	t	2021-09-16 09:15:48.221322+00
1554	1011	Male	South	>$70K	t	2021-09-16 09:15:48.226017+00
1555	1012	Male	South	>$70K	t	2021-09-16 09:15:48.231228+00
1556	1013	Male	South	>$70K	f	2021-09-16 09:15:48.23599+00
1557	1014	Male	South	>$70K	f	2021-09-16 09:15:48.240643+00
1558	1015	Male	South	>$70K	f	2021-09-16 09:15:48.245385+00
1559	1016	Male	South	>$70K	t	2021-09-16 09:15:48.250316+00
1560	1017	Male	South	>$70K	t	2021-09-16 09:15:48.254832+00
1561	1018	Male	South	>$70K	t	2021-09-16 09:15:48.259397+00
1562	1019	Male	South	>$70K	t	2021-09-16 09:15:48.263796+00
1563	1020	Male	South	>$70K	f	2021-09-16 09:15:48.268219+00
1564	1021	Male	South	>$70K	t	2021-09-16 09:15:48.272572+00
1565	1022	Male	South	>$70K	f	2021-09-16 09:15:48.276953+00
1566	1023	Male	South	>$70K	f	2021-09-16 09:15:48.281587+00
1567	1024	Male	South	>$70K	t	2021-09-16 09:15:48.285951+00
1568	1025	Male	South	>$70K	f	2021-09-16 09:15:48.290305+00
1569	1026	Male	South	>$70K	t	2021-09-16 09:15:48.294768+00
1570	1027	Male	South	>$70K	f	2021-09-16 09:15:48.299287+00
1571	1028	Male	South	>$70K	t	2021-09-16 09:15:48.303912+00
1572	1029	Male	South	>$70K	t	2021-09-16 09:15:48.308273+00
1573	1030	Male	South	>$70K	t	2021-09-16 09:15:48.312799+00
1574	1031	Male	South	>$70K	f	2021-09-16 09:15:48.317816+00
1575	1032	Male	South	>$70K	f	2021-09-16 09:15:48.322394+00
1576	1033	Male	South	>$70K	f	2021-09-16 09:15:48.327135+00
1577	1034	Male	South	>$70K	f	2021-09-16 09:15:48.332259+00
1578	1035	Male	South	>$70K	f	2021-09-16 09:15:48.336531+00
1579	1036	Male	South	>$70K	t	2021-09-16 09:15:48.34046+00
1580	1037	Male	South	>$70K	t	2021-09-16 09:15:48.344897+00
1581	1038	Male	South	>$70K	f	2021-09-16 09:15:48.349271+00
1582	1039	Male	South	>$70K	t	2021-09-16 09:15:48.35363+00
1583	1040	Male	South	>$70K	t	2021-09-16 09:15:48.35761+00
1584	1041	Male	South	>$70K	t	2021-09-16 09:15:48.36175+00
1585	1042	Male	South	>$70K	f	2021-09-16 09:15:48.367059+00
1586	1043	Male	South	$25-$70K	t	2021-09-16 09:15:48.371125+00
1587	1044	Male	South	$25-$70K	f	2021-09-16 09:15:48.375441+00
1588	1045	Male	South	$25-$70K	f	2021-09-16 09:15:48.379581+00
1589	1046	Male	South	$25-$70K	t	2021-09-16 09:15:48.383917+00
1590	1047	Male	South	$25-$70K	t	2021-09-16 09:15:48.387984+00
1591	1048	Male	South	$25-$70K	t	2021-09-16 09:15:48.392178+00
1592	1049	Male	South	$25-$70K	f	2021-09-16 09:15:48.396989+00
1593	1050	Male	South	$25-$70K	f	2021-09-16 09:15:48.402082+00
1594	1051	Male	South	$25-$70K	f	2021-09-16 09:15:48.407142+00
1595	1052	Male	South	$25-$70K	t	2021-09-16 09:15:48.413635+00
1596	1053	Male	South	$25-$70K	f	2021-09-16 09:15:48.418577+00
1597	1054	Male	South	$25-$70K	t	2021-09-16 09:15:48.42377+00
1598	1055	Male	South	$25-$70K	t	2021-09-16 09:15:48.428964+00
1599	1056	Male	South	$25-$70K	t	2021-09-16 09:15:48.435898+00
1600	1057	Male	South	$25-$70K	f	2021-09-16 09:15:48.441911+00
1601	1058	Male	South	$25-$70K	t	2021-09-16 09:15:48.44825+00
1602	1059	Male	South	$25-$70K	f	2021-09-16 09:15:48.455039+00
1603	1060	Male	South	$25-$70K	t	2021-09-16 09:15:48.460399+00
1604	1061	Male	South	$25-$70K	t	2021-09-16 09:15:48.467253+00
1605	1062	Male	South	$25-$70K	f	2021-09-16 09:15:48.47317+00
1606	1063	Male	South	$25-$70K	t	2021-09-16 09:15:48.482153+00
1607	1064	Male	South	$25-$70K	t	2021-09-16 09:15:48.489323+00
1608	1065	Male	South	$25-$70K	f	2021-09-16 09:15:48.503584+00
1609	1066	Male	South	$25-$70K	f	2021-09-16 09:15:48.509433+00
1610	1067	Male	South	$25-$70K	f	2021-09-16 09:15:48.515804+00
1611	1068	Male	South	$25-$70K	f	2021-09-16 09:15:48.523665+00
1612	1069	Male	South	$25-$70K	f	2021-09-16 09:15:48.530132+00
1613	1070	Male	South	$25-$70K	f	2021-09-16 09:15:48.539805+00
1614	1071	Male	South	$25-$70K	f	2021-09-16 09:15:48.545827+00
1615	1072	Male	South	$25-$70K	f	2021-09-16 09:15:48.552116+00
1616	1073	Male	South	$25-$70K	f	2021-09-16 09:15:48.557855+00
1617	1074	Male	South	$25-$70K	t	2021-09-16 09:15:48.562841+00
1618	1075	Male	South	$25-$70K	t	2021-09-16 09:15:48.568305+00
1619	1076	Male	South	$25-$70K	t	2021-09-16 09:15:48.573952+00
1620	1077	Male	South	$25-$70K	t	2021-09-16 09:15:48.580471+00
1621	1078	Male	South	$25-$70K	f	2021-09-16 09:15:48.585868+00
1622	1079	Male	South	$25-$70K	t	2021-09-16 09:15:48.591347+00
1623	1080	Male	South	$25-$70K	f	2021-09-16 09:15:48.596535+00
1624	1081	Male	South	$25-$70K	t	2021-09-16 09:15:48.601758+00
1625	1082	Male	South	$25-$70K	f	2021-09-16 09:15:48.607316+00
1626	1083	Male	South	$25-$70K	f	2021-09-16 09:15:48.613713+00
1627	1084	Male	South	$25-$70K	f	2021-09-16 09:15:48.619464+00
1628	1085	Male	South	$25-$70K	f	2021-09-16 09:15:48.625128+00
1629	1086	Male	South	$25-$70K	f	2021-09-16 09:15:48.630723+00
1630	1087	Male	South	$25-$70K	f	2021-09-16 09:15:48.635944+00
1631	1088	Male	South	$25-$70K	f	2021-09-16 09:15:48.641308+00
1632	1089	Male	South	$25-$70K	t	2021-09-16 09:15:48.647592+00
1633	1090	Male	South	$25-$70K	f	2021-09-16 09:15:48.653612+00
1634	1091	Male	South	$25-$70K	f	2021-09-16 09:15:48.659082+00
1635	1092	Male	South	$25-$70K	f	2021-09-16 09:15:48.665032+00
1636	1093	Male	South	$25-$70K	t	2021-09-16 09:15:48.671099+00
1637	1094	Male	South	$25-$70K	t	2021-09-16 09:15:48.676107+00
1638	1095	Male	South	$25-$70K	f	2021-09-16 09:15:48.681923+00
1639	1096	Male	South	$25-$70K	t	2021-09-16 09:15:48.687846+00
1640	1097	Male	South	$25-$70K	t	2021-09-16 09:15:48.693494+00
1641	1098	Male	South	$25-$70K	f	2021-09-16 09:15:48.699001+00
1642	1099	Male	South	$25-$70K	t	2021-09-16 09:15:48.704053+00
1643	1100	Male	South	$25-$70K	t	2021-09-16 09:15:48.70931+00
1644	1101	Male	South	$25-$70K	f	2021-09-16 09:15:48.714717+00
1645	1102	Male	South	$25-$70K	f	2021-09-16 09:15:48.719536+00
1646	1103	Male	South	$25-$70K	t	2021-09-16 09:15:48.723936+00
1647	1104	Male	South	$25-$70K	f	2021-09-16 09:15:48.728962+00
1648	1105	Male	South	$25-$70K	f	2021-09-16 09:15:48.733618+00
1649	1106	Male	South	$25-$70K	f	2021-09-16 09:15:48.73864+00
1650	1107	Male	South	$25-$70K	t	2021-09-16 09:15:48.743011+00
1651	1108	Male	South	$25-$70K	t	2021-09-16 09:15:48.747875+00
1652	1109	Male	South	$25-$70K	t	2021-09-16 09:15:48.753609+00
1653	1110	Male	South	$25-$70K	f	2021-09-16 09:15:48.759349+00
1654	1111	Male	South	$25-$70K	f	2021-09-16 09:15:48.765329+00
1655	1112	Male	South	$25-$70K	t	2021-09-16 09:15:48.771573+00
1656	1113	Male	South	$25-$70K	f	2021-09-16 09:15:48.777705+00
1657	1114	Male	South	$25-$70K	f	2021-09-16 09:15:48.786357+00
1658	1115	Male	South	$25-$70K	t	2021-09-16 09:15:48.792177+00
1659	1116	Male	South	$25-$70K	t	2021-09-16 09:15:48.799254+00
1660	1117	Male	South	$25-$70K	f	2021-09-16 09:15:48.805985+00
1661	1118	Male	South	$25-$70K	f	2021-09-16 09:15:48.813399+00
1662	1119	Male	South	$25-$70K	f	2021-09-16 09:15:48.818959+00
1663	1120	Male	South	$25-$70K	f	2021-09-16 09:15:48.824067+00
1664	1121	Male	South	$25-$70K	f	2021-09-16 09:15:48.830376+00
1665	1122	Male	South	$25-$70K	t	2021-09-16 09:15:48.836135+00
1666	1123	Male	South	$25-$70K	f	2021-09-16 09:15:48.847806+00
1667	1124	Male	South	$25-$70K	f	2021-09-16 09:15:48.862176+00
1668	1125	Male	South	$25-$70K	f	2021-09-16 09:15:48.87922+00
1669	1126	Male	South	$25-$70K	t	2021-09-16 09:15:48.887585+00
1670	1127	Male	South	$25-$70K	t	2021-09-16 09:15:48.896536+00
1671	1128	Male	East	$25-$70K	f	2021-09-16 09:15:48.905644+00
1672	1129	Male	East	$25-$70K	f	2021-09-16 09:15:48.914476+00
1673	1130	Male	East	$25-$70K	f	2021-09-16 09:15:48.924293+00
1674	1131	Male	East	$25-$70K	t	2021-09-16 09:15:48.934415+00
1675	1132	Male	East	$25-$70K	f	2021-09-16 09:15:48.942973+00
1676	1133	Male	East	$25-$70K	t	2021-09-16 09:15:48.951242+00
1677	1134	Male	East	$25-$70K	f	2021-09-16 09:15:48.96004+00
1678	1135	Male	East	$25-$70K	t	2021-09-16 09:15:48.967979+00
1679	1136	Male	East	$25-$70K	t	2021-09-16 09:15:48.975932+00
1680	1137	Male	East	$25-$70K	t	2021-09-16 09:15:48.982686+00
1681	1138	Male	East	$25-$70K	t	2021-09-16 09:15:48.989583+00
1682	1139	Male	East	$25-$70K	f	2021-09-16 09:15:48.996726+00
1683	1140	Male	East	$25-$70K	t	2021-09-16 09:15:49.003754+00
1684	1141	Male	East	$25-$70K	t	2021-09-16 09:15:49.010624+00
1685	1142	Male	East	$25-$70K	t	2021-09-16 09:15:49.017781+00
1686	1143	Male	East	$25-$70K	t	2021-09-16 09:15:49.024843+00
1687	1144	Male	East	$25-$70K	f	2021-09-16 09:15:49.031957+00
1688	1145	Male	East	$25-$70K	t	2021-09-16 09:15:49.038701+00
1689	1146	Male	East	$25-$70K	t	2021-09-16 09:15:49.045836+00
1690	1147	Male	East	$25-$70K	f	2021-09-16 09:15:49.053044+00
1691	1148	Male	East	$25-$70K	t	2021-09-16 09:15:49.06134+00
1692	1149	Male	East	$25-$70K	t	2021-09-16 09:15:49.068501+00
1693	1150	Male	East	$25-$70K	f	2021-09-16 09:15:49.074846+00
1694	1151	Male	East	$25-$70K	t	2021-09-16 09:15:49.082835+00
1695	1152	Male	East	$25-$70K	t	2021-09-16 09:15:49.090741+00
1696	1153	Male	East	$25-$70K	f	2021-09-16 09:15:49.096983+00
1697	1154	Male	East	$25-$70K	f	2021-09-16 09:15:49.102252+00
1698	1155	Male	East	$25-$70K	f	2021-09-16 09:15:49.107068+00
1699	1156	Male	East	$25-$70K	t	2021-09-16 09:15:49.111863+00
1700	1157	Male	East	$25-$70K	t	2021-09-16 09:15:49.116603+00
1701	1158	Male	East	$25-$70K	t	2021-09-16 09:15:49.123029+00
1702	1159	Male	East	$25-$70K	f	2021-09-16 09:15:49.130732+00
1703	1160	Male	East	$25-$70K	t	2021-09-16 09:15:49.136281+00
1704	1161	Male	East	$25-$70K	f	2021-09-16 09:15:49.141945+00
1705	1162	Male	East	$25-$70K	t	2021-09-16 09:15:49.148005+00
1706	1163	Male	East	$25-$70K	t	2021-09-16 09:15:49.153817+00
1707	1164	Male	East	$25-$70K	f	2021-09-16 09:15:49.158777+00
1708	1165	Male	East	$25-$70K	t	2021-09-16 09:15:49.16413+00
1709	1166	Male	East	$25-$70K	t	2021-09-16 09:15:49.169196+00
1710	1167	Male	East	0- $25K	f	2021-09-16 09:15:49.17531+00
1711	1168	Male	East	0- $25K	f	2021-09-16 09:15:49.179859+00
1712	1169	Male	East	0- $25K	t	2021-09-16 09:15:49.184537+00
1713	1170	Male	East	0- $25K	t	2021-09-16 09:15:49.189419+00
1714	1171	Male	East	0- $25K	t	2021-09-16 09:15:49.194402+00
1715	1172	Male	East	0- $25K	f	2021-09-16 09:15:49.198455+00
1716	1173	Male	East	0- $25K	t	2021-09-16 09:15:49.20242+00
1717	1174	Male	East	0- $25K	t	2021-09-16 09:15:49.206414+00
1718	1175	Male	East	0- $25K	f	2021-09-16 09:15:49.210503+00
1719	1176	Male	East	0- $25K	f	2021-09-16 09:15:49.214254+00
1720	1177	Male	East	0- $25K	f	2021-09-16 09:15:49.218343+00
1721	1178	Male	East	0- $25K	t	2021-09-16 09:15:49.223049+00
1722	1179	Male	East	0- $25K	t	2021-09-16 09:15:49.228033+00
1723	1180	Male	East	0- $25K	f	2021-09-16 09:15:49.231958+00
1724	1181	Male	East	0- $25K	t	2021-09-16 09:15:49.235765+00
1725	1182	Male	East	0- $25K	t	2021-09-16 09:15:49.239847+00
1726	1183	Male	East	0- $25K	f	2021-09-16 09:15:49.24355+00
1727	1184	Male	East	0- $25K	t	2021-09-16 09:15:49.24754+00
1728	1185	Male	East	0- $25K	f	2021-09-16 09:15:49.251387+00
1729	1186	Male	East	0- $25K	t	2021-09-16 09:15:49.255562+00
1730	1187	Male	East	0- $25K	t	2021-09-16 09:15:49.259412+00
1731	1188	Male	East	0- $25K	t	2021-09-16 09:15:49.266778+00
1732	1189	Male	East	0- $25K	f	2021-09-16 09:15:49.275102+00
1733	1190	Male	East	0- $25K	f	2021-09-16 09:15:49.281174+00
1734	1191	Male	East	0- $25K	t	2021-09-16 09:15:49.285978+00
1735	1192	Male	East	0- $25K	t	2021-09-16 09:15:49.290527+00
1736	1193	Male	East	0- $25K	f	2021-09-16 09:15:49.294551+00
1737	1194	Male	East	0- $25K	f	2021-09-16 09:15:49.298372+00
1738	1195	Male	East	0- $25K	f	2021-09-16 09:15:49.302318+00
1739	1196	Male	East	0- $25K	t	2021-09-16 09:15:49.306287+00
1740	1197	Male	East	0- $25K	t	2021-09-16 09:15:49.310057+00
1741	1198	Male	East	0- $25K	f	2021-09-16 09:15:49.31413+00
1742	1199	Male	East	0- $25K	f	2021-09-16 09:15:49.317968+00
1743	1200	Male	East	0- $25K	f	2021-09-16 09:15:49.323061+00
1744	1201	Male	East	0- $25K	t	2021-09-16 09:15:49.328338+00
1745	1202	Male	East	0- $25K	f	2021-09-16 09:15:49.33358+00
1746	1203	Male	East	0- $25K	f	2021-09-16 09:15:49.339112+00
1747	1204	Male	East	0- $25K	f	2021-09-16 09:15:49.349004+00
1748	1205	Male	East	0- $25K	t	2021-09-16 09:15:49.35389+00
1749	1206	Male	East	0- $25K	f	2021-09-16 09:15:49.35848+00
1750	1207	Male	East	0- $25K	f	2021-09-16 09:15:49.363029+00
1751	1208	Male	East	0- $25K	t	2021-09-16 09:15:49.367181+00
1752	1209	Male	East	0- $25K	f	2021-09-16 09:15:49.371605+00
1753	1210	Male	East	0- $25K	t	2021-09-16 09:15:49.375852+00
1754	1211	Male	East	0- $25K	t	2021-09-16 09:15:49.38018+00
1755	1212	Male	East	0- $25K	t	2021-09-16 09:15:49.384278+00
1756	1213	Male	East	0- $25K	f	2021-09-16 09:15:49.392111+00
1757	1214	Male	East	0- $25K	t	2021-09-16 09:15:49.400432+00
1758	1215	Male	East	0- $25K	t	2021-09-16 09:15:49.409686+00
1759	1216	Male	East	0- $25K	t	2021-09-16 09:15:49.418806+00
1760	1217	Male	East	0- $25K	f	2021-09-16 09:15:49.427599+00
1761	1218	Male	East	0- $25K	f	2021-09-16 09:15:49.437018+00
1762	1219	Male	East	0- $25K	t	2021-09-16 09:15:49.446124+00
1763	1220	Male	East	0- $25K	f	2021-09-16 09:15:49.456143+00
1764	1221	Male	East	0- $25K	f	2021-09-16 09:15:49.464397+00
1765	1222	Male	East	0- $25K	t	2021-09-16 09:15:49.470392+00
1766	1223	Male	East	0- $25K	f	2021-09-16 09:15:49.475825+00
1767	1224	Male	East	0- $25K	t	2021-09-16 09:15:49.480038+00
1768	1225	Male	East	0- $25K	t	2021-09-16 09:15:49.484122+00
1769	1226	Male	East	0- $25K	f	2021-09-16 09:15:49.487741+00
1770	1227	Male	East	0- $25K	t	2021-09-16 09:15:49.493835+00
1771	1228	Male	East	0- $25K	f	2021-09-16 09:15:49.500248+00
1772	1229	Male	East	0- $25K	f	2021-09-16 09:15:49.505173+00
1773	1230	Male	East	0- $25K	f	2021-09-16 09:15:49.509422+00
1774	1231	Male	East	0- $25K	t	2021-09-16 09:15:49.516499+00
1775	1232	Male	East	0- $25K	f	2021-09-16 09:15:49.522086+00
1776	1233	Male	East	0- $25K	f	2021-09-16 09:15:49.526686+00
1777	1234	Male	East	0- $25K	t	2021-09-16 09:15:49.530828+00
1778	1235	Male	East	0- $25K	t	2021-09-16 09:15:49.537465+00
1779	1236	Male	East	0- $25K	t	2021-09-16 09:15:49.541341+00
1780	1237	Male	East	0- $25K	t	2021-09-16 09:15:49.544944+00
1781	1238	Male	East	0- $25K	t	2021-09-16 09:15:49.549236+00
1782	1239	Male	East	0- $25K	f	2021-09-16 09:15:49.558112+00
1783	1240	Male	East	0- $25K	f	2021-09-16 09:15:49.567008+00
1784	1241	Male	East	0- $25K	t	2021-09-16 09:15:49.575305+00
1785	1242	Male	East	0- $25K	t	2021-09-16 09:15:49.583418+00
1786	1243	Male	East	0- $25K	f	2021-09-16 09:15:49.591874+00
1787	1244	Male	East	0- $25K	f	2021-09-16 09:15:49.600372+00
1788	1245	Male	East	0- $25K	t	2021-09-16 09:15:49.609024+00
1789	1246	Male	East	0- $25K	t	2021-09-16 09:15:49.617802+00
1790	1247	Male	East	0- $25K	t	2021-09-16 09:15:49.625417+00
1791	1248	Male	East	0- $25K	t	2021-09-16 09:15:49.631369+00
1792	1249	Male	East	0- $25K	f	2021-09-16 09:15:49.636692+00
1793	1250	Male	East	0- $25K	t	2021-09-16 09:15:49.641193+00
1794	1251	Male	East	0- $25K	t	2021-09-16 09:15:49.645615+00
1795	1252	Male	East	0- $25K	t	2021-09-16 09:15:49.64992+00
1796	1253	Male	East	0- $25K	f	2021-09-16 09:15:49.65423+00
1797	1254	Male	East	0- $25K	f	2021-09-16 09:15:49.658836+00
1798	1255	Male	East	0- $25K	f	2021-09-16 09:15:49.663266+00
1799	1256	Male	East	0- $25K	t	2021-09-16 09:15:49.667498+00
1800	1257	Male	East	0- $25K	t	2021-09-16 09:15:49.672041+00
1801	1258	Male	East	0- $25K	f	2021-09-16 09:15:49.676474+00
1802	1259	Male	East	0- $25K	t	2021-09-16 09:15:49.680905+00
1803	1260	Male	East	0- $25K	t	2021-09-16 09:15:49.685101+00
1804	1261	Male	East	0- $25K	f	2021-09-16 09:15:49.68944+00
1805	1262	Male	East	0- $25K	t	2021-09-16 09:15:49.69369+00
1806	1263	Male	East	0- $25K	t	2021-09-16 09:15:49.698109+00
1807	1264	Male	East	0- $25K	t	2021-09-16 09:15:49.702323+00
1808	1265	Male	East	0- $25K	f	2021-09-16 09:15:49.706732+00
1809	1266	Male	East	0- $25K	t	2021-09-16 09:15:49.711097+00
1810	1267	Male	East	0- $25K	t	2021-09-16 09:15:49.715376+00
1811	1268	Male	East	0- $25K	t	2021-09-16 09:15:49.719673+00
1812	1269	Male	East	0- $25K	f	2021-09-16 09:15:49.724057+00
1813	1270	Male	East	0- $25K	f	2021-09-16 09:15:49.729198+00
1814	1271	Male	East	0- $25K	t	2021-09-16 09:15:49.734574+00
1815	1272	Male	East	0- $25K	f	2021-09-16 09:15:49.739097+00
1816	1273	Male	East	0- $25K	t	2021-09-16 09:15:49.74376+00
1817	1274	Male	East	0- $25K	f	2021-09-16 09:15:49.748029+00
1818	1275	Male	East	0- $25K	t	2021-09-16 09:15:49.752688+00
1819	1276	Male	East	0- $25K	t	2021-09-16 09:15:49.757037+00
1820	1277	Male	East	0- $25K	t	2021-09-16 09:15:49.761952+00
1821	1278	Male	East	0- $25K	t	2021-09-16 09:15:49.766377+00
1822	1279	Male	East	0- $25K	f	2021-09-16 09:15:49.77067+00
1823	1280	Male	East	0- $25K	t	2021-09-16 09:15:49.774903+00
1824	1281	Male	East	0- $25K	t	2021-09-16 09:15:49.77901+00
1825	1282	Male	East	0- $25K	f	2021-09-16 09:15:49.782955+00
1826	1283	Male	East	0- $25K	t	2021-09-16 09:15:49.787164+00
1827	1284	Male	East	0- $25K	f	2021-09-16 09:15:49.791284+00
1828	1285	Male	East	0- $25K	t	2021-09-16 09:15:49.795026+00
1829	1286	Male	East	0- $25K	f	2021-09-16 09:15:49.799163+00
1830	1287	Male	East	0- $25K	t	2021-09-16 09:15:49.803275+00
1831	1288	Male	East	0- $25K	f	2021-09-16 09:15:49.807116+00
1832	1289	Male	East	0- $25K	t	2021-09-16 09:15:49.811418+00
1833	1290	Male	East	0- $25K	t	2021-09-16 09:15:49.815496+00
1834	1291	Male	East	0- $25K	t	2021-09-16 09:15:49.819957+00
1835	1292	Male	East	0- $25K	t	2021-09-16 09:15:49.824388+00
1836	1293	Male	East	0- $25K	t	2021-09-16 09:15:49.829295+00
1837	1294	Male	East	0- $25K	f	2021-09-16 09:15:49.838241+00
1838	1295	Male	East	0- $25K	t	2021-09-16 09:15:49.847268+00
1839	1296	Male	East	0- $25K	t	2021-09-16 09:15:49.856636+00
1840	1297	Male	East	0- $25K	f	2021-09-16 09:15:49.865054+00
1841	1298	Male	East	0- $25K	f	2021-09-16 09:15:49.873422+00
1842	1299	Male	East	0- $25K	f	2021-09-16 09:15:49.880133+00
1843	1300	Male	East	0- $25K	t	2021-09-16 09:15:49.885309+00
1844	1301	Male	East	0- $25K	f	2021-09-16 09:15:49.890454+00
1845	1302	Male	East	0- $25K	f	2021-09-16 09:15:49.895479+00
1846	1303	Male	East	0- $25K	f	2021-09-16 09:15:49.90065+00
1847	1304	Male	East	0- $25K	f	2021-09-16 09:15:49.90832+00
1848	1305	Male	East	0- $25K	t	2021-09-16 09:15:49.912701+00
1849	1306	Male	East	0- $25K	t	2021-09-16 09:15:49.923261+00
1850	1307	Female	East	0- $25K	f	2021-09-16 09:15:49.932422+00
1851	1308	Female	East	0- $25K	t	2021-09-16 09:15:49.941952+00
1852	1309	Female	East	0- $25K	t	2021-09-16 09:15:49.950305+00
1853	1310	Female	East	0- $25K	t	2021-09-16 09:15:49.95886+00
1854	1311	Female	East	0- $25K	f	2021-09-16 09:15:49.967556+00
1855	1312	Female	East	0- $25K	t	2021-09-16 09:15:49.980882+00
1856	1313	Female	East	0- $25K	f	2021-09-16 09:15:49.989897+00
1857	1314	Female	East	0- $25K	t	2021-09-16 09:15:49.999112+00
1858	1315	Female	East	0- $25K	f	2021-09-16 09:15:50.008927+00
1859	1316	Female	East	0- $25K	t	2021-09-16 09:15:50.01786+00
1860	1317	Female	East	0- $25K	f	2021-09-16 09:15:50.027301+00
1861	1318	Female	East	0- $25K	t	2021-09-16 09:15:50.036887+00
1862	1319	Female	East	0- $25K	f	2021-09-16 09:15:50.044805+00
1863	1320	Female	East	0- $25K	f	2021-09-16 09:15:50.050382+00
1864	1321	Female	East	0- $25K	f	2021-09-16 09:15:50.059977+00
1865	1322	Female	East	0- $25K	f	2021-09-16 09:15:50.065418+00
1866	1323	Female	East	0- $25K	f	2021-09-16 09:15:50.070899+00
1867	1324	Female	East	0- $25K	t	2021-09-16 09:15:50.07608+00
1868	1325	Female	East	0- $25K	f	2021-09-16 09:15:50.081227+00
1869	1326	Female	East	0- $25K	f	2021-09-16 09:15:50.086304+00
1870	1327	Female	East	0- $25K	t	2021-09-16 09:15:50.091503+00
1871	1328	Female	East	0- $25K	t	2021-09-16 09:15:50.096591+00
1872	1329	Female	East	0- $25K	t	2021-09-16 09:15:50.101661+00
1873	1330	Female	East	0- $25K	f	2021-09-16 09:15:50.106449+00
1874	1331	Female	East	0- $25K	t	2021-09-16 09:15:50.110946+00
1875	1332	Female	East	0- $25K	f	2021-09-16 09:15:50.114948+00
1876	1333	Female	East	0- $25K	f	2021-09-16 09:15:50.118841+00
1877	1334	Female	East	0- $25K	t	2021-09-16 09:15:50.122933+00
1878	1335	Female	East	0- $25K	t	2021-09-16 09:15:50.129045+00
1879	1336	Female	East	0- $25K	t	2021-09-16 09:15:50.136251+00
1880	1337	Female	East	0- $25K	f	2021-09-16 09:15:50.144878+00
1881	1338	Female	East	0- $25K	f	2021-09-16 09:15:50.153919+00
1882	1339	Female	East	0- $25K	t	2021-09-16 09:15:50.163515+00
1883	1340	Female	East	0- $25K	t	2021-09-16 09:15:50.172439+00
1884	1341	Female	East	0- $25K	t	2021-09-16 09:15:50.181311+00
1885	1342	Female	East	0- $25K	f	2021-09-16 09:15:50.190282+00
1886	1343	Female	East	0- $25K	f	2021-09-16 09:15:50.199507+00
1887	1344	Female	East	0- $25K	t	2021-09-16 09:15:50.208619+00
1888	1345	Female	East	0- $25K	t	2021-09-16 09:15:50.217692+00
1889	1346	Female	East	0- $25K	f	2021-09-16 09:15:50.226813+00
1890	1347	Female	East	0- $25K	f	2021-09-16 09:15:50.236957+00
1891	1348	Female	East	0- $25K	f	2021-09-16 09:15:50.244936+00
1892	1349	Female	East	0- $25K	t	2021-09-16 09:15:50.250797+00
1893	1350	Female	East	0- $25K	t	2021-09-16 09:15:50.255155+00
1894	1351	Female	East	0- $25K	t	2021-09-16 09:15:50.259348+00
1895	1352	Female	East	0- $25K	f	2021-09-16 09:15:50.263482+00
1896	1353	Female	East	0- $25K	t	2021-09-16 09:15:50.267602+00
1897	1354	Female	East	0- $25K	f	2021-09-16 09:15:50.27162+00
1898	1355	Female	East	0- $25K	t	2021-09-16 09:15:50.275746+00
1899	1356	Female	East	0- $25K	t	2021-09-16 09:15:50.279788+00
1900	1357	Female	East	0- $25K	f	2021-09-16 09:15:50.283912+00
1901	1358	Female	East	0- $25K	t	2021-09-16 09:15:50.288064+00
1902	1359	Female	East	0- $25K	t	2021-09-16 09:15:50.292648+00
1903	1360	Female	East	0- $25K	f	2021-09-16 09:15:50.299177+00
1904	1361	Female	East	0- $25K	f	2021-09-16 09:15:50.30809+00
1905	1362	Female	East	0- $25K	f	2021-09-16 09:15:50.316769+00
1906	1363	Female	East	0- $25K	t	2021-09-16 09:15:50.326019+00
1907	1364	Female	East	0- $25K	f	2021-09-16 09:15:50.334058+00
1908	1365	Female	East	0- $25K	t	2021-09-16 09:15:50.34268+00
1909	1366	Female	East	0- $25K	t	2021-09-16 09:15:50.349429+00
1910	1367	Female	East	0- $25K	t	2021-09-16 09:15:50.3542+00
1911	1368	Female	East	0- $25K	f	2021-09-16 09:15:50.361264+00
1912	1369	Female	East	0- $25K	f	2021-09-16 09:15:50.366867+00
1913	1370	Female	East	0- $25K	t	2021-09-16 09:15:50.371502+00
1914	1371	Female	East	0- $25K	f	2021-09-16 09:15:50.375826+00
1915	1372	Female	East	0- $25K	t	2021-09-16 09:15:50.3796+00
1916	1373	Female	East	0- $25K	t	2021-09-16 09:15:50.383548+00
1917	1374	Female	East	0- $25K	f	2021-09-16 09:15:50.388049+00
1918	1375	Female	East	0- $25K	t	2021-09-16 09:15:50.392405+00
1919	1376	Female	East	0- $25K	t	2021-09-16 09:15:50.399111+00
1920	1377	Female	East	0- $25K	t	2021-09-16 09:15:50.408411+00
1921	1378	Female	East	0- $25K	f	2021-09-16 09:15:50.41733+00
1922	1379	Female	East	0- $25K	f	2021-09-16 09:15:50.430931+00
1923	1380	Female	East	0- $25K	t	2021-09-16 09:15:50.436951+00
1924	1381	Female	East	0- $25K	t	2021-09-16 09:15:50.441972+00
1925	1382	Female	East	0- $25K	t	2021-09-16 09:15:50.446849+00
1926	1383	Female	East	0- $25K	t	2021-09-16 09:15:50.451307+00
1927	1384	Female	East	0- $25K	t	2021-09-16 09:15:50.455733+00
1928	1385	Female	East	0- $25K	t	2021-09-16 09:15:50.459928+00
1929	1386	Female	East	0- $25K	f	2021-09-16 09:15:50.464295+00
1930	1387	Female	East	0- $25K	f	2021-09-16 09:15:50.472154+00
1931	1388	Female	East	0- $25K	f	2021-09-16 09:15:50.48087+00
1932	1389	Female	East	0- $25K	f	2021-09-16 09:15:50.489664+00
1933	1390	Female	East	0- $25K	f	2021-09-16 09:15:50.498361+00
1934	1391	Female	East	0- $25K	f	2021-09-16 09:15:50.507188+00
1935	1392	Female	East	0- $25K	f	2021-09-16 09:15:50.516018+00
1936	1393	Female	East	0- $25K	t	2021-09-16 09:15:50.52205+00
1937	1394	Female	East	0- $25K	f	2021-09-16 09:15:50.527297+00
1938	1395	Female	East	0- $25K	f	2021-09-16 09:15:50.533888+00
1939	1396	Female	East	0- $25K	t	2021-09-16 09:15:50.542738+00
1940	1397	Female	East	0- $25K	f	2021-09-16 09:15:50.549438+00
1941	1398	Female	East	0- $25K	t	2021-09-16 09:15:50.554465+00
1942	1399	Female	East	0- $25K	f	2021-09-16 09:15:50.561752+00
1943	1400	Female	East	0- $25K	t	2021-09-16 09:15:50.571846+00
1944	1401	Female	East	0- $25K	t	2021-09-16 09:15:50.580518+00
1945	1402	Female	East	0- $25K	f	2021-09-16 09:15:50.587064+00
1946	1403	Female	East	0- $25K	t	2021-09-16 09:15:50.594259+00
1947	1404	Female	East	0- $25K	f	2021-09-16 09:15:50.601311+00
1948	1405	Female	East	0- $25K	t	2021-09-16 09:15:50.608401+00
1949	1406	Female	East	0- $25K	f	2021-09-16 09:15:50.615288+00
1950	1407	Female	East	0- $25K	f	2021-09-16 09:15:50.622301+00
1951	1408	Female	East	0- $25K	t	2021-09-16 09:15:50.629426+00
1952	1409	Female	East	0- $25K	f	2021-09-16 09:15:50.636129+00
1953	1410	Female	East	0- $25K	t	2021-09-16 09:15:50.642888+00
1954	1411	Female	East	0- $25K	f	2021-09-16 09:15:50.649397+00
1955	1412	Female	East	0- $25K	t	2021-09-16 09:15:50.656262+00
1956	1413	Female	East	0- $25K	t	2021-09-16 09:15:50.662864+00
1957	1414	Female	East	0- $25K	t	2021-09-16 09:15:50.669134+00
1958	1415	Female	East	0- $25K	t	2021-09-16 09:15:50.684253+00
1959	1416	Female	East	0- $25K	f	2021-09-16 09:15:50.696962+00
1960	1417	Female	East	0- $25K	f	2021-09-16 09:15:50.710048+00
1961	1418	Female	East	0- $25K	t	2021-09-16 09:15:50.723091+00
1962	1419	Female	East	0- $25K	f	2021-09-16 09:15:50.733275+00
1963	1420	Female	East	0- $25K	t	2021-09-16 09:15:50.740226+00
1964	1421	Female	East	0- $25K	f	2021-09-16 09:15:50.746909+00
1965	1422	Female	East	0- $25K	f	2021-09-16 09:15:50.753991+00
1966	1423	Female	East	0- $25K	f	2021-09-16 09:15:50.761009+00
1967	1424	Female	East	0- $25K	f	2021-09-16 09:15:50.767487+00
1968	1425	Female	East	0- $25K	t	2021-09-16 09:15:50.773906+00
1969	1426	Female	East	0- $25K	t	2021-09-16 09:15:50.780635+00
1970	1427	Female	East	0- $25K	f	2021-09-16 09:15:50.786273+00
1971	1428	Female	East	0- $25K	f	2021-09-16 09:15:50.790857+00
1972	1429	Female	East	0- $25K	t	2021-09-16 09:15:50.794866+00
1973	1430	Female	East	0- $25K	t	2021-09-16 09:15:50.798887+00
1974	1431	Female	East	0- $25K	f	2021-09-16 09:15:50.809216+00
1975	1432	Female	East	0- $25K	f	2021-09-16 09:15:50.817895+00
1976	1433	Female	West	0- $25K	f	2021-09-16 09:15:50.827638+00
1977	1434	Female	West	0- $25K	f	2021-09-16 09:15:50.833576+00
1978	1435	Female	West	0- $25K	t	2021-09-16 09:15:50.838858+00
1979	1436	Female	West	0- $25K	t	2021-09-16 09:15:50.844144+00
1980	1437	Female	West	0- $25K	t	2021-09-16 09:15:50.853212+00
1981	1438	Female	West	0- $25K	f	2021-09-16 09:15:50.862329+00
1982	1439	Female	West	0- $25K	f	2021-09-16 09:15:50.872833+00
1983	1440	Female	West	0- $25K	t	2021-09-16 09:15:50.88231+00
1984	1441	Female	West	0- $25K	f	2021-09-16 09:15:50.888946+00
1985	1442	Female	West	0- $25K	f	2021-09-16 09:15:50.900511+00
1986	1443	Female	West	0- $25K	f	2021-09-16 09:15:50.905736+00
1987	1444	Female	West	0- $25K	f	2021-09-16 09:15:50.910842+00
1988	1445	Female	West	0- $25K	f	2021-09-16 09:15:50.917102+00
1989	1446	Female	West	0- $25K	f	2021-09-16 09:15:50.922379+00
1990	1447	Female	West	0- $25K	f	2021-09-16 09:15:50.92738+00
1991	1448	Female	West	0- $25K	f	2021-09-16 09:15:50.93223+00
1992	1449	Female	West	0- $25K	t	2021-09-16 09:15:50.937061+00
1993	1450	Female	West	0- $25K	f	2021-09-16 09:15:50.945687+00
1994	1451	Female	West	0- $25K	f	2021-09-16 09:15:50.954564+00
1995	1452	Female	West	0- $25K	t	2021-09-16 09:15:50.960443+00
1996	1453	Female	West	0- $25K	t	2021-09-16 09:15:50.966616+00
1997	1454	Female	West	0- $25K	t	2021-09-16 09:15:50.972702+00
1998	1455	Female	West	0- $25K	f	2021-09-16 09:15:50.97843+00
1999	1456	Female	West	0- $25K	t	2021-09-16 09:15:50.984562+00
2000	1457	Female	West	0- $25K	t	2021-09-16 09:15:50.99197+00
2001	1458	Female	West	0- $25K	t	2021-09-16 09:15:50.999875+00
2002	1459	Female	West	0- $25K	t	2021-09-16 09:15:51.005052+00
2003	1460	Female	West	0- $25K	f	2021-09-16 09:15:51.010221+00
2004	1461	Female	West	0- $25K	f	2021-09-16 09:15:51.015327+00
2005	1462	Female	West	0- $25K	f	2021-09-16 09:15:51.020171+00
2006	1463	Female	West	0- $25K	f	2021-09-16 09:15:51.025202+00
2007	1464	Female	West	0- $25K	f	2021-09-16 09:15:51.029715+00
2008	1465	Female	West	0- $25K	t	2021-09-16 09:15:51.03394+00
2009	1466	Female	West	0- $25K	t	2021-09-16 09:15:51.038615+00
2010	1467	Female	West	0- $25K	t	2021-09-16 09:15:51.045519+00
2011	1468	Female	West	0- $25K	t	2021-09-16 09:15:51.054925+00
2012	1469	Female	West	0- $25K	f	2021-09-16 09:15:51.0643+00
2013	1470	Female	West	0- $25K	t	2021-09-16 09:15:51.073194+00
2014	1471	Female	West	0- $25K	f	2021-09-16 09:15:51.080367+00
2015	1472	Female	West	0- $25K	t	2021-09-16 09:15:51.086282+00
2016	1473	Female	West	0- $25K	t	2021-09-16 09:15:51.09091+00
2017	1474	Female	West	0- $25K	f	2021-09-16 09:15:51.095804+00
2018	1475	Female	West	0- $25K	f	2021-09-16 09:15:51.100586+00
2019	1476	Female	West	0- $25K	f	2021-09-16 09:15:51.105962+00
2020	1477	Female	West	0- $25K	f	2021-09-16 09:15:51.111484+00
2021	1478	Female	West	0- $25K	f	2021-09-16 09:15:51.1162+00
2022	1479	Female	West	0- $25K	t	2021-09-16 09:15:51.121254+00
2023	1480	Female	West	0- $25K	t	2021-09-16 09:15:51.12603+00
2024	1481	Female	West	0- $25K	f	2021-09-16 09:15:51.130776+00
2025	1482	Female	West	0- $25K	t	2021-09-16 09:15:51.135989+00
2026	1483	Female	West	0- $25K	f	2021-09-16 09:15:51.14085+00
2027	1484	Female	West	0- $25K	t	2021-09-16 09:15:51.145523+00
2028	1485	Female	West	0- $25K	f	2021-09-16 09:15:51.150616+00
2029	1486	Female	West	0- $25K	t	2021-09-16 09:15:51.155585+00
2030	1487	Female	West	0- $25K	f	2021-09-16 09:15:51.160611+00
2031	1488	Female	West	0- $25K	t	2021-09-16 09:15:51.165944+00
2032	1489	Female	West	0- $25K	t	2021-09-16 09:15:51.170824+00
2033	1490	Female	West	0- $25K	t	2021-09-16 09:15:51.175799+00
2034	1491	Female	West	0- $25K	t	2021-09-16 09:15:51.180923+00
2035	1492	Female	West	0- $25K	f	2021-09-16 09:15:51.186391+00
2036	1493	Female	West	0- $25K	t	2021-09-16 09:15:51.191461+00
2037	1494	Female	West	0- $25K	f	2021-09-16 09:15:51.19702+00
2038	1495	Female	West	0- $25K	f	2021-09-16 09:15:51.201999+00
2039	1496	Female	West	0- $25K	t	2021-09-16 09:15:51.207396+00
2040	1497	Female	West	0- $25K	f	2021-09-16 09:15:51.212986+00
2041	1498	Female	West	0- $25K	f	2021-09-16 09:15:51.218024+00
2042	1499	Female	West	0- $25K	f	2021-09-16 09:15:51.222863+00
2043	1500	Female	West	0- $25K	t	2021-09-16 09:15:51.228471+00
2044	1501	Female	West	0- $25K	t	2021-09-16 09:15:51.233741+00
2045	1502	Female	West	0- $25K	t	2021-09-16 09:15:51.238652+00
2046	1503	Female	West	0- $25K	f	2021-09-16 09:15:51.24462+00
2047	1504	Female	West	0- $25K	t	2021-09-16 09:15:51.250439+00
2048	1505	Female	West	0- $25K	t	2021-09-16 09:15:51.2558+00
2049	1506	Female	West	0- $25K	f	2021-09-16 09:15:51.260951+00
2050	1507	Female	West	0- $25K	f	2021-09-16 09:15:51.266305+00
2051	1508	Female	West	0- $25K	t	2021-09-16 09:15:51.271157+00
2052	1509	Female	West	0- $25K	t	2021-09-16 09:15:51.275682+00
2053	1510	Female	West	0- $25K	t	2021-09-16 09:15:51.280381+00
2054	1511	Female	West	0- $25K	t	2021-09-16 09:15:51.286217+00
2055	1512	Female	West	0- $25K	t	2021-09-16 09:15:51.291086+00
2056	1513	Female	West	0- $25K	t	2021-09-16 09:15:51.296222+00
2057	1514	Female	West	0- $25K	t	2021-09-16 09:15:51.301257+00
2058	1515	Female	West	0- $25K	f	2021-09-16 09:15:51.306265+00
2059	1516	Female	West	0- $25K	f	2021-09-16 09:15:51.311402+00
2060	1517	Female	West	0- $25K	f	2021-09-16 09:15:51.316445+00
2061	1518	Female	West	0- $25K	f	2021-09-16 09:15:51.321083+00
2062	1519	Female	West	0- $25K	t	2021-09-16 09:15:51.325877+00
2063	1520	Female	West	0- $25K	t	2021-09-16 09:15:51.330908+00
2064	1521	Female	West	0- $25K	t	2021-09-16 09:15:51.335948+00
2065	1522	Female	West	0- $25K	t	2021-09-16 09:15:51.34083+00
2066	1523	Female	West	0- $25K	t	2021-09-16 09:15:51.346105+00
2067	1524	Female	West	0- $25K	t	2021-09-16 09:15:51.352043+00
2068	1525	Female	West	0- $25K	t	2021-09-16 09:15:51.356841+00
2069	1526	Female	West	0- $25K	f	2021-09-16 09:15:51.361386+00
2070	1527	Female	West	0- $25K	f	2021-09-16 09:15:51.366555+00
2071	1528	Female	West	0- $25K	t	2021-09-16 09:15:51.373497+00
2072	1529	Female	West	0- $25K	t	2021-09-16 09:15:51.380476+00
2073	1530	Female	West	0- $25K	t	2021-09-16 09:15:51.388159+00
2074	1531	Female	West	0- $25K	t	2021-09-16 09:15:51.393529+00
2075	1532	Female	West	0- $25K	f	2021-09-16 09:15:51.399877+00
2076	1533	Female	West	0- $25K	t	2021-09-16 09:15:51.407638+00
2077	1534	Female	West	0- $25K	f	2021-09-16 09:15:51.415614+00
2078	1535	Female	West	0- $25K	f	2021-09-16 09:15:51.421219+00
2079	1536	Female	West	0- $25K	f	2021-09-16 09:15:51.427046+00
2080	1537	Female	West	0- $25K	t	2021-09-16 09:15:51.433965+00
2081	1538	Female	West	0- $25K	t	2021-09-16 09:15:51.439996+00
2082	1539	Female	West	0- $25K	t	2021-09-16 09:15:51.446583+00
2083	1540	Female	West	0- $25K	t	2021-09-16 09:15:51.454108+00
2084	1541	Female	West	0- $25K	f	2021-09-16 09:15:51.460085+00
2085	1542	Female	West	0- $25K	t	2021-09-16 09:15:51.466409+00
2086	1543	Female	West	0- $25K	f	2021-09-16 09:15:51.474576+00
2087	1544	Female	West	0- $25K	f	2021-09-16 09:15:51.48232+00
2088	1545	Female	West	0- $25K	f	2021-09-16 09:15:51.490132+00
2089	1546	Female	West	0- $25K	f	2021-09-16 09:15:51.49884+00
2090	1547	Female	West	0- $25K	f	2021-09-16 09:15:51.506066+00
2091	1548	Female	West	0- $25K	f	2021-09-16 09:15:51.512662+00
2092	1549	Female	West	0- $25K	f	2021-09-16 09:15:51.52083+00
2093	1550	Female	West	0- $25K	f	2021-09-16 09:15:51.52643+00
2094	1551	Female	West	0- $25K	f	2021-09-16 09:15:51.533042+00
2095	1552	Female	West	0- $25K	f	2021-09-16 09:15:51.539834+00
2096	1553	Female	West	0- $25K	t	2021-09-16 09:15:51.546799+00
2097	1554	Female	West	0- $25K	t	2021-09-16 09:15:51.552682+00
2098	1555	Female	West	0- $25K	t	2021-09-16 09:15:51.559045+00
2099	1556	Female	West	0- $25K	t	2021-09-16 09:15:51.565484+00
2100	1557	Female	West	0- $25K	f	2021-09-16 09:15:51.572476+00
2101	1558	Female	West	0- $25K	t	2021-09-16 09:15:51.580262+00
2102	1559	Female	West	0- $25K	t	2021-09-16 09:15:51.590218+00
2103	1560	Female	West	0- $25K	f	2021-09-16 09:15:51.59648+00
2104	1561	Female	West	0- $25K	f	2021-09-16 09:15:51.604222+00
2105	1562	Female	West	0- $25K	f	2021-09-16 09:15:51.611765+00
2106	1563	Female	West	0- $25K	t	2021-09-16 09:15:51.622028+00
2107	1564	Female	West	0- $25K	t	2021-09-16 09:15:51.628844+00
2108	1565	Female	West	0- $25K	t	2021-09-16 09:15:51.6406+00
2109	1566	Female	West	0- $25K	t	2021-09-16 09:15:51.647696+00
2110	1567	Female	West	0- $25K	f	2021-09-16 09:15:51.657375+00
2111	1568	Female	West	0- $25K	t	2021-09-16 09:15:51.665368+00
2112	1569	Female	West	0- $25K	f	2021-09-16 09:15:51.673972+00
2113	1570	Female	West	0- $25K	f	2021-09-16 09:15:51.681137+00
2114	1571	Female	West	0- $25K	t	2021-09-16 09:15:51.687407+00
2115	1572	Female	West	0- $25K	f	2021-09-16 09:15:51.694135+00
2116	1573	Female	West	0- $25K	f	2021-09-16 09:15:51.700163+00
2117	1574	Female	West	0- $25K	f	2021-09-16 09:15:51.707798+00
2118	1575	Female	West	0- $25K	f	2021-09-16 09:15:51.717195+00
2119	1576	Female	West	0- $25K	t	2021-09-16 09:15:51.724343+00
2120	1577	Female	West	0- $25K	t	2021-09-16 09:15:51.730626+00
2121	1578	Female	West	0- $25K	t	2021-09-16 09:15:51.737769+00
2122	1579	Female	West	0- $25K	t	2021-09-16 09:15:51.744246+00
2123	1580	Female	West	0- $25K	t	2021-09-16 09:15:51.750669+00
2124	1581	Female	West	0- $25K	f	2021-09-16 09:15:51.75671+00
2125	1582	Female	West	0- $25K	f	2021-09-16 09:15:51.763079+00
2126	1583	Female	West	0- $25K	f	2021-09-16 09:15:51.770288+00
2127	1584	Female	West	0- $25K	t	2021-09-16 09:15:51.776283+00
2128	1585	Female	West	0- $25K	t	2021-09-16 09:15:51.782678+00
2129	1586	Female	West	0- $25K	f	2021-09-16 09:15:51.78968+00
2130	1587	Female	West	0- $25K	t	2021-09-16 09:15:51.79622+00
2131	1588	Female	West	0- $25K	f	2021-09-16 09:15:51.802297+00
2132	1589	Female	West	0- $25K	t	2021-09-16 09:15:51.810584+00
2133	1590	Female	West	0- $25K	t	2021-09-16 09:15:51.817329+00
2134	1591	Female	West	0- $25K	f	2021-09-16 09:15:51.82342+00
2135	1592	Female	West	0- $25K	f	2021-09-16 09:15:51.829667+00
2136	1593	Female	West	0- $25K	t	2021-09-16 09:15:51.835955+00
2137	1594	Female	West	0- $25K	t	2021-09-16 09:15:51.841984+00
2138	1595	Female	West	0- $25K	f	2021-09-16 09:15:51.848191+00
2139	1596	Female	West	0- $25K	f	2021-09-16 09:15:51.856941+00
2140	1597	Female	West	0- $25K	f	2021-09-16 09:15:51.863965+00
2141	1598	Female	West	0- $25K	t	2021-09-16 09:15:51.869937+00
2142	1599	Female	West	0- $25K	f	2021-09-16 09:15:51.876551+00
\.


--
-- Data for Name: api_customerinsurancedata; Type: TABLE DATA; Schema: public; Owner: narayanac
--

COPY public.api_customerinsurancedata (id, policy_id, fuel, vehicle_segment, premium, bodily_injury_liability, personal_injury_protection, purchase_date, created_on, customer_id, collision, comprehensive, property_damage_liability) FROM stdin;
2421	12345	CNG	A	958	f	f	2018-01-16	2021-09-16 09:15:45.630529+00	1431	t	t	f
2422	12346	CNG	A	1272	t	f	2018-01-04	2021-09-16 09:15:45.635961+00	1432	f	t	f
2423	12347	CNG	A	2150	f	t	2018-01-07	2021-09-16 09:15:45.640352+00	1433	t	f	t
2424	12348	CNG	A	2123	t	f	2018-01-07	2021-09-16 09:15:45.653564+00	1434	t	t	t
2425	12349	CNG	A	1110	t	t	2018-01-01	2021-09-16 09:15:45.668999+00	1435	t	t	t
2426	12350	CNG	A	1571	f	f	2018-01-22	2021-09-16 09:15:45.68455+00	1436	f	f	t
2427	12351	CNG	A	1030	t	t	2018-01-19	2021-09-16 09:15:45.697594+00	1437	t	f	t
2428	12352	CNG	A	1732	t	t	2018-01-23	2021-09-16 09:15:45.707152+00	1438	f	f	t
2429	12353	CNG	A	2175	f	f	2018-01-01	2021-09-16 09:15:45.715198+00	1439	f	t	t
2430	12354	CNG	A	1725	f	f	2018-01-20	2021-09-16 09:15:45.722626+00	1440	t	f	f
2431	12355	CNG	A	1805	f	t	2018-01-28	2021-09-16 09:15:45.729053+00	1441	t	f	f
2432	12356	CNG	A	1552	t	t	2018-01-17	2021-09-16 09:15:45.735226+00	1442	f	t	f
2433	12357	CNG	A	1888	f	t	2018-01-29	2021-09-16 09:15:45.742236+00	1443	t	t	t
2434	12358	CNG	A	1355	t	f	2018-01-18	2021-09-16 09:15:45.7508+00	1444	t	f	f
2435	12359	CNG	A	2356	f	f	2018-01-19	2021-09-16 09:15:45.75653+00	1445	f	t	f
2436	12360	CNG	A	2102	t	f	2018-01-06	2021-09-16 09:15:45.761783+00	1446	t	f	f
2437	12361	CNG	A	1258	t	t	2018-01-27	2021-09-16 09:15:45.766456+00	1447	t	t	t
2438	12362	CNG	A	2326	f	t	2018-01-21	2021-09-16 09:15:45.770366+00	1448	t	t	f
2439	420	CNG	A	584	t	t	2018-01-24	2021-09-16 09:15:45.775317+00	1449	t	f	t
2440	12364	CNG	A	2397	t	t	2018-01-08	2021-09-16 09:15:45.780416+00	1450	f	f	t
2441	12365	CNG	A	1493	t	f	2018-01-23	2021-09-16 09:15:45.785097+00	1451	f	f	f
2442	12366	CNG	A	661	f	t	2018-01-02	2021-09-16 09:15:45.787767+00	1451	f	f	t
2443	12367	CNG	A	685	t	t	2018-01-03	2021-09-16 09:15:45.790347+00	1451	t	f	f
2444	12368	CNG	A	1006	t	t	2018-01-08	2021-09-16 09:15:45.792991+00	1451	t	t	t
2445	12369	CNG	A	1160	t	f	2018-01-16	2021-09-16 09:15:45.798315+00	1451	t	t	f
2446	12370	CNG	A	1602	f	f	2018-01-25	2021-09-16 09:15:45.802808+00	1451	t	t	f
2447	12371	CNG	A	2355	f	t	2018-01-25	2021-09-16 09:15:45.807187+00	1451	t	t	t
2448	12372	CNG	A	1921	t	t	2018-01-05	2021-09-16 09:15:45.811037+00	1451	f	t	t
2449	12373	CNG	A	2200	f	t	2018-01-21	2021-09-16 09:15:45.814475+00	1451	t	t	f
2450	12374	CNG	A	2357	f	t	2018-01-27	2021-09-16 09:15:45.817557+00	1451	t	t	t
2451	12375	CNG	A	1047	f	t	2018-01-22	2021-09-16 09:15:45.820294+00	1451	t	f	f
2452	840	CNG	A	2089	f	t	2018-01-04	2021-09-16 09:15:45.822944+00	1451	t	f	f
2453	12377	CNG	A	1780	f	f	2018-01-15	2021-09-16 09:15:45.825523+00	1451	t	t	f
2454	12378	CNG	A	1035	t	f	2018-01-26	2021-09-16 09:15:45.828195+00	1451	f	f	t
2455	12379	CNG	A	526	t	t	2018-01-29	2021-09-16 09:15:45.830809+00	1451	f	t	t
2456	12380	CNG	A	1827	f	f	2018-01-20	2021-09-16 09:15:45.833854+00	1451	t	f	t
2457	12381	CNG	A	2003	t	t	2018-01-24	2021-09-16 09:15:45.836874+00	1451	t	f	f
2458	12382	CNG	A	1960	f	t	2018-01-08	2021-09-16 09:15:45.840012+00	1451	t	t	t
2459	12383	CNG	A	1197	f	t	2018-01-03	2021-09-16 09:15:45.843233+00	1451	t	f	f
2460	12384	CNG	A	614	f	t	2018-01-21	2021-09-16 09:15:45.846165+00	1451	f	t	f
2461	12385	CNG	A	568	t	t	2018-01-09	2021-09-16 09:15:45.849506+00	1451	f	f	t
2462	12386	CNG	A	2149	t	f	2018-01-12	2021-09-16 09:15:45.852252+00	1451	t	t	t
2463	12387	CNG	A	1797	t	f	2018-01-27	2021-09-16 09:15:45.854965+00	1451	f	t	t
2464	12388	CNG	A	1246	t	f	2018-01-14	2021-09-16 09:15:45.857734+00	1451	f	f	f
2465	12389	CNG	A	1295	t	t	2018-01-24	2021-09-16 09:15:45.860529+00	1451	f	f	f
2466	12390	CNG	A	642	f	f	2018-01-21	2021-09-16 09:15:45.863417+00	1451	f	t	t
2467	12391	CNG	A	1979	t	t	2018-01-06	2021-09-16 09:15:45.866517+00	1451	t	t	t
2468	12392	CNG	A	2327	f	f	2018-01-13	2021-09-16 09:15:45.869822+00	1451	t	t	t
2469	12393	CNG	A	1025	t	f	2018-01-05	2021-09-16 09:15:45.873353+00	1451	f	t	f
2470	12394	CNG	A	529	f	f	2018-01-23	2021-09-16 09:15:45.876508+00	1451	f	t	f
2471	12395	CNG	A	2072	f	f	2018-01-16	2021-09-16 09:15:45.879444+00	1451	f	f	t
2472	12396	CNG	A	2103	f	f	2018-01-18	2021-09-16 09:15:45.882386+00	1451	t	f	f
2473	12397	CNG	A	1929	f	t	2018-01-15	2021-09-16 09:15:45.885183+00	1451	f	t	f
2474	12398	CNG	A	2143	t	f	2018-01-13	2021-09-16 09:15:45.887967+00	1451	t	t	t
2475	12399	CNG	A	2132	f	f	2018-01-11	2021-09-16 09:15:45.893171+00	1451	t	f	f
2476	12400	CNG	A	1459	t	f	2018-01-13	2021-09-16 09:15:45.89907+00	1451	f	f	f
2477	12401	CNG	A	1831	t	t	2018-01-17	2021-09-16 09:15:45.904526+00	1451	f	f	f
2478	12402	CNG	A	1171	t	t	2018-01-30	2021-09-16 09:15:45.910075+00	1451	f	f	t
2479	12403	CNG	A	1692	t	f	2018-01-10	2021-09-16 09:15:45.914736+00	1451	t	f	t
2480	12404	CNG	A	1262	f	t	2018-01-11	2021-09-16 09:15:45.920074+00	1451	f	t	t
2481	12405	CNG	A	2451	f	t	2018-01-23	2021-09-16 09:15:45.924757+00	1451	f	f	f
2482	12406	CNG	A	1906	f	f	2018-01-03	2021-09-16 09:15:45.929611+00	1451	t	t	t
2483	12407	CNG	A	2095	t	f	2018-01-24	2021-09-16 09:15:45.934516+00	1451	t	f	t
2484	12408	CNG	A	1597	t	f	2018-01-20	2021-09-16 09:15:45.938783+00	1451	t	f	f
2485	12409	CNG	A	2362	t	t	2018-01-15	2021-09-16 09:15:45.942761+00	1451	t	f	f
2486	12410	CNG	A	1612	t	t	2018-01-17	2021-09-16 09:15:45.948485+00	1451	t	f	f
2487	12411	CNG	A	711	f	f	2018-01-19	2021-09-16 09:15:45.953062+00	1451	t	t	f
2488	12412	CNG	A	1771	t	f	2018-01-29	2021-09-16 09:15:45.956721+00	1451	f	t	t
2489	12413	CNG	A	1301	f	t	2018-01-20	2021-09-16 09:15:45.960113+00	1451	f	t	f
2490	12414	CNG	A	2411	t	t	2018-01-16	2021-09-16 09:15:45.96342+00	1451	f	t	f
2491	12415	CNG	A	1065	f	t	2018-01-10	2021-09-16 09:15:45.966812+00	1451	f	t	f
2492	12416	CNG	A	2393	t	t	2018-01-13	2021-09-16 09:15:45.970074+00	1451	t	f	t
2493	12417	CNG	A	1837	t	t	2018-01-05	2021-09-16 09:15:45.973373+00	1451	f	f	t
2494	12418	CNG	A	1598	f	f	2018-01-01	2021-09-16 09:15:45.976307+00	1451	f	f	t
2495	12419	CNG	A	1072	t	f	2018-01-30	2021-09-16 09:15:45.979208+00	1451	f	t	t
2496	12420	CNG	A	591	f	f	2018-01-25	2021-09-16 09:15:45.982025+00	1451	f	f	t
2497	12421	CNG	A	1617	t	t	2018-01-18	2021-09-16 09:15:45.985007+00	1451	f	t	t
2498	12422	CNG	A	1174	t	f	2018-01-04	2021-09-16 09:15:45.987939+00	1451	t	f	t
2499	12423	CNG	A	2241	t	t	2018-01-28	2021-09-16 09:15:45.993635+00	1451	f	t	t
2500	12424	CNG	A	2248	t	t	2018-01-31	2021-09-16 09:15:45.998847+00	1451	t	t	f
2501	12425	CNG	A	2121	t	t	2018-01-27	2021-09-16 09:15:46.003528+00	1451	f	t	f
2502	12426	CNG	A	1994	f	f	2018-01-25	2021-09-16 09:15:46.007688+00	1451	f	t	f
2503	12427	CNG	A	2442	t	t	2018-01-01	2021-09-16 09:15:46.010911+00	1451	f	f	f
2504	12428	CNG	A	1763	f	t	2018-01-23	2021-09-16 09:15:46.013792+00	1451	f	f	t
2505	12429	CNG	A	1494	f	f	2018-01-28	2021-09-16 09:15:46.016778+00	1451	t	t	f
2506	12430	CNG	A	2039	t	f	2018-01-03	2021-09-16 09:15:46.019763+00	1451	t	f	t
2507	12431	CNG	A	898	t	f	2018-01-27	2021-09-16 09:15:46.0226+00	1451	f	f	f
2508	12432	CNG	A	823	f	f	2018-01-12	2021-09-16 09:15:46.025434+00	1451	t	f	t
2509	12433	CNG	A	1144	f	t	2018-01-11	2021-09-16 09:15:46.028821+00	1451	f	f	f
2510	12434	CNG	A	1051	f	f	2018-01-23	2021-09-16 09:15:46.032518+00	1451	f	f	t
2511	12435	CNG	A	1212	f	f	2018-01-10	2021-09-16 09:15:46.04264+00	1451	f	f	t
2512	12436	CNG	A	1829	f	t	2018-01-13	2021-09-16 09:15:46.049244+00	1451	f	f	t
2513	12437	CNG	A	1548	f	f	2018-01-15	2021-09-16 09:15:46.055525+00	1451	t	f	f
2514	12438	CNG	A	1663	t	t	2018-01-06	2021-09-16 09:15:46.062572+00	1451	t	t	f
2515	12439	CNG	A	1313	f	f	2018-01-23	2021-09-16 09:15:46.07096+00	1451	t	t	f
2516	12440	CNG	A	1915	t	t	2018-01-04	2021-09-16 09:15:46.078247+00	1451	f	f	t
2517	12441	CNG	A	1888	f	f	2018-01-08	2021-09-16 09:15:46.084793+00	1451	t	t	f
2518	12442	CNG	A	694	t	t	2018-01-19	2021-09-16 09:15:46.091758+00	1451	f	f	t
2519	12443	CNG	A	637	f	f	2018-01-05	2021-09-16 09:15:46.097805+00	1451	t	f	t
2520	12444	CNG	A	1959	t	t	2018-02-10	2021-09-16 09:15:46.104408+00	1451	f	t	f
2521	12445	CNG	A	1673	f	t	2018-02-08	2021-09-16 09:15:46.1088+00	1451	t	t	t
2522	12446	CNG	A	934	t	f	2018-02-01	2021-09-16 09:15:46.112655+00	1451	f	t	t
2523	12447	CNG	A	2391	t	f	2018-02-23	2021-09-16 09:15:46.116519+00	1451	t	f	f
2524	12448	CNG	A	2246	t	f	2018-02-20	2021-09-16 09:15:46.120078+00	1451	t	f	f
2525	12449	CNG	A	1328	f	t	2018-02-03	2021-09-16 09:15:46.1236+00	1451	f	f	t
2526	12450	CNG	A	2455	t	t	2018-02-13	2021-09-16 09:15:46.129142+00	1451	t	t	t
2527	12451	CNG	A	1611	f	t	2018-02-25	2021-09-16 09:15:46.133489+00	1451	t	f	f
2528	12452	CNG	A	2256	t	f	2018-02-03	2021-09-16 09:15:46.137189+00	1451	t	f	f
2529	12453	CNG	A	1284	f	f	2018-02-26	2021-09-16 09:15:46.140648+00	1451	f	f	f
2530	12454	CNG	A	2067	t	t	2018-02-05	2021-09-16 09:15:46.144093+00	1451	t	f	f
2531	12455	CNG	A	1477	t	f	2018-02-11	2021-09-16 09:15:46.147361+00	1451	t	f	f
2532	12456	CNG	A	865	f	f	2018-02-18	2021-09-16 09:15:46.150726+00	1451	f	t	f
2533	12457	CNG	A	1662	f	f	2018-02-01	2021-09-16 09:15:46.154007+00	1451	t	t	f
2534	12458	CNG	A	1870	f	f	2018-02-03	2021-09-16 09:15:46.159278+00	1451	f	f	f
2535	12459	CNG	A	626	t	t	2018-02-05	2021-09-16 09:15:46.163576+00	1451	t	f	t
2536	12460	CNG	A	1658	t	f	2018-02-11	2021-09-16 09:15:46.167786+00	1451	t	f	t
2537	12461	CNG	A	1168	f	t	2018-02-16	2021-09-16 09:15:46.171037+00	1451	t	t	f
2538	12462	CNG	A	1083	f	f	2018-02-02	2021-09-16 09:15:46.174251+00	1451	t	f	t
2539	12463	CNG	A	2253	f	t	2018-02-06	2021-09-16 09:15:46.178106+00	1451	t	f	f
2540	12464	CNG	A	708	t	t	2018-02-05	2021-09-16 09:15:46.181428+00	1451	f	t	t
2541	12465	CNG	A	2254	t	t	2018-02-14	2021-09-16 09:15:46.184651+00	1451	t	f	f
2542	12466	CNG	A	1092	f	t	2018-02-15	2021-09-16 09:15:46.187797+00	1451	f	t	f
2543	12467	CNG	A	778	t	f	2018-02-26	2021-09-16 09:15:46.19135+00	1451	f	t	f
2544	12468	CNG	A	1041	f	f	2018-02-15	2021-09-16 09:15:46.195542+00	1451	t	t	t
2545	12469	CNG	A	2310	f	f	2018-02-03	2021-09-16 09:15:46.199429+00	1451	t	f	t
2546	12470	CNG	A	2339	t	t	2018-02-25	2021-09-16 09:15:46.202582+00	1451	t	f	t
2547	12471	CNG	A	2218	f	f	2018-02-12	2021-09-16 09:15:46.205657+00	1451	f	t	t
2548	12472	CNG	A	1628	t	t	2018-02-11	2021-09-16 09:15:46.208464+00	1451	f	t	f
2549	12473	CNG	A	850	t	f	2018-02-16	2021-09-16 09:15:46.212131+00	1451	t	f	t
2550	12474	CNG	A	618	f	t	2018-02-14	2021-09-16 09:15:46.215597+00	1451	t	t	f
2551	12475	CNG	A	2128	f	f	2018-02-22	2021-09-16 09:15:46.219215+00	1451	t	f	f
2552	12476	CNG	A	1213	t	f	2018-02-16	2021-09-16 09:15:46.222412+00	1451	t	t	f
2553	12477	CNG	A	749	t	f	2018-02-04	2021-09-16 09:15:46.225867+00	1451	f	t	t
2554	12478	CNG	A	2420	f	f	2018-02-14	2021-09-16 09:15:46.229998+00	1451	f	t	t
2555	12479	CNG	A	1231	f	t	2018-02-08	2021-09-16 09:15:46.233924+00	1451	t	f	f
2556	12480	CNG	A	510	f	t	2018-02-27	2021-09-16 09:15:46.237433+00	1451	f	t	f
2557	12481	CNG	A	2376	f	t	2018-02-14	2021-09-16 09:15:46.240757+00	1451	f	t	t
2558	12482	CNG	A	1746	t	t	2018-02-27	2021-09-16 09:15:46.243881+00	1451	t	f	f
2559	12483	CNG	A	1533	f	f	2018-02-21	2021-09-16 09:15:46.247657+00	1451	f	t	f
2560	12484	CNG	A	901	t	t	2018-02-19	2021-09-16 09:15:46.251264+00	1451	f	f	t
2561	12485	CNG	A	2021	f	f	2018-02-04	2021-09-16 09:15:46.254467+00	1451	t	f	t
2562	12486	CNG	A	1570	f	f	2018-02-22	2021-09-16 09:15:46.257782+00	1451	f	t	f
2563	12487	CNG	A	2338	f	f	2018-02-19	2021-09-16 09:15:46.261439+00	1451	t	f	t
2564	12488	CNG	A	1065	f	f	2018-02-10	2021-09-16 09:15:46.265198+00	1451	f	t	t
2565	12489	CNG	A	2227	f	t	2018-02-14	2021-09-16 09:15:46.268617+00	1451	f	f	f
2566	12490	CNG	A	1721	t	f	2018-02-16	2021-09-16 09:15:46.272076+00	1451	f	f	f
2567	12491	CNG	A	1914	f	f	2018-02-05	2021-09-16 09:15:46.274824+00	1451	t	f	t
2568	12492	CNG	A	1149	f	f	2018-02-10	2021-09-16 09:15:46.278651+00	1451	f	f	f
2569	12493	CNG	A	2033	f	f	2018-02-20	2021-09-16 09:15:46.281579+00	1451	t	t	t
2570	12494	CNG	A	2025	f	f	2018-02-22	2021-09-16 09:15:46.284741+00	1451	t	f	f
2571	12495	CNG	A	743	t	f	2018-02-12	2021-09-16 09:15:46.287872+00	1451	f	f	t
2572	12496	CNG	A	1044	t	f	2018-02-18	2021-09-16 09:15:46.291551+00	1451	f	t	t
2573	12497	CNG	A	1404	t	f	2018-02-25	2021-09-16 09:15:46.294543+00	1451	f	f	t
2574	12498	CNG	A	1569	t	t	2018-02-22	2021-09-16 09:15:46.297452+00	1451	f	f	t
2575	12499	CNG	A	1477	t	f	2018-02-01	2021-09-16 09:15:46.300145+00	1451	f	t	t
2576	12500	CNG	A	1464	t	t	2018-02-17	2021-09-16 09:15:46.30299+00	1451	t	t	f
2577	12501	CNG	A	2487	t	t	2018-02-20	2021-09-16 09:15:46.305651+00	1451	f	t	f
2578	12502	CNG	A	2205	f	t	2018-02-15	2021-09-16 09:15:46.308409+00	1451	t	f	t
2579	12503	CNG	A	2148	f	f	2018-02-06	2021-09-16 09:15:46.311123+00	1451	f	t	t
2580	12504	CNG	A	2006	t	t	2018-02-25	2021-09-16 09:15:46.314253+00	1451	t	f	f
2581	12505	CNG	A	2168	t	t	2018-02-27	2021-09-16 09:15:46.317504+00	1451	f	t	f
2582	12506	CNG	A	1574	f	t	2018-02-02	2021-09-16 09:15:46.320957+00	1451	f	f	t
2583	12507	CNG	A	1764	t	f	2018-02-02	2021-09-16 09:15:46.324647+00	1451	t	t	f
2584	12508	CNG	A	2484	t	t	2018-02-06	2021-09-16 09:15:46.327571+00	1451	t	f	t
2585	12509	CNG	A	640	t	f	2018-02-06	2021-09-16 09:15:46.33032+00	1451	t	f	t
2586	12510	CNG	A	663	f	f	2018-02-12	2021-09-16 09:15:46.333121+00	1451	t	f	t
2587	12511	CNG	A	1504	f	f	2018-02-22	2021-09-16 09:15:46.335922+00	1451	f	f	f
2588	12512	CNG	A	1908	f	t	2018-02-17	2021-09-16 09:15:46.338467+00	1451	t	t	f
2589	12513	CNG	A	519	f	f	2018-02-08	2021-09-16 09:15:46.340974+00	1451	f	t	f
2590	12514	CNG	A	1475	t	t	2018-02-05	2021-09-16 09:15:46.343691+00	1451	t	f	t
2591	12515	CNG	A	1663	t	f	2018-02-27	2021-09-16 09:15:46.346755+00	1451	t	f	t
2592	12516	CNG	A	1179	f	t	2018-02-14	2021-09-16 09:15:46.350029+00	1451	f	f	f
2593	12517	CNG	A	898	f	t	2018-02-17	2021-09-16 09:15:46.353742+00	1451	f	t	t
2594	12518	CNG	A	641	t	f	2018-02-08	2021-09-16 09:15:46.356819+00	1451	t	f	t
2595	12519	CNG	A	1002	f	f	2018-02-24	2021-09-16 09:15:46.359725+00	1451	f	f	t
2596	12520	CNG	A	2253	t	f	2018-02-25	2021-09-16 09:15:46.362818+00	1451	t	t	t
2597	12521	CNG	A	1100	t	f	2018-02-10	2021-09-16 09:15:46.365603+00	1451	t	t	f
2598	12522	CNG	A	2316	f	t	2018-02-19	2021-09-16 09:15:46.368752+00	1451	t	t	t
2599	12523	CNG	A	2387	t	f	2018-02-14	2021-09-16 09:15:46.371571+00	1451	t	t	t
2600	12524	CNG	A	2193	f	t	2018-02-21	2021-09-16 09:15:46.374129+00	1451	t	f	f
2601	12525	CNG	A	2484	t	f	2018-02-22	2021-09-16 09:15:46.377191+00	1451	f	t	f
2602	12526	CNG	A	1835	f	f	2018-02-24	2021-09-16 09:15:46.380681+00	1451	t	t	t
2603	12527	CNG	A	2310	f	f	2018-02-13	2021-09-16 09:15:46.383868+00	1451	t	f	t
2604	12528	CNG	A	545	t	t	2018-02-06	2021-09-16 09:15:46.387148+00	1451	f	f	f
2605	12529	CNG	A	1924	t	f	2018-02-21	2021-09-16 09:15:46.389557+00	1451	t	t	f
2606	12530	CNG	A	615	t	t	2018-02-12	2021-09-16 09:15:46.392514+00	1451	f	f	t
2607	12531	CNG	A	727	t	t	2018-02-02	2021-09-16 09:15:46.39513+00	1451	f	t	t
2608	12532	CNG	A	685	f	t	2018-02-10	2021-09-16 09:15:46.39776+00	1451	t	t	f
2609	12533	CNG	A	2390	f	f	2018-02-02	2021-09-16 09:15:46.400564+00	1451	t	t	t
2610	12534	CNG	A	709	t	f	2018-02-24	2021-09-16 09:15:46.403208+00	1451	f	t	t
2611	12535	CNG	A	910	f	t	2018-02-27	2021-09-16 09:15:46.405804+00	1451	f	f	t
2612	12536	CNG	A	1030	f	t	2018-02-19	2021-09-16 09:15:46.408824+00	1451	f	f	t
2613	12537	CNG	A	1538	t	f	2018-02-16	2021-09-16 09:15:46.411822+00	1451	t	t	t
2614	12538	CNG	A	600	f	t	2018-02-17	2021-09-16 09:15:46.415185+00	1451	f	f	t
2615	12539	CNG	A	540	t	t	2018-02-21	2021-09-16 09:15:46.418655+00	1451	f	t	f
2616	12540	CNG	A	997	t	t	2018-02-16	2021-09-16 09:15:46.42152+00	1451	f	f	t
2617	12541	CNG	A	2086	f	f	2018-02-11	2021-09-16 09:15:46.424299+00	1451	t	t	f
2618	12542	CNG	A	2491	t	f	2018-02-15	2021-09-16 09:15:46.427163+00	1451	t	f	f
2619	12543	CNG	A	651	t	t	2018-02-22	2021-09-16 09:15:46.429932+00	1451	f	t	t
2620	12544	CNG	A	1359	t	t	2018-02-23	2021-09-16 09:15:46.432609+00	1451	t	f	t
2621	12545	CNG	A	1109	t	f	2018-03-22	2021-09-16 09:15:46.435146+00	1451	f	t	t
2622	12546	CNG	A	2347	t	t	2018-03-23	2021-09-16 09:15:46.437865+00	1451	t	f	t
2623	12547	CNG	A	966	f	f	2018-03-18	2021-09-16 09:15:46.440749+00	1451	t	t	t
2624	12548	CNG	A	935	f	t	2018-03-07	2021-09-16 09:15:46.443848+00	1451	t	f	f
2625	12549	CNG	A	2191	f	t	2018-03-27	2021-09-16 09:15:46.447192+00	1451	f	f	f
2626	12550	CNG	A	1245	f	t	2018-03-18	2021-09-16 09:15:46.45044+00	1451	t	f	f
2627	12551	CNG	A	2448	f	f	2018-03-05	2021-09-16 09:15:46.453073+00	1451	t	f	t
2628	12552	CNG	A	525	f	t	2018-03-29	2021-09-16 09:15:46.455728+00	1451	f	t	t
2629	12553	CNG	A	1214	t	f	2018-03-02	2021-09-16 09:15:46.458406+00	1451	t	t	f
2630	12554	CNG	A	836	t	t	2018-03-10	2021-09-16 09:15:46.461254+00	1451	t	f	f
2631	12555	CNG	A	525	t	t	2018-03-03	2021-09-16 09:15:46.463937+00	1451	f	f	f
2632	12556	CNG	A	1640	f	t	2018-03-27	2021-09-16 09:15:46.46659+00	1451	f	f	f
2633	12557	CNG	A	554	t	f	2018-03-02	2021-09-16 09:15:46.469252+00	1451	f	f	f
2634	12558	CNG	A	599	f	t	2018-03-20	2021-09-16 09:15:46.471844+00	1451	t	f	t
2635	12559	CNG	A	1269	t	t	2018-03-30	2021-09-16 09:15:46.474679+00	1451	t	f	t
2636	12560	CNG	A	838	t	f	2018-03-24	2021-09-16 09:15:46.477641+00	1451	t	t	f
2637	12561	CNG	A	745	t	f	2018-03-18	2021-09-16 09:15:46.480877+00	1451	f	t	t
2638	12562	CNG	A	1349	t	f	2018-03-28	2021-09-16 09:15:46.483969+00	1451	t	f	t
2639	12563	CNG	A	646	t	f	2018-03-22	2021-09-16 09:15:46.486558+00	1451	t	f	f
2640	12564	CNG	A	1322	t	t	2018-03-01	2021-09-16 09:15:46.489329+00	1451	t	f	t
2641	12565	CNG	A	1050	f	f	2018-03-30	2021-09-16 09:15:46.492248+00	1451	f	f	t
2642	12566	CNG	A	2114	f	t	2018-03-23	2021-09-16 09:15:46.494866+00	1451	f	f	f
2643	12567	CNG	A	1674	f	f	2018-03-23	2021-09-16 09:15:46.497503+00	1451	t	t	t
2644	12568	CNG	A	690	t	t	2018-03-30	2021-09-16 09:15:46.500082+00	1451	t	f	t
2645	12569	CNG	A	1679	t	f	2018-03-23	2021-09-16 09:15:46.503144+00	1451	f	f	t
2646	12570	CNG	A	1634	t	f	2018-03-10	2021-09-16 09:15:46.5063+00	1451	t	t	t
2647	12571	CNG	A	1241	t	t	2018-03-18	2021-09-16 09:15:46.509766+00	1451	f	t	t
2648	12572	CNG	A	774	t	t	2018-03-11	2021-09-16 09:15:46.513545+00	1451	f	f	t
2649	12573	CNG	A	1049	t	f	2018-03-15	2021-09-16 09:15:46.517211+00	1451	t	f	t
2650	12574	CNG	A	697	f	f	2018-03-05	2021-09-16 09:15:46.520413+00	1451	t	t	f
2651	12575	CNG	A	653	f	t	2018-03-10	2021-09-16 09:15:46.523621+00	1451	f	f	f
2652	12576	CNG	A	1908	t	f	2018-03-10	2021-09-16 09:15:46.526476+00	1451	t	f	f
2653	12577	CNG	A	2445	f	f	2018-03-14	2021-09-16 09:15:46.529855+00	1451	f	t	t
2654	12578	CNG	A	613	t	f	2018-03-21	2021-09-16 09:15:46.533122+00	1451	f	t	t
2655	12579	CNG	A	1560	t	f	2018-03-21	2021-09-16 09:15:46.536087+00	1451	t	t	t
2656	12580	CNG	A	1651	f	t	2018-03-22	2021-09-16 09:15:46.539267+00	1451	f	t	f
2657	12581	CNG	A	2490	t	t	2018-03-03	2021-09-16 09:15:46.54281+00	1451	f	t	t
2658	12582	CNG	A	1669	f	f	2018-03-10	2021-09-16 09:15:46.546245+00	1451	f	f	t
2659	12583	CNG	A	1774	t	f	2018-03-09	2021-09-16 09:15:46.549626+00	1451	f	f	t
2660	12584	CNG	A	1556	t	f	2018-03-28	2021-09-16 09:15:46.552463+00	1451	t	f	t
2661	12585	CNG	A	890	t	t	2018-03-08	2021-09-16 09:15:46.554944+00	1451	f	f	t
2662	12586	CNG	A	1169	f	f	2018-03-09	2021-09-16 09:15:46.557517+00	1451	t	t	f
2663	12587	CNG	A	1479	f	f	2018-03-20	2021-09-16 09:15:46.560289+00	1451	t	t	t
2664	12588	CNG	A	1103	f	f	2018-03-25	2021-09-16 09:15:46.563203+00	1451	f	t	f
2665	12589	CNG	A	1747	t	t	2018-03-25	2021-09-16 09:15:46.565901+00	1451	t	f	t
2666	12590	CNG	A	2047	t	t	2018-03-22	2021-09-16 09:15:46.568941+00	1451	f	f	t
2667	12591	CNG	A	878	t	t	2018-03-08	2021-09-16 09:15:46.572082+00	1451	t	f	t
2668	12592	CNG	A	2073	t	t	2018-03-25	2021-09-16 09:15:46.578599+00	1451	t	f	f
2669	12593	CNG	A	519	f	t	2018-03-04	2021-09-16 09:15:46.583697+00	1451	f	f	t
2670	12594	CNG	A	1215	t	t	2018-03-21	2021-09-16 09:15:46.587675+00	1451	f	f	f
2671	12595	CNG	A	1821	f	t	2018-03-25	2021-09-16 09:15:46.590686+00	1451	f	f	t
2672	12596	CNG	A	598	t	f	2018-12-26	2021-09-16 09:15:46.593661+00	1451	t	f	t
2673	12597	CNG	A	1768	f	f	2018-12-10	2021-09-16 09:15:46.596363+00	1451	t	f	f
2674	12598	CNG	A	1821	t	f	2018-12-10	2021-09-16 09:15:46.59948+00	1451	t	f	f
2675	12599	CNG	A	2093	f	f	2018-12-27	2021-09-16 09:15:46.602668+00	1451	f	f	t
2676	12600	CNG	A	727	f	t	2018-12-01	2021-09-16 09:15:46.605855+00	1451	f	t	t
2677	12601	CNG	A	2038	t	t	2018-12-23	2021-09-16 09:15:46.609461+00	1451	f	f	f
2678	12602	CNG	A	1830	t	t	2018-12-26	2021-09-16 09:15:46.612458+00	1451	t	f	f
2679	12603	CNG	A	1607	f	t	2018-12-25	2021-09-16 09:15:46.615233+00	1451	t	f	f
2680	12604	CNG	A	1591	t	f	2018-12-06	2021-09-16 09:15:46.618103+00	1451	t	t	f
2681	12605	CNG	A	856	t	t	2018-12-20	2021-09-16 09:15:46.620952+00	1451	f	t	f
2682	12606	CNG	A	2135	t	f	2018-12-25	2021-09-16 09:15:46.623771+00	1451	t	t	f
2683	12607	CNG	A	854	t	t	2018-12-30	2021-09-16 09:15:46.626683+00	1451	f	f	f
2684	12608	CNG	A	2202	t	f	2018-12-07	2021-09-16 09:15:46.629378+00	1451	f	t	t
2685	12609	CNG	A	1282	f	f	2018-12-01	2021-09-16 09:15:46.632481+00	1451	t	t	t
2686	12610	CNG	A	1975	t	t	2018-12-01	2021-09-16 09:15:46.635967+00	1451	t	f	f
2687	12611	CNG	A	1390	t	f	2018-12-12	2021-09-16 09:15:46.639619+00	1451	f	t	t
2688	12612	CNG	A	642	f	t	2018-12-19	2021-09-16 09:15:46.642812+00	1451	f	f	t
2689	12613	CNG	A	1453	t	f	2018-12-09	2021-09-16 09:15:46.645898+00	1451	f	t	f
2690	12614	CNG	A	2031	f	f	2018-12-11	2021-09-16 09:15:46.648821+00	1451	t	t	f
2691	12615	CNG	A	2212	t	f	2018-12-06	2021-09-16 09:15:46.651744+00	1451	t	f	f
2692	12616	CNG	A	902	f	f	2018-12-03	2021-09-16 09:15:46.654554+00	1451	t	t	t
2693	12617	CNG	A	2365	f	t	2018-12-28	2021-09-16 09:15:46.657316+00	1451	f	t	t
2694	12618	CNG	A	733	t	t	2018-12-25	2021-09-16 09:15:46.662666+00	1451	f	f	t
2695	12619	CNG	A	2266	t	t	2018-12-09	2021-09-16 09:15:46.668089+00	1451	f	t	f
2696	12620	CNG	A	2373	t	t	2018-03-17	2021-09-16 09:15:46.672658+00	1451	t	f	f
2697	12621	CNG	A	776	t	f	2018-03-03	2021-09-16 09:15:46.676794+00	1451	f	f	f
2698	12622	CNG	A	1729	t	t	2018-03-28	2021-09-16 09:15:46.679994+00	1451	f	f	f
2699	12623	CNG	A	2437	f	t	2018-03-30	2021-09-16 09:15:46.683226+00	1451	t	t	t
2700	12624	CNG	A	1173	f	f	2018-03-26	2021-09-16 09:15:46.68643+00	1451	f	t	f
2701	12625	CNG	A	751	f	t	2018-03-01	2021-09-16 09:15:46.689218+00	1451	t	t	t
2702	12626	CNG	A	2499	t	t	2018-03-21	2021-09-16 09:15:46.692334+00	1451	f	f	f
2703	12627	CNG	A	2292	t	f	2018-03-23	2021-09-16 09:15:46.695505+00	1451	f	f	f
2704	12628	CNG	A	1359	f	f	2018-03-08	2021-09-16 09:15:46.698945+00	1451	f	t	t
2705	12629	CNG	A	613	f	f	2018-03-08	2021-09-16 09:15:46.702447+00	1451	f	f	t
2706	12630	CNG	A	694	t	t	2018-03-26	2021-09-16 09:15:46.705948+00	1451	t	f	t
2707	12631	CNG	A	585	t	f	2018-03-21	2021-09-16 09:15:46.70949+00	1451	f	f	t
2708	12632	CNG	A	1965	f	t	2018-03-01	2021-09-16 09:15:46.712556+00	1451	f	f	f
2709	12633	CNG	A	1414	f	f	2018-03-10	2021-09-16 09:15:46.715567+00	1451	f	f	f
2710	12634	CNG	A	1015	f	f	2018-03-16	2021-09-16 09:15:46.718456+00	1451	f	f	t
2711	12635	CNG	A	1074	f	f	2018-03-28	2021-09-16 09:15:46.721149+00	1451	f	f	t
2712	12636	CNG	A	1583	t	t	2018-03-24	2021-09-16 09:15:46.723796+00	1451	f	t	f
2713	12637	CNG	A	873	f	f	2018-03-11	2021-09-16 09:15:46.726352+00	1451	f	f	f
2714	12638	CNG	A	1650	f	t	2018-03-03	2021-09-16 09:15:46.729615+00	1451	t	t	f
2715	12639	CNG	A	2104	t	t	2018-03-24	2021-09-16 09:15:46.733206+00	1451	t	f	t
2716	12640	CNG	A	1328	f	f	2018-03-16	2021-09-16 09:15:46.736596+00	1451	t	t	t
2717	12641	CNG	A	2390	t	f	2018-03-20	2021-09-16 09:15:46.739472+00	1451	f	f	f
2718	12642	CNG	A	948	f	f	2018-03-11	2021-09-16 09:15:46.742061+00	1451	f	f	t
2719	12643	CNG	A	1116	t	t	2018-03-06	2021-09-16 09:15:46.744855+00	1451	f	f	t
2720	12644	CNG	A	1101	t	f	2018-03-02	2021-09-16 09:15:46.747665+00	1451	t	t	f
2721	12645	CNG	A	1302	t	t	2018-04-24	2021-09-16 09:15:46.750672+00	1451	t	t	f
2722	12646	CNG	A	1895	f	t	2018-04-04	2021-09-16 09:15:46.754397+00	1451	t	t	f
2723	12647	CNG	A	1601	f	t	2018-04-24	2021-09-16 09:15:46.757539+00	1451	f	f	f
2724	12648	CNG	A	1110	t	f	2018-04-24	2021-09-16 09:15:46.760765+00	1451	f	t	t
2725	12649	CNG	A	2049	t	f	2018-04-06	2021-09-16 09:15:46.763857+00	1451	f	t	t
2726	12650	CNG	A	598	t	f	2018-04-17	2021-09-16 09:15:46.767212+00	1451	t	f	f
2727	12651	CNG	A	1023	f	t	2018-04-28	2021-09-16 09:15:46.770182+00	1451	f	t	f
2728	12652	CNG	A	1638	t	t	2018-04-12	2021-09-16 09:15:46.772802+00	1451	t	t	f
2729	12653	CNG	A	2076	t	t	2018-04-11	2021-09-16 09:15:46.775351+00	1451	f	t	f
2730	12654	CNG	A	738	f	t	2018-04-25	2021-09-16 09:15:46.777939+00	1451	t	f	t
2731	12655	CNG	A	1869	f	f	2018-04-13	2021-09-16 09:15:46.780439+00	1451	f	f	t
2732	12656	CNG	A	2455	t	f	2018-04-22	2021-09-16 09:15:46.782821+00	1451	t	t	f
2733	12657	CNG	A	1506	f	f	2018-04-06	2021-09-16 09:15:46.785606+00	1451	t	f	t
2734	12658	CNG	A	1615	t	f	2018-04-19	2021-09-16 09:15:46.788431+00	1451	t	f	f
2735	12659	CNG	A	2399	t	f	2018-04-23	2021-09-16 09:15:46.791314+00	1451	t	f	f
2736	12660	CNG	A	761	f	t	2018-04-16	2021-09-16 09:15:46.794571+00	1451	f	f	f
2737	12661	CNG	A	1553	t	f	2018-04-19	2021-09-16 09:15:46.79828+00	1451	t	t	f
2738	12662	CNG	A	1873	f	t	2018-04-19	2021-09-16 09:15:46.801802+00	1451	f	f	t
2739	12663	CNG	A	2190	f	t	2018-04-24	2021-09-16 09:15:46.807608+00	1451	t	t	t
2740	12664	CNG	A	1150	t	f	2018-04-22	2021-09-16 09:15:46.81255+00	1451	f	f	f
2741	12665	CNG	A	2495	t	t	2018-04-06	2021-09-16 09:15:46.81664+00	1451	t	t	t
2742	12666	CNG	A	1211	t	f	2018-04-10	2021-09-16 09:15:46.823035+00	1451	t	f	t
2743	12667	CNG	A	1944	f	f	2018-04-22	2021-09-16 09:15:46.828053+00	1451	f	f	f
2744	12668	CNG	A	795	f	t	2018-04-06	2021-09-16 09:15:46.832973+00	1451	f	t	t
2745	12669	CNG	A	1726	f	t	2018-04-23	2021-09-16 09:15:46.836475+00	1451	t	t	t
2746	12670	CNG	A	1412	t	f	2018-04-26	2021-09-16 09:15:46.839952+00	1451	t	t	f
2747	12671	CNG	A	681	t	f	2018-04-19	2021-09-16 09:15:46.843174+00	1451	t	f	t
2748	12672	CNG	A	1428	t	t	2018-04-08	2021-09-16 09:15:46.845848+00	1451	f	f	f
2749	12673	CNG	A	1891	t	f	2018-04-08	2021-09-16 09:15:46.848594+00	1451	t	t	f
2750	12674	CNG	A	621	t	t	2018-04-11	2021-09-16 09:15:46.85112+00	1451	f	t	f
2751	12675	CNG	A	1475	f	f	2018-04-12	2021-09-16 09:15:46.853632+00	1451	t	f	f
2752	12676	CNG	A	2205	f	t	2018-04-29	2021-09-16 09:15:46.856452+00	1451	t	t	f
2753	12677	CNG	A	555	t	f	2018-04-25	2021-09-16 09:15:46.859763+00	1451	t	f	f
2754	12678	CNG	A	2437	f	t	2018-04-18	2021-09-16 09:15:46.863406+00	1451	t	t	f
2755	12679	CNG	A	1837	f	f	2018-04-26	2021-09-16 09:15:46.866478+00	1451	t	t	t
2756	12680	CNG	A	1798	t	f	2018-04-05	2021-09-16 09:15:46.869636+00	1451	f	t	t
2757	12681	CNG	A	1759	f	f	2018-04-28	2021-09-16 09:15:46.872363+00	1451	t	f	f
2758	12682	CNG	A	2125	f	f	2018-04-07	2021-09-16 09:15:46.874991+00	1451	f	f	t
2759	12683	CNG	A	1429	f	t	2018-04-01	2021-09-16 09:15:46.877635+00	1451	t	t	f
2760	12684	CNG	A	548	t	f	2018-04-27	2021-09-16 09:15:46.88025+00	1451	t	t	t
2761	12685	CNG	A	2081	t	t	2018-04-07	2021-09-16 09:15:46.88303+00	1451	f	f	t
2762	12686	CNG	A	940	t	f	2018-04-17	2021-09-16 09:15:46.886036+00	1451	f	t	f
2763	12687	CNG	A	1835	t	t	2018-04-20	2021-09-16 09:15:46.889015+00	1451	f	t	t
2764	12688	CNG	A	744	t	f	2018-04-14	2021-09-16 09:15:46.892108+00	1451	f	t	f
2765	12689	CNG	A	1193	f	f	2018-04-15	2021-09-16 09:15:46.898323+00	1451	f	t	t
2766	12690	CNG	A	1328	f	f	2018-04-05	2021-09-16 09:15:46.903741+00	1451	f	t	f
2767	12691	CNG	A	1752	t	t	2018-04-10	2021-09-16 09:15:46.907769+00	1451	t	t	t
2768	12692	CNG	A	614	t	f	2018-04-23	2021-09-16 09:15:46.911676+00	1451	t	t	f
2769	12693	CNG	A	644	f	f	2018-04-28	2021-09-16 09:15:46.915561+00	1451	f	t	t
2770	12694	CNG	A	2362	f	t	2018-04-03	2021-09-16 09:15:46.919708+00	1451	f	f	t
2771	12695	CNG	A	1450	t	f	2018-04-21	2021-09-16 09:15:46.924214+00	1451	f	f	f
2772	12696	CNG	A	2032	f	t	2018-04-02	2021-09-16 09:15:46.928883+00	1451	f	f	t
2773	12697	CNG	A	516	f	f	2018-04-12	2021-09-16 09:15:46.933198+00	1451	f	f	f
2774	12698	CNG	A	513	f	t	2018-04-30	2021-09-16 09:15:46.93724+00	1451	f	f	t
2775	12699	CNG	A	677	t	t	2018-04-25	2021-09-16 09:15:46.940909+00	1451	t	t	t
2776	12700	CNG	A	2050	t	f	2018-04-23	2021-09-16 09:15:46.946618+00	1452	t	t	f
2777	12701	CNG	A	785	f	t	2018-04-10	2021-09-16 09:15:46.952062+00	1453	t	f	f
2778	12702	CNG	A	1770	t	t	2018-04-12	2021-09-16 09:15:46.963358+00	1454	f	t	f
2779	12703	CNG	A	2437	f	f	2018-04-03	2021-09-16 09:15:46.97359+00	1455	f	t	t
2780	12704	CNG	A	595	f	f	2018-04-14	2021-09-16 09:15:46.98313+00	1456	t	f	t
2781	12705	CNG	A	1868	t	t	2018-04-17	2021-09-16 09:15:46.990101+00	1457	t	f	f
2782	12706	CNG	A	1300	f	t	2018-04-01	2021-09-16 09:15:46.995914+00	1458	f	t	f
2783	12707	CNG	A	1830	f	f	2018-04-26	2021-09-16 09:15:47.001738+00	1459	t	f	t
2784	12708	CNG	A	1893	f	t	2018-04-04	2021-09-16 09:15:47.006292+00	1460	t	f	t
2785	12709	CNG	A	1256	f	t	2018-04-29	2021-09-16 09:15:47.010714+00	1461	f	f	t
2786	12710	CNG	A	839	t	f	2018-04-29	2021-09-16 09:15:47.015622+00	1462	t	f	f
2787	12711	CNG	A	1954	f	f	2018-04-16	2021-09-16 09:15:47.02087+00	1463	t	f	f
2788	12712	CNG	A	2001	t	t	2018-04-12	2021-09-16 09:15:47.025747+00	1464	f	f	f
2789	12713	CNG	A	1375	t	t	2018-04-04	2021-09-16 09:15:47.03011+00	1465	t	t	t
2790	12714	CNG	A	1134	f	f	2018-04-25	2021-09-16 09:15:47.034753+00	1466	f	t	f
2791	12715	CNG	A	2239	f	f	2018-04-25	2021-09-16 09:15:47.039197+00	1467	f	f	f
2792	12716	CNG	A	2445	t	f	2018-04-15	2021-09-16 09:15:47.043484+00	1468	t	f	f
2793	12717	CNG	A	1095	f	f	2018-04-13	2021-09-16 09:15:47.048353+00	1469	f	t	f
2794	12718	CNG	A	2326	t	f	2018-04-30	2021-09-16 09:15:47.053997+00	1470	f	f	t
2795	12719	CNG	A	1169	f	f	2018-04-06	2021-09-16 09:15:47.059432+00	1471	t	f	f
2796	12720	CNG	A	1186	t	f	2018-04-22	2021-09-16 09:15:47.067806+00	1472	f	t	f
2797	12721	CNG	A	1362	f	t	2018-04-11	2021-09-16 09:15:47.076737+00	1473	f	f	f
2798	12722	CNG	A	1091	f	t	2018-04-28	2021-09-16 09:15:47.084311+00	1474	t	f	f
2799	12723	CNG	A	1133	f	f	2018-04-17	2021-09-16 09:15:47.090391+00	1475	t	f	f
2800	12724	CNG	A	1657	t	t	2018-04-22	2021-09-16 09:15:47.094752+00	1476	t	f	f
2801	12725	CNG	A	1265	f	f	2018-04-17	2021-09-16 09:15:47.099422+00	1477	f	f	f
2802	12726	CNG	A	2162	t	f	2018-04-14	2021-09-16 09:15:47.106437+00	1478	f	f	f
2803	12727	CNG	A	1675	f	t	2018-04-28	2021-09-16 09:15:47.112475+00	1479	t	f	t
2804	12728	CNG	A	1066	t	t	2018-04-08	2021-09-16 09:15:47.118323+00	1480	f	t	t
2805	12729	CNG	A	1008	t	f	2018-04-06	2021-09-16 09:15:47.122547+00	1481	t	t	f
2806	12730	CNG	A	508	f	f	2018-04-14	2021-09-16 09:15:47.12662+00	1482	t	f	t
2807	12731	CNG	A	708	t	f	2018-04-25	2021-09-16 09:15:47.130822+00	1483	t	f	f
2808	12732	CNG	A	2129	t	t	2018-04-10	2021-09-16 09:15:47.135701+00	1484	t	t	t
2809	12733	CNG	A	1633	f	t	2018-04-12	2021-09-16 09:15:47.141861+00	1485	t	t	f
2810	12734	CNG	A	954	t	t	2018-04-07	2021-09-16 09:15:47.149906+00	1486	f	f	t
2811	12735	CNG	A	1413	t	t	2018-04-06	2021-09-16 09:15:47.155078+00	1487	t	f	t
2812	12736	CNG	A	1674	f	f	2018-04-18	2021-09-16 09:15:47.160039+00	1488	f	t	t
2813	12737	CNG	A	2246	t	t	2018-04-03	2021-09-16 09:15:47.164945+00	1489	f	t	f
2814	12738	CNG	A	1700	f	t	2018-04-23	2021-09-16 09:15:47.16956+00	1490	t	t	f
2815	12739	CNG	A	782	t	t	2018-04-07	2021-09-16 09:15:47.174517+00	1491	t	t	t
2816	12740	CNG	A	2069	f	f	2018-04-05	2021-09-16 09:15:47.179315+00	1492	f	t	t
2817	12741	CNG	A	1932	t	t	2018-04-02	2021-09-16 09:15:47.18434+00	1493	f	f	t
2818	12742	CNG	A	1389	t	t	2018-04-23	2021-09-16 09:15:47.189565+00	1494	t	f	f
2819	12743	CNG	A	1378	f	f	2018-04-05	2021-09-16 09:15:47.194541+00	1495	t	t	f
2820	12744	CNG	A	1142	t	t	2018-04-24	2021-09-16 09:15:47.199133+00	1496	f	f	t
2821	12745	CNG	A	1305	t	t	2018-04-11	2021-09-16 09:15:47.204935+00	1497	t	f	f
2822	12746	CNG	A	790	t	f	2018-04-27	2021-09-16 09:15:47.210345+00	1498	t	t	t
2823	12747	CNG	A	811	f	t	2018-04-30	2021-09-16 09:15:47.214959+00	1499	f	f	t
2824	12748	CNG	A	1860	f	t	2018-04-26	2021-09-16 09:15:47.220222+00	1500	t	t	f
2825	12749	CNG	A	1858	f	t	2018-04-06	2021-09-16 09:15:47.224616+00	1501	f	f	f
2826	12750	CNG	A	1981	t	f	2018-04-16	2021-09-16 09:15:47.229404+00	1502	t	t	t
2827	12751	CNG	A	1654	f	t	2018-04-11	2021-09-16 09:15:47.234482+00	1503	f	f	f
2828	12752	CNG	A	2449	f	t	2018-04-19	2021-09-16 09:15:47.239468+00	1504	f	t	t
2829	12753	CNG	A	2087	t	t	2018-04-13	2021-09-16 09:15:47.24466+00	1505	f	f	f
2830	12754	CNG	A	1815	f	t	2018-04-29	2021-09-16 09:15:47.249817+00	1506	t	t	f
2831	12755	CNG	A	781	t	t	2018-04-26	2021-09-16 09:15:47.255175+00	1507	t	t	t
2832	12756	CNG	A	2428	t	f	2018-04-05	2021-09-16 09:15:47.259803+00	1508	f	f	f
2833	12757	CNG	A	587	f	t	2018-04-27	2021-09-16 09:15:47.26425+00	1509	t	f	t
2834	12758	CNG	A	1897	t	f	2018-04-02	2021-09-16 09:15:47.269946+00	1510	t	f	t
2835	12759	CNG	A	517	f	t	2018-04-29	2021-09-16 09:15:47.275228+00	1511	t	t	f
2836	12760	CNG	A	1705	f	t	2018-04-02	2021-09-16 09:15:47.279667+00	1512	t	f	t
2837	12761	CNG	A	912	t	f	2018-04-07	2021-09-16 09:15:47.284614+00	1513	t	f	t
2838	12762	CNG	A	2204	t	t	2018-04-02	2021-09-16 09:15:47.289306+00	1514	f	f	t
2839	12763	CNG	A	2207	f	t	2018-04-02	2021-09-16 09:15:47.293849+00	1515	f	f	t
2840	12764	CNG	A	1943	f	f	2018-04-23	2021-09-16 09:15:47.298897+00	1516	t	f	t
2841	12765	CNG	A	1625	f	t	2018-04-29	2021-09-16 09:15:47.304083+00	1517	t	f	t
2842	12766	CNG	A	762	t	f	2018-04-30	2021-09-16 09:15:47.308253+00	1518	t	f	t
2843	12767	CNG	A	1595	f	f	2018-04-13	2021-09-16 09:15:47.312476+00	1519	t	f	f
2844	12768	CNG	A	625	f	f	2018-04-27	2021-09-16 09:15:47.317079+00	1520	f	f	f
2845	12769	CNG	A	1484	f	f	2018-04-09	2021-09-16 09:15:47.321962+00	1521	f	f	f
2846	12770	CNG	A	1604	t	t	2018-04-30	2021-09-16 09:15:47.326838+00	1522	f	t	t
2847	12771	CNG	A	1046	f	f	2018-04-08	2021-09-16 09:15:47.331995+00	1523	t	t	t
2848	12772	CNG	A	694	f	f	2018-04-06	2021-09-16 09:15:47.336618+00	1524	f	t	t
2849	12773	CNG	A	1248	t	t	2018-04-17	2021-09-16 09:15:47.341453+00	1525	f	t	t
2850	12774	CNG	A	2111	t	f	2018-04-02	2021-09-16 09:15:47.346075+00	1526	f	t	t
2851	12775	CNG	A	1648	f	t	2018-04-04	2021-09-16 09:15:47.350921+00	1527	t	t	t
2852	12776	CNG	A	630	f	f	2018-04-21	2021-09-16 09:15:47.356385+00	1528	f	f	f
2853	12777	CNG	A	1829	f	f	2018-04-19	2021-09-16 09:15:47.361468+00	1529	f	f	f
2854	12778	CNG	A	2113	f	f	2018-04-21	2021-09-16 09:15:47.366711+00	1530	f	f	f
2855	12779	CNG	A	734	t	t	2018-04-18	2021-09-16 09:15:47.371931+00	1531	t	f	f
2856	12780	CNG	A	2177	t	f	2018-04-08	2021-09-16 09:15:47.380129+00	1532	f	t	t
2857	12781	CNG	A	1923	f	f	2018-04-07	2021-09-16 09:15:47.387088+00	1533	f	t	t
2858	12782	CNG	A	1340	f	t	2018-04-18	2021-09-16 09:15:47.394237+00	1534	f	t	f
2859	12783	CNG	A	1282	t	f	2018-04-15	2021-09-16 09:15:47.400343+00	1535	t	f	f
2860	12784	CNG	A	2479	t	t	2018-04-05	2021-09-16 09:15:47.405743+00	1536	t	t	f
2861	12785	CNG	A	1206	f	t	2018-04-11	2021-09-16 09:15:47.410368+00	1537	t	f	t
2862	12786	CNG	A	1116	t	f	2018-04-23	2021-09-16 09:15:47.413087+00	1537	f	t	f
2863	12787	CNG	A	1845	f	t	2018-04-24	2021-09-16 09:15:47.415706+00	1537	t	f	f
2864	12788	CNG	A	600	f	t	2018-04-16	2021-09-16 09:15:47.418931+00	1537	f	t	f
2865	12789	CNG	A	812	t	t	2018-04-01	2021-09-16 09:15:47.422257+00	1537	f	f	t
2866	12790	CNG	A	2278	t	t	2018-04-26	2021-09-16 09:15:47.4258+00	1537	t	f	f
2867	12791	CNG	A	1488	t	f	2018-04-05	2021-09-16 09:15:47.428726+00	1537	f	f	t
2868	12792	CNG	A	1445	f	t	2018-04-28	2021-09-16 09:15:47.43192+00	1537	f	t	f
2869	12793	CNG	A	1677	t	f	2018-04-15	2021-09-16 09:15:47.438343+00	1537	f	f	f
2870	12794	CNG	A	2143	f	f	2018-04-07	2021-09-16 09:15:47.444591+00	1537	f	t	f
2871	12795	CNG	A	1124	f	t	2018-04-09	2021-09-16 09:15:47.451858+00	1537	t	f	t
2872	12796	CNG	A	2076	t	f	2018-04-29	2021-09-16 09:15:47.457195+00	1537	f	t	f
2873	12797	CNG	A	614	f	f	2018-04-20	2021-09-16 09:15:47.461981+00	1537	f	t	f
2874	12798	CNG	A	854	t	f	2018-04-22	2021-09-16 09:15:47.467998+00	1537	f	t	t
2875	12799	CNG	A	861	t	f	2018-04-22	2021-09-16 09:15:47.473434+00	1537	f	f	f
2876	12800	CNG	A	2323	t	f	2018-04-17	2021-09-16 09:15:47.477196+00	1537	t	f	f
2877	12801	CNG	A	508	f	f	2018-04-29	2021-09-16 09:15:47.480573+00	1537	t	t	f
2878	12802	CNG	A	1484	t	f	2018-04-12	2021-09-16 09:15:47.48415+00	1537	t	f	t
2879	12803	CNG	A	1273	f	t	2018-04-21	2021-09-16 09:15:47.487651+00	1537	f	f	t
2880	12804	CNG	A	796	f	t	2018-04-15	2021-09-16 09:15:47.491351+00	1537	t	f	f
2881	12805	CNG	A	681	f	t	2018-04-26	2021-09-16 09:15:47.494408+00	1537	t	f	t
2882	12806	CNG	A	1639	t	t	2018-04-03	2021-09-16 09:15:47.497242+00	1537	t	t	t
2883	12807	CNG	A	791	f	f	2018-04-15	2021-09-16 09:15:47.502118+00	1537	t	t	t
2884	12808	CNG	A	845	t	t	2018-04-06	2021-09-16 09:15:47.505229+00	1537	f	t	t
2885	12809	CNG	A	920	f	t	2018-04-08	2021-09-16 09:15:47.508547+00	1537	t	f	f
2886	12810	CNG	A	2123	f	f	2018-04-25	2021-09-16 09:15:47.511941+00	1537	t	t	f
2887	12811	CNG	A	1973	t	f	2018-04-07	2021-09-16 09:15:47.517427+00	1537	f	f	f
2888	12812	CNG	A	2234	f	f	2018-04-16	2021-09-16 09:15:47.524221+00	1537	t	t	t
2889	12813	CNG	A	1988	f	f	2018-04-14	2021-09-16 09:15:47.528228+00	1537	f	f	f
2890	12814	CNG	A	2366	t	t	2018-04-16	2021-09-16 09:15:47.532057+00	1537	f	f	f
2891	12815	CNG	A	1773	t	f	2018-04-15	2021-09-16 09:15:47.536355+00	1537	f	f	t
2892	12816	CNG	A	1017	t	f	2018-04-09	2021-09-16 09:15:47.539812+00	1537	f	f	t
2893	12817	CNG	A	1008	t	t	2018-04-08	2021-09-16 09:15:47.543193+00	1537	f	t	t
2894	12818	CNG	A	1088	t	f	2018-04-21	2021-09-16 09:15:47.547194+00	1537	t	t	f
2895	12819	CNG	A	923	t	f	2018-04-08	2021-09-16 09:15:47.554857+00	1537	f	t	t
2896	12820	CNG	A	813	t	f	2018-04-05	2021-09-16 09:15:47.558744+00	1537	f	t	t
2897	12821	CNG	A	1982	t	t	2018-04-18	2021-09-16 09:15:47.562943+00	1537	t	t	t
2898	12822	CNG	A	549	t	t	2018-04-20	2021-09-16 09:15:47.566485+00	1537	t	t	f
2899	12823	CNG	A	1389	f	f	2018-04-22	2021-09-16 09:15:47.573272+00	1537	f	t	t
2900	12824	CNG	A	2268	f	f	2018-04-19	2021-09-16 09:15:47.578222+00	1537	t	t	f
2901	12825	CNG	A	770	t	t	2018-04-21	2021-09-16 09:15:47.58237+00	1537	f	t	f
2902	12826	CNG	A	887	t	f	2018-04-26	2021-09-16 09:15:47.586125+00	1537	t	f	t
2903	12827	CNG	A	1758	t	f	2018-04-17	2021-09-16 09:15:47.590351+00	1537	t	f	t
2904	12828	CNG	A	1018	t	f	2018-04-12	2021-09-16 09:15:47.594441+00	1537	f	t	t
2905	12829	CNG	A	2162	t	t	2018-04-09	2021-09-16 09:15:47.598638+00	1537	t	f	f
2906	12830	CNG	A	860	f	t	2018-04-21	2021-09-16 09:15:47.603443+00	1537	t	t	t
2907	12831	CNG	A	1319	f	t	2018-04-24	2021-09-16 09:15:47.608869+00	1537	f	f	t
2908	12832	CNG	A	1918	t	t	2018-04-08	2021-09-16 09:15:47.615037+00	1537	t	f	t
2909	12833	CNG	A	732	f	t	2018-04-04	2021-09-16 09:15:47.620936+00	1537	t	f	t
2910	12834	CNG	A	1019	t	t	2018-04-01	2021-09-16 09:15:47.624392+00	1537	t	f	f
2911	12835	CNG	A	2415	f	t	2018-04-26	2021-09-16 09:15:47.628813+00	1537	f	f	t
2912	12836	CNG	A	1538	t	f	2018-04-18	2021-09-16 09:15:47.633307+00	1537	t	f	t
2913	12837	CNG	A	2279	t	f	2018-04-20	2021-09-16 09:15:47.640454+00	1537	t	f	t
2914	12838	CNG	A	2218	f	f	2018-04-26	2021-09-16 09:15:47.644568+00	1537	f	f	f
2915	12839	CNG	A	1653	t	f	2018-04-20	2021-09-16 09:15:47.649632+00	1537	f	f	t
2916	12840	CNG	A	2436	t	t	2018-04-09	2021-09-16 09:15:47.653831+00	1537	f	t	t
2917	12841	CNG	A	1673	f	f	2018-04-19	2021-09-16 09:15:47.657845+00	1537	t	f	f
2918	12842	CNG	A	2332	f	f	2018-04-03	2021-09-16 09:15:47.662617+00	1537	t	t	f
2919	12843	CNG	A	959	f	f	2018-04-19	2021-09-16 09:15:47.666617+00	1537	f	t	t
2920	12844	CNG	A	837	t	f	2018-04-27	2021-09-16 09:15:47.671203+00	1537	t	t	f
2921	12845	CNG	A	2434	f	f	2018-05-02	2021-09-16 09:15:47.675+00	1537	t	f	t
2922	12846	CNG	A	1778	t	t	2018-05-08	2021-09-16 09:15:47.679733+00	1537	t	t	f
2923	12847	CNG	A	2417	f	f	2018-05-27	2021-09-16 09:15:47.683591+00	1537	t	t	t
2924	12848	CNG	A	1775	t	t	2018-05-07	2021-09-16 09:15:47.68751+00	1537	f	f	f
2925	12849	CNG	A	1210	f	t	2018-05-27	2021-09-16 09:15:47.691399+00	1537	t	f	f
2926	12850	CNG	A	568	f	t	2018-05-27	2021-09-16 09:15:47.69673+00	1537	t	t	t
2927	12851	CNG	A	724	t	f	2018-05-22	2021-09-16 09:15:47.701064+00	1537	t	f	t
2928	12852	CNG	A	1760	t	t	2018-05-02	2021-09-16 09:15:47.704994+00	1537	t	t	t
2929	12853	CNG	A	1345	t	t	2018-05-13	2021-09-16 09:15:47.710028+00	1537	t	f	t
2930	12854	CNG	A	2094	t	t	2018-05-05	2021-09-16 09:15:47.716656+00	1537	f	f	f
2931	12855	CNG	A	2241	t	t	2018-05-25	2021-09-16 09:15:47.725566+00	1537	f	f	t
2932	12856	CNG	A	1488	t	t	2018-05-03	2021-09-16 09:15:47.730406+00	1537	t	t	f
2933	12857	CNG	A	1028	t	t	2018-05-28	2021-09-16 09:15:47.734279+00	1537	t	t	t
2934	12858	CNG	A	1252	f	f	2018-05-09	2021-09-16 09:15:47.73814+00	1537	t	f	t
2935	12859	CNG	A	2445	f	f	2018-05-10	2021-09-16 09:15:47.741752+00	1537	f	f	t
2936	12860	CNG	A	1525	t	f	2018-05-11	2021-09-16 09:15:47.74816+00	1537	t	f	t
2937	12861	CNG	A	1157	t	f	2018-05-10	2021-09-16 09:15:47.751635+00	1537	t	f	f
2938	12862	CNG	A	902	t	f	2018-05-22	2021-09-16 09:15:47.755639+00	1537	t	f	f
2939	12863	CNG	A	2082	f	f	2018-05-01	2021-09-16 09:15:47.759776+00	1537	t	f	f
2940	12864	CNG	A	1723	t	t	2018-05-22	2021-09-16 09:15:47.763852+00	1537	t	t	f
2941	12865	CNG	A	1532	f	f	2018-05-06	2021-09-16 09:15:47.768196+00	1537	t	t	t
2942	12866	CNG	A	972	f	f	2018-05-14	2021-09-16 09:15:47.772124+00	1537	f	f	t
2943	12867	CNG	A	1089	f	t	2018-05-05	2021-09-16 09:15:47.775884+00	1537	t	t	f
2944	12868	CNG	A	2400	t	t	2018-05-11	2021-09-16 09:15:47.781812+00	1537	t	t	t
2945	12869	CNG	A	1667	t	t	2018-05-22	2021-09-16 09:15:47.785536+00	1537	t	f	t
2946	12870	CNG	A	908	t	t	2018-05-30	2021-09-16 09:15:47.789217+00	1537	t	t	f
2947	12871	CNG	A	1719	t	f	2018-05-10	2021-09-16 09:15:47.792793+00	1537	f	t	t
2948	12872	CNG	A	1148	f	t	2018-05-24	2021-09-16 09:15:47.797715+00	1537	f	f	t
2949	12873	CNG	A	1412	f	t	2018-05-15	2021-09-16 09:15:47.802493+00	1537	f	t	f
2950	12874	CNG	A	1291	t	f	2018-05-06	2021-09-16 09:15:47.806622+00	1537	t	f	f
2951	12875	CNG	A	1211	t	f	2018-05-06	2021-09-16 09:15:47.810382+00	1537	t	t	t
2952	12876	CNG	A	995	t	f	2018-05-07	2021-09-16 09:15:47.814276+00	1537	t	f	t
2953	12877	CNG	A	724	t	t	2018-05-30	2021-09-16 09:15:47.817563+00	1537	t	f	t
2954	12878	CNG	A	1177	t	f	2018-05-02	2021-09-16 09:15:47.82099+00	1537	t	t	f
2955	12879	CNG	A	2403	f	f	2018-05-12	2021-09-16 09:15:47.82475+00	1537	t	f	f
2956	12880	CNG	A	1134	f	t	2018-05-09	2021-09-16 09:15:47.82849+00	1537	f	f	f
2957	12881	CNG	A	2419	t	t	2018-05-13	2021-09-16 09:15:47.832674+00	1537	t	f	t
2958	12882	CNG	A	1049	f	t	2018-05-20	2021-09-16 09:15:47.836197+00	1537	t	t	t
2959	12883	CNG	A	1123	f	t	2018-05-06	2021-09-16 09:15:47.839167+00	1537	f	f	f
2960	12884	CNG	A	1571	t	f	2018-05-02	2021-09-16 09:15:47.842067+00	1537	f	t	t
2961	12885	CNG	A	1540	t	f	2018-05-17	2021-09-16 09:15:47.845297+00	1537	f	t	t
2962	12886	CNG	A	2154	t	t	2018-05-15	2021-09-16 09:15:47.849058+00	1537	t	t	f
2963	12887	CNG	A	1188	t	t	2018-05-12	2021-09-16 09:15:47.852494+00	1537	f	t	t
2964	12888	CNG	A	2465	t	t	2018-05-07	2021-09-16 09:15:47.856718+00	1537	f	f	t
2965	12889	CNG	A	1972	f	f	2018-05-08	2021-09-16 09:15:47.860578+00	1537	f	t	f
2966	12890	CNG	A	947	f	t	2018-05-21	2021-09-16 09:15:47.864668+00	1537	f	f	t
2967	12891	CNG	A	2032	f	t	2018-05-14	2021-09-16 09:15:47.868646+00	1537	t	t	f
2968	12892	CNG	A	1409	f	t	2018-05-12	2021-09-16 09:15:47.872549+00	1537	f	t	t
2969	12893	CNG	A	1353	f	t	2018-05-02	2021-09-16 09:15:47.87598+00	1537	t	t	f
2970	12894	CNG	A	2449	t	t	2018-05-11	2021-09-16 09:15:47.879308+00	1537	t	f	f
2971	12895	CNG	A	1602	t	f	2018-05-30	2021-09-16 09:15:47.883155+00	1537	f	t	t
2972	12896	CNG	A	1400	f	f	2018-05-19	2021-09-16 09:15:47.887281+00	1537	f	t	t
2973	12897	CNG	A	915	f	f	2018-05-29	2021-09-16 09:15:47.890664+00	1537	t	t	t
2974	12898	CNG	A	601	f	f	2018-05-02	2021-09-16 09:15:47.894402+00	1537	t	t	t
2975	12899	CNG	A	1898	t	f	2018-05-09	2021-09-16 09:15:47.898053+00	1537	f	f	f
2976	12900	CNG	A	1852	t	t	2018-05-14	2021-09-16 09:15:47.901683+00	1537	t	t	f
2977	12901	CNG	A	686	t	f	2018-05-16	2021-09-16 09:15:47.904963+00	1537	t	t	f
2978	12902	CNG	A	1914	f	t	2018-05-29	2021-09-16 09:15:47.911383+00	1537	f	t	t
2979	12903	CNG	A	689	f	t	2018-05-06	2021-09-16 09:15:47.918696+00	1537	f	t	t
2980	12904	CNG	A	776	t	t	2018-05-14	2021-09-16 09:15:47.926869+00	1537	t	f	f
2981	12905	CNG	A	1399	f	f	2018-05-15	2021-09-16 09:15:47.93437+00	1537	f	t	t
2982	12906	CNG	A	2344	f	f	2018-05-07	2021-09-16 09:15:47.941085+00	1537	f	t	t
2983	12907	CNG	A	1010	t	t	2018-05-24	2021-09-16 09:15:47.947424+00	1537	t	t	t
2984	12908	CNG	A	1988	f	t	2018-05-27	2021-09-16 09:15:47.953848+00	1537	t	t	f
2985	12909	CNG	A	634	f	t	2018-05-05	2021-09-16 09:15:47.961545+00	1537	f	f	t
2986	12910	CNG	A	2100	f	f	2018-05-12	2021-09-16 09:15:47.969203+00	1537	t	f	t
2987	12911	CNG	A	635	t	t	2018-05-15	2021-09-16 09:15:47.976279+00	1537	f	t	f
2988	12912	CNG	A	1829	t	t	2018-05-11	2021-09-16 09:15:47.982987+00	1537	t	f	t
2989	12913	CNG	A	582	t	f	2018-05-17	2021-09-16 09:15:47.99026+00	1537	t	t	f
2990	12914	CNG	A	1884	t	f	2018-05-30	2021-09-16 09:15:47.998036+00	1537	f	f	f
2991	12915	CNG	A	2203	f	t	2018-05-26	2021-09-16 09:15:48.005256+00	1537	f	f	t
2992	12916	CNG	A	946	t	f	2018-05-19	2021-09-16 09:15:48.011474+00	1537	t	f	f
2993	12917	CNG	A	716	f	t	2018-05-02	2021-09-16 09:15:48.017695+00	1537	t	t	t
2994	12918	CNG	A	1625	f	t	2018-05-13	2021-09-16 09:15:48.025154+00	1537	t	t	f
2995	12919	CNG	A	837	f	f	2018-05-11	2021-09-16 09:15:48.033514+00	1537	t	f	f
2996	12920	CNG	A	2469	t	t	2018-05-19	2021-09-16 09:15:48.048956+00	1537	f	t	t
2997	12921	CNG	A	1162	f	f	2018-05-17	2021-09-16 09:15:48.057763+00	1537	f	t	t
2998	12922	CNG	A	1565	f	f	2018-05-16	2021-09-16 09:15:48.06516+00	1537	t	f	t
2999	12923	CNG	A	2313	t	f	2018-05-18	2021-09-16 09:15:48.071403+00	1537	t	f	f
3000	12924	CNG	A	741	t	f	2018-05-12	2021-09-16 09:15:48.081339+00	1537	t	t	f
3001	12925	CNG	A	1845	t	f	2018-05-07	2021-09-16 09:15:48.087288+00	1537	f	f	t
3002	12926	CNG	A	1325	f	t	2018-05-21	2021-09-16 09:15:48.092126+00	1537	f	t	t
3003	12927	CNG	A	1570	t	t	2018-05-21	2021-09-16 09:15:48.096396+00	1537	f	t	t
3004	12928	CNG	A	2209	f	f	2018-05-17	2021-09-16 09:15:48.100193+00	1537	t	t	t
3005	12929	CNG	A	538	f	t	2018-05-24	2021-09-16 09:15:48.103997+00	1537	t	f	f
3006	12930	CNG	A	1895	t	t	2018-05-21	2021-09-16 09:15:48.107205+00	1537	t	t	t
3007	12931	CNG	A	2182	f	t	2018-05-30	2021-09-16 09:15:48.112242+00	1537	t	t	f
3008	12932	CNG	A	1483	t	t	2018-05-23	2021-09-16 09:15:48.115697+00	1537	f	t	f
3009	12933	CNG	A	1241	t	f	2018-05-05	2021-09-16 09:15:48.119354+00	1537	t	f	t
3010	12934	CNG	A	1256	f	f	2018-05-25	2021-09-16 09:15:48.123175+00	1537	f	f	t
3011	12935	CNG	A	2340	f	f	2018-05-17	2021-09-16 09:15:48.12765+00	1537	t	f	f
3012	12936	CNG	A	2365	f	t	2018-05-23	2021-09-16 09:15:48.131313+00	1537	f	f	t
3013	12937	CNG	A	651	t	t	2018-05-26	2021-09-16 09:15:48.134784+00	1537	f	f	f
3014	12938	CNG	A	2141	f	t	2018-05-30	2021-09-16 09:15:48.138902+00	1537	f	t	f
3015	12939	CNG	A	1358	f	t	2018-05-25	2021-09-16 09:15:48.142485+00	1537	t	t	t
3016	12940	CNG	A	2094	t	f	2018-05-16	2021-09-16 09:15:48.14722+00	1538	f	f	f
3017	12941	CNG	A	1962	f	t	2018-05-18	2021-09-16 09:15:48.152514+00	1539	t	f	t
3018	12942	CNG	A	2055	t	f	2018-05-30	2021-09-16 09:15:48.157819+00	1540	f	f	t
3019	12943	CNG	A	1542	t	f	2018-05-22	2021-09-16 09:15:48.163413+00	1541	t	t	t
3020	12944	CNG	A	1339	f	t	2018-05-19	2021-09-16 09:15:48.168932+00	1542	t	f	t
3021	12945	CNG	A	2284	t	f	2018-06-21	2021-09-16 09:15:48.173939+00	1543	f	t	t
3022	12946	CNG	A	1340	t	t	2018-06-29	2021-09-16 09:15:48.178508+00	1544	t	t	t
3023	12947	CNG	A	2041	f	f	2018-06-12	2021-09-16 09:15:48.183637+00	1545	f	t	f
3024	12948	CNG	A	861	t	f	2018-06-09	2021-09-16 09:15:48.188293+00	1546	t	f	f
3025	12949	CNG	B	1047	t	t	2018-06-22	2021-09-16 09:15:48.19353+00	1547	f	f	f
3026	12950	CNG	B	2001	t	f	2018-06-14	2021-09-16 09:15:48.198411+00	1548	f	f	f
3027	12951	CNG	B	1327	f	f	2018-06-26	2021-09-16 09:15:48.203474+00	1549	f	t	f
3028	12952	CNG	B	2367	f	f	2018-06-21	2021-09-16 09:15:48.208424+00	1550	f	t	t
3029	12953	CNG	B	1829	t	f	2018-06-27	2021-09-16 09:15:48.213815+00	1551	t	f	f
3030	12954	CNG	B	1744	f	f	2018-06-09	2021-09-16 09:15:48.218539+00	1552	t	t	t
3031	12955	CNG	B	878	f	t	2018-06-06	2021-09-16 09:15:48.223029+00	1553	f	f	f
3032	12956	Petrol	B	641	f	f	2018-06-11	2021-09-16 09:15:48.227908+00	1554	t	t	f
3033	12957	Petrol	B	1096	t	t	2018-06-26	2021-09-16 09:15:48.233257+00	1555	t	t	t
3034	12958	Petrol	B	1761	t	f	2018-06-21	2021-09-16 09:15:48.237724+00	1556	t	t	f
3035	12959	Petrol	B	875	t	f	2018-06-15	2021-09-16 09:15:48.242609+00	1557	t	t	f
3036	12960	Petrol	B	698	f	t	2018-06-01	2021-09-16 09:15:48.247278+00	1558	f	f	f
3037	12961	Petrol	B	1251	t	t	2018-06-30	2021-09-16 09:15:48.252036+00	1559	f	f	f
3038	12962	Petrol	B	1500	t	t	2018-06-17	2021-09-16 09:15:48.256477+00	1560	f	t	f
3039	12963	Petrol	B	2422	t	f	2018-06-30	2021-09-16 09:15:48.261153+00	1561	f	t	t
3040	12964	Petrol	B	1197	t	f	2018-06-22	2021-09-16 09:15:48.265374+00	1562	t	t	t
3041	12965	Petrol	B	1507	t	t	2018-06-01	2021-09-16 09:15:48.269824+00	1563	f	t	t
3042	12966	Petrol	B	609	f	f	2018-06-28	2021-09-16 09:15:48.274224+00	1564	f	t	t
3043	12967	Petrol	B	1339	t	f	2018-06-25	2021-09-16 09:15:48.27875+00	1565	f	t	t
3044	12968	Petrol	B	1730	f	f	2018-06-07	2021-09-16 09:15:48.283209+00	1566	f	f	t
3045	12969	Petrol	B	2164	t	t	2018-06-01	2021-09-16 09:15:48.287602+00	1567	t	t	t
3046	12970	Petrol	B	1791	f	f	2018-06-23	2021-09-16 09:15:48.29201+00	1568	f	t	t
3047	12971	Petrol	B	759	t	t	2018-06-18	2021-09-16 09:15:48.296384+00	1569	f	f	t
3048	12972	Petrol	B	1199	t	f	2018-06-14	2021-09-16 09:15:48.300976+00	1570	t	f	f
3049	12973	Petrol	B	1214	t	t	2018-06-28	2021-09-16 09:15:48.305508+00	1571	t	t	t
3050	12974	Petrol	B	2104	t	f	2018-06-13	2021-09-16 09:15:48.31002+00	1572	f	t	t
3051	12975	Petrol	B	538	t	f	2018-06-09	2021-09-16 09:15:48.314488+00	1573	f	t	t
3052	12976	Petrol	B	1951	f	f	2018-06-27	2021-09-16 09:15:48.31944+00	1574	f	f	t
3053	12977	Petrol	B	804	f	t	2018-06-18	2021-09-16 09:15:48.324178+00	1575	f	t	t
3054	12978	Petrol	B	1778	t	f	2018-06-08	2021-09-16 09:15:48.32902+00	1576	f	f	t
3055	12979	Petrol	B	1368	f	f	2018-06-22	2021-09-16 09:15:48.333884+00	1577	f	t	f
3056	12980	Petrol	B	1744	f	t	2018-06-21	2021-09-16 09:15:48.337934+00	1578	t	t	t
3057	12981	Petrol	B	844	t	f	2018-06-10	2021-09-16 09:15:48.342147+00	1579	f	t	f
3058	12982	Petrol	B	1769	t	t	2018-06-12	2021-09-16 09:15:48.346551+00	1580	t	t	t
3059	12983	Petrol	B	873	t	t	2018-06-02	2021-09-16 09:15:48.350972+00	1581	t	f	t
3060	12984	Petrol	B	2088	t	t	2018-06-07	2021-09-16 09:15:48.35508+00	1582	f	t	f
3061	12985	Petrol	B	520	f	t	2018-06-04	2021-09-16 09:15:48.359209+00	1583	t	f	f
3062	12986	Petrol	B	1642	t	t	2018-06-12	2021-09-16 09:15:48.364267+00	1584	t	t	f
3063	12987	Petrol	B	1261	t	t	2018-06-26	2021-09-16 09:15:48.368485+00	1585	f	t	t
3064	12988	Petrol	B	2266	f	f	2018-06-24	2021-09-16 09:15:48.372743+00	1586	t	f	t
3065	12989	Petrol	B	2302	f	f	2018-06-27	2021-09-16 09:15:48.377086+00	1587	t	t	t
3066	12990	Petrol	B	2191	f	t	2018-06-07	2021-09-16 09:15:48.38115+00	1588	f	t	t
3067	12991	Petrol	B	620	t	f	2018-06-29	2021-09-16 09:15:48.385392+00	1589	t	f	f
3068	12992	Petrol	B	874	t	t	2018-06-10	2021-09-16 09:15:48.389374+00	1590	f	t	t
3069	12993	Petrol	B	1217	t	f	2018-06-18	2021-09-16 09:15:48.393878+00	1591	f	t	t
3070	12994	Petrol	B	586	f	t	2018-06-14	2021-09-16 09:15:48.398826+00	1592	t	f	f
3071	12995	Petrol	B	972	f	t	2018-06-19	2021-09-16 09:15:48.403771+00	1593	f	t	t
3072	12996	Petrol	B	958	t	t	2018-06-17	2021-09-16 09:15:48.408764+00	1594	t	f	f
3073	12997	Petrol	B	1971	t	t	2018-06-16	2021-09-16 09:15:48.415436+00	1595	t	f	f
3074	12998	Petrol	B	1225	t	t	2018-06-04	2021-09-16 09:15:48.420516+00	1596	f	f	t
3075	12999	Petrol	B	1789	f	t	2018-06-25	2021-09-16 09:15:48.425717+00	1597	f	t	t
3076	13000	Petrol	B	523	f	t	2018-06-29	2021-09-16 09:15:48.430954+00	1598	t	t	f
3077	13001	Petrol	B	1548	t	t	2018-06-23	2021-09-16 09:15:48.438023+00	1599	t	t	t
3078	13002	Petrol	B	1524	f	f	2018-06-06	2021-09-16 09:15:48.443871+00	1600	f	f	t
3079	13003	Petrol	B	752	t	f	2018-06-18	2021-09-16 09:15:48.450316+00	1601	f	t	t
3080	13004	Petrol	B	1787	f	t	2018-06-07	2021-09-16 09:15:48.456859+00	1602	f	f	t
3081	13005	Petrol	B	1416	t	f	2018-06-02	2021-09-16 09:15:48.462643+00	1603	t	t	f
3082	13006	Petrol	B	1685	t	f	2018-06-16	2021-09-16 09:15:48.469541+00	1604	t	t	f
3083	13007	Petrol	B	920	f	f	2018-06-15	2021-09-16 09:15:48.475229+00	1605	f	f	f
3084	13008	Petrol	B	1976	t	t	2018-06-24	2021-09-16 09:15:48.484984+00	1606	f	t	t
3085	13009	Petrol	B	2353	t	t	2018-06-11	2021-09-16 09:15:48.494391+00	1607	t	f	t
3086	13010	Petrol	B	1045	t	f	2018-06-19	2021-09-16 09:15:48.50568+00	1608	t	f	t
3087	13011	Petrol	B	1998	t	f	2018-06-14	2021-09-16 09:15:48.511694+00	1609	f	f	t
3088	13012	Petrol	B	593	f	f	2018-06-03	2021-09-16 09:15:48.518621+00	1610	f	t	t
3089	13013	Petrol	B	1994	t	t	2018-06-06	2021-09-16 09:15:48.525849+00	1611	t	f	f
3090	13014	Petrol	B	1595	f	f	2018-06-13	2021-09-16 09:15:48.532836+00	1612	f	t	f
3091	13015	Petrol	B	2181	f	t	2018-06-22	2021-09-16 09:15:48.541851+00	1613	t	f	f
3092	13016	Petrol	B	2403	f	f	2018-06-16	2021-09-16 09:15:48.547877+00	1614	t	f	t
3093	13017	Petrol	B	1918	t	f	2018-06-08	2021-09-16 09:15:48.554449+00	1615	t	f	f
3094	13018	Petrol	B	1651	f	t	2018-06-11	2021-09-16 09:15:48.559535+00	1616	f	t	t
3095	13019	Petrol	B	541	t	t	2018-06-28	2021-09-16 09:15:48.564777+00	1617	f	f	t
3096	13020	Petrol	B	1164	t	t	2018-06-30	2021-09-16 09:15:48.570183+00	1618	f	f	f
3097	13021	Petrol	B	2289	f	t	2018-06-16	2021-09-16 09:15:48.576159+00	1619	f	f	f
3098	13022	Petrol	B	1519	f	t	2018-06-11	2021-09-16 09:15:48.582519+00	1620	f	t	t
3099	13023	Petrol	B	1840	t	f	2018-06-05	2021-09-16 09:15:48.587865+00	1621	t	t	t
3100	13024	Petrol	B	1798	f	f	2018-06-20	2021-09-16 09:15:48.593398+00	1622	f	t	f
3101	13025	Petrol	B	933	f	f	2018-06-15	2021-09-16 09:15:48.598376+00	1623	t	f	t
3102	13026	Petrol	B	2291	f	f	2018-06-27	2021-09-16 09:15:48.603604+00	1624	t	t	f
3103	13027	Petrol	B	648	t	f	2018-06-28	2021-09-16 09:15:48.609631+00	1625	t	f	f
3104	13028	Petrol	B	1933	t	f	2018-06-26	2021-09-16 09:15:48.615866+00	1626	f	f	t
3105	13029	Petrol	B	911	f	f	2018-06-29	2021-09-16 09:15:48.621417+00	1627	f	f	f
3106	13030	Petrol	B	2299	t	f	2018-06-10	2021-09-16 09:15:48.62732+00	1628	t	t	t
3107	13031	Petrol	B	620	f	t	2018-06-27	2021-09-16 09:15:48.632617+00	1629	f	f	t
3108	13032	Petrol	B	2418	f	t	2018-06-27	2021-09-16 09:15:48.638036+00	1630	t	f	f
3109	13033	Petrol	B	1785	t	f	2018-06-18	2021-09-16 09:15:48.64333+00	1631	f	f	f
3110	13034	Petrol	B	2139	t	t	2018-06-04	2021-09-16 09:15:48.649861+00	1632	f	f	t
3111	13035	Petrol	B	2023	f	t	2018-06-23	2021-09-16 09:15:48.655864+00	1633	t	t	t
3112	13036	Petrol	B	2099	f	f	2018-06-06	2021-09-16 09:15:48.661196+00	1634	f	t	f
3113	13037	Petrol	B	1559	t	f	2018-06-19	2021-09-16 09:15:48.667238+00	1635	t	f	f
3114	13038	Petrol	B	1277	f	f	2018-06-11	2021-09-16 09:15:48.672769+00	1636	t	t	f
3115	13039	Petrol	B	2487	t	t	2018-06-29	2021-09-16 09:15:48.677997+00	1637	f	t	f
3116	13040	Petrol	B	1717	f	f	2018-06-02	2021-09-16 09:15:48.684077+00	1638	t	t	f
3117	13041	Petrol	B	2342	t	t	2018-06-14	2021-09-16 09:15:48.689863+00	1639	f	f	f
3118	13042	Petrol	B	717	f	t	2018-06-01	2021-09-16 09:15:48.695775+00	1640	t	f	t
3119	13043	Petrol	B	1533	f	f	2018-06-16	2021-09-16 09:15:48.700872+00	1641	t	f	f
3120	13044	Petrol	B	751	t	f	2018-06-23	2021-09-16 09:15:48.705884+00	1642	f	f	t
3121	13045	Petrol	B	2277	t	f	2018-07-29	2021-09-16 09:15:48.71111+00	1643	f	t	f
3122	13046	Petrol	B	2205	f	f	2018-07-06	2021-09-16 09:15:48.716667+00	1644	f	t	f
3123	13047	Petrol	B	1095	t	f	2018-07-05	2021-09-16 09:15:48.721162+00	1645	f	t	f
3124	13048	Petrol	B	552	f	f	2018-07-23	2021-09-16 09:15:48.725645+00	1646	t	t	t
3125	13049	Petrol	B	2489	t	f	2018-07-24	2021-09-16 09:15:48.730633+00	1647	f	f	t
3126	13050	Petrol	B	2345	t	t	2018-07-21	2021-09-16 09:15:48.735318+00	1648	f	f	t
3127	13051	Petrol	B	1712	f	f	2018-07-05	2021-09-16 09:15:48.740286+00	1649	f	f	t
3128	13052	Petrol	B	2326	t	f	2018-07-23	2021-09-16 09:15:48.744591+00	1650	t	t	t
3129	13053	Petrol	B	505	f	f	2018-07-06	2021-09-16 09:15:48.749826+00	1651	t	t	f
3130	13054	Petrol	B	882	f	f	2018-07-23	2021-09-16 09:15:48.755526+00	1652	f	t	f
3131	13055	Petrol	B	837	t	f	2018-07-18	2021-09-16 09:15:48.761847+00	1653	t	f	f
3132	13056	Petrol	B	2418	f	t	2018-07-03	2021-09-16 09:15:48.767269+00	1654	f	t	t
3133	13057	Petrol	B	625	f	t	2018-07-24	2021-09-16 09:15:48.773528+00	1655	t	f	t
3134	13058	Petrol	B	1643	t	t	2018-07-20	2021-09-16 09:15:48.780102+00	1656	t	f	f
3135	13059	Petrol	B	1564	f	f	2018-07-28	2021-09-16 09:15:48.788473+00	1657	t	f	f
3136	13060	Petrol	B	1642	f	f	2018-07-13	2021-09-16 09:15:48.794335+00	1658	t	t	f
3137	13061	Petrol	B	1354	f	t	2018-07-24	2021-09-16 09:15:48.802058+00	1659	f	f	f
3138	13062	Petrol	B	1614	t	t	2018-07-13	2021-09-16 09:15:48.808069+00	1660	f	t	f
3139	13063	Petrol	B	1764	t	t	2018-07-03	2021-09-16 09:15:48.815635+00	1661	f	t	t
3140	13064	Petrol	B	1848	t	t	2018-07-18	2021-09-16 09:15:48.820887+00	1662	f	t	t
3141	13065	Petrol	B	953	t	t	2018-07-08	2021-09-16 09:15:48.826007+00	1663	t	t	f
3142	13066	Petrol	B	2009	f	f	2018-07-03	2021-09-16 09:15:48.832524+00	1664	f	t	f
3143	13067	Petrol	B	1726	f	t	2018-07-26	2021-09-16 09:15:48.839021+00	1665	t	t	f
3144	13068	Petrol	B	741	t	t	2018-07-25	2021-09-16 09:15:48.85324+00	1666	f	t	t
3145	13069	Petrol	B	2467	f	f	2018-07-17	2021-09-16 09:15:48.870509+00	1667	f	f	t
3146	13070	Petrol	B	775	f	t	2018-07-15	2021-09-16 09:15:48.882207+00	1668	t	t	t
3147	13071	Petrol	B	1545	t	f	2018-07-19	2021-09-16 09:15:48.890961+00	1669	t	f	f
3148	13072	Petrol	B	631	t	f	2018-07-14	2021-09-16 09:15:48.899273+00	1670	t	t	f
3149	13073	Petrol	B	2250	t	t	2018-07-24	2021-09-16 09:15:48.908315+00	1671	f	f	t
3150	13074	Petrol	B	1741	f	f	2018-07-07	2021-09-16 09:15:48.918724+00	1672	t	f	f
3151	13075	Petrol	B	1668	f	t	2018-07-28	2021-09-16 09:15:48.927097+00	1673	t	f	f
3152	13076	Petrol	B	1303	t	t	2018-07-20	2021-09-16 09:15:48.937477+00	1674	f	t	t
3153	13077	Petrol	B	691	t	f	2018-07-07	2021-09-16 09:15:48.945867+00	1675	t	f	t
3154	13078	Petrol	B	1157	t	f	2018-07-07	2021-09-16 09:15:48.954482+00	1676	t	f	t
3155	13079	Petrol	B	648	t	f	2018-07-14	2021-09-16 09:15:48.962918+00	1677	t	f	f
3156	13080	Petrol	B	563	f	f	2018-07-05	2021-09-16 09:15:48.970837+00	1678	t	t	t
3157	13081	Petrol	B	2040	t	t	2018-07-02	2021-09-16 09:15:48.978269+00	1679	f	t	t
3158	13082	Petrol	B	1211	f	f	2018-07-11	2021-09-16 09:15:48.985017+00	1680	f	f	t
3159	13083	Petrol	B	1666	f	t	2018-07-14	2021-09-16 09:15:48.992078+00	1681	t	f	t
3160	13084	Petrol	B	1140	f	t	2018-07-13	2021-09-16 09:15:48.999122+00	1682	f	f	f
3161	13085	Petrol	B	2108	f	f	2018-07-22	2021-09-16 09:15:49.00612+00	1683	f	t	f
3162	13086	Petrol	B	1734	t	f	2018-07-09	2021-09-16 09:15:49.01309+00	1684	f	t	f
3163	13087	Petrol	B	1281	f	f	2018-07-10	2021-09-16 09:15:49.020352+00	1685	f	f	f
3164	13088	Petrol	B	1926	f	f	2018-07-05	2021-09-16 09:15:49.027275+00	1686	f	t	t
3165	13089	Petrol	B	2095	t	f	2018-07-28	2021-09-16 09:15:49.034272+00	1687	t	f	f
3166	13090	Petrol	B	2199	f	f	2018-07-26	2021-09-16 09:15:49.041169+00	1688	t	t	t
3167	13091	Petrol	B	611	t	f	2018-07-16	2021-09-16 09:15:49.048411+00	1689	t	f	f
3168	13092	Petrol	B	2117	t	t	2018-07-02	2021-09-16 09:15:49.055704+00	1690	t	t	t
3169	13093	Petrol	B	1427	f	f	2018-07-23	2021-09-16 09:15:49.0641+00	1691	t	t	f
3170	13094	Petrol	B	2141	f	t	2018-07-13	2021-09-16 09:15:49.070645+00	1692	t	t	t
3171	13095	Petrol	B	1286	t	t	2018-07-19	2021-09-16 09:15:49.077388+00	1693	t	t	t
3172	13096	Petrol	B	521	t	t	2018-07-26	2021-09-16 09:15:49.085532+00	1694	f	f	t
3173	13097	Petrol	B	1002	f	t	2018-07-18	2021-09-16 09:15:49.093163+00	1695	f	t	t
3174	13098	Petrol	B	1425	t	f	2018-07-30	2021-09-16 09:15:49.098988+00	1696	f	f	f
3175	13099	Petrol	B	1796	t	t	2018-07-04	2021-09-16 09:15:49.104091+00	1697	t	t	f
3176	13100	Petrol	B	1627	t	t	2018-07-11	2021-09-16 09:15:49.108803+00	1698	t	f	t
3177	13101	Petrol	B	1019	t	f	2018-07-25	2021-09-16 09:15:49.113612+00	1699	f	f	t
3178	13102	Petrol	B	1492	f	f	2018-07-14	2021-09-16 09:15:49.11828+00	1700	t	f	f
3179	13103	Petrol	B	2220	f	t	2018-07-09	2021-09-16 09:15:49.125974+00	1701	t	t	t
3180	13104	Petrol	B	705	f	t	2018-07-15	2021-09-16 09:15:49.132855+00	1702	t	t	f
3181	13105	Petrol	B	1925	t	f	2018-07-06	2021-09-16 09:15:49.13837+00	1703	f	f	f
3182	13106	Petrol	B	1673	t	f	2018-07-11	2021-09-16 09:15:49.144455+00	1704	f	t	f
3183	13107	Petrol	B	2359	t	t	2018-07-20	2021-09-16 09:15:49.150275+00	1705	t	t	t
3184	13108	Petrol	B	1857	t	t	2018-07-02	2021-09-16 09:15:49.15554+00	1706	t	t	f
3185	13109	Petrol	B	2235	t	f	2018-07-28	2021-09-16 09:15:49.160856+00	1707	t	t	f
3186	13110	Petrol	B	1192	t	t	2018-07-15	2021-09-16 09:15:49.166081+00	1708	f	f	t
3187	13111	Petrol	B	1490	f	t	2018-07-13	2021-09-16 09:15:49.171758+00	1709	t	t	f
3188	13112	Petrol	B	2156	t	t	2018-07-15	2021-09-16 09:15:49.177004+00	1710	t	t	t
3189	13113	Petrol	B	2241	f	t	2018-07-05	2021-09-16 09:15:49.181586+00	1711	t	f	f
3190	13114	Petrol	B	2052	f	t	2018-07-08	2021-09-16 09:15:49.1862+00	1712	t	f	t
3191	13115	Petrol	B	1371	f	t	2018-07-10	2021-09-16 09:15:49.191389+00	1713	t	t	f
3192	13116	Petrol	B	1351	t	t	2018-07-22	2021-09-16 09:15:49.195925+00	1714	t	f	f
3193	13117	Petrol	B	1008	f	f	2018-07-11	2021-09-16 09:15:49.19995+00	1715	t	f	f
3194	13118	Petrol	B	1798	f	t	2018-07-04	2021-09-16 09:15:49.203814+00	1716	t	f	f
3195	13119	Petrol	B	1253	f	t	2018-07-01	2021-09-16 09:15:49.208086+00	1717	t	f	t
3196	13120	Petrol	B	1939	t	f	2018-07-08	2021-09-16 09:15:49.211845+00	1718	f	f	f
3197	13121	Petrol	B	2475	t	f	2018-07-25	2021-09-16 09:15:49.215685+00	1719	f	f	t
3198	13122	Petrol	B	1949	f	t	2018-07-13	2021-09-16 09:15:49.220174+00	1720	f	t	f
3199	13123	Petrol	B	1143	t	f	2018-07-25	2021-09-16 09:15:49.224872+00	1721	t	t	t
3200	13124	Petrol	B	2084	f	t	2018-07-01	2021-09-16 09:15:49.22945+00	1722	t	f	f
3201	13125	Petrol	B	2434	t	t	2018-07-22	2021-09-16 09:15:49.233381+00	1723	f	t	f
3202	13126	Petrol	B	2313	f	f	2018-07-25	2021-09-16 09:15:49.237306+00	1724	t	f	f
3203	13127	Petrol	B	500	f	t	2018-07-25	2021-09-16 09:15:49.241291+00	1725	f	f	f
3204	13128	Petrol	B	1977	t	f	2018-07-30	2021-09-16 09:15:49.245081+00	1726	f	t	t
3205	13129	Petrol	B	1195	t	t	2018-07-20	2021-09-16 09:15:49.248999+00	1727	f	f	t
3206	13130	Petrol	B	1163	f	f	2018-07-14	2021-09-16 09:15:49.252964+00	1728	f	t	f
3207	13131	Petrol	B	2010	t	f	2018-07-16	2021-09-16 09:15:49.25711+00	1729	t	t	f
3208	13132	Petrol	B	532	t	t	2018-07-17	2021-09-16 09:15:49.261522+00	1730	f	t	t
3209	13133	Petrol	B	2478	f	f	2018-07-23	2021-09-16 09:15:49.269479+00	1731	f	f	f
3210	13134	Petrol	B	1388	t	f	2018-07-15	2021-09-16 09:15:49.277337+00	1732	f	f	f
3211	13135	Petrol	B	1076	t	f	2018-07-24	2021-09-16 09:15:49.282971+00	1733	f	f	f
3212	13136	Petrol	B	564	f	f	2018-07-01	2021-09-16 09:15:49.287683+00	1734	t	t	t
3213	13137	Petrol	B	1225	f	f	2018-07-17	2021-09-16 09:15:49.292035+00	1735	f	f	t
3214	13138	Petrol	B	2295	f	t	2018-07-25	2021-09-16 09:15:49.296052+00	1736	t	f	t
3215	13139	Petrol	B	2285	t	f	2018-07-23	2021-09-16 09:15:49.299882+00	1737	t	t	t
3216	13140	Petrol	B	508	t	f	2018-07-28	2021-09-16 09:15:49.303883+00	1738	t	f	t
3217	13141	Petrol	B	1967	t	f	2018-07-06	2021-09-16 09:15:49.307636+00	1739	f	t	f
3218	13142	Petrol	B	1553	f	f	2018-07-10	2021-09-16 09:15:49.311588+00	1740	t	t	t
3219	13143	Petrol	B	2141	f	t	2018-07-03	2021-09-16 09:15:49.315546+00	1741	f	f	f
3220	13144	Petrol	B	2102	t	f	2018-07-29	2021-09-16 09:15:49.319507+00	1742	t	f	t
3221	13145	Petrol	B	1662	f	t	2018-08-05	2021-09-16 09:15:49.324883+00	1743	t	f	t
3222	13146	Petrol	B	910	f	t	2018-08-27	2021-09-16 09:15:49.330131+00	1744	f	f	f
3223	13147	Petrol	B	2402	f	t	2018-08-10	2021-09-16 09:15:49.335357+00	1745	t	f	f
3224	13148	Petrol	B	1731	t	f	2018-08-18	2021-09-16 09:15:49.345669+00	1746	f	f	f
3225	13149	Petrol	B	557	t	f	2018-08-24	2021-09-16 09:15:49.350694+00	1747	t	f	t
3226	13150	Petrol	B	1377	t	f	2018-08-10	2021-09-16 09:15:49.355697+00	1748	t	f	f
3227	13151	Petrol	B	2301	t	f	2018-08-30	2021-09-16 09:15:49.360297+00	1749	f	f	f
3228	13152	Petrol	B	1157	f	f	2018-08-03	2021-09-16 09:15:49.364443+00	1750	f	f	t
3229	13153	Petrol	B	629	t	f	2018-08-06	2021-09-16 09:15:49.368757+00	1751	t	f	t
3230	13154	Petrol	B	2255	f	t	2018-08-30	2021-09-16 09:15:49.373171+00	1752	t	f	f
3231	13155	Petrol	B	974	f	t	2018-08-02	2021-09-16 09:15:49.377424+00	1753	f	f	t
3232	13156	Petrol	B	1399	t	f	2018-08-17	2021-09-16 09:15:49.381652+00	1754	t	f	f
3233	13157	Petrol	B	1612	f	t	2018-08-02	2021-09-16 09:15:49.386437+00	1755	f	f	t
3234	13158	Petrol	B	2410	t	f	2018-08-03	2021-09-16 09:15:49.394872+00	1756	t	t	t
3235	13159	Petrol	B	1545	f	t	2018-08-26	2021-09-16 09:15:49.403497+00	1757	t	t	f
3236	13160	Petrol	B	1566	t	t	2018-08-14	2021-09-16 09:15:49.412766+00	1758	f	t	f
3237	13161	Petrol	B	703	t	f	2018-08-23	2021-09-16 09:15:49.421895+00	1759	t	t	f
3238	13162	Petrol	B	1441	t	t	2018-08-25	2021-09-16 09:15:49.430573+00	1760	t	t	t
3239	13163	Petrol	B	1005	t	t	2018-08-03	2021-09-16 09:15:49.440268+00	1761	f	t	t
3240	13164	Petrol	B	2163	t	f	2018-08-04	2021-09-16 09:15:49.449272+00	1762	t	f	f
3241	13165	Petrol	B	1835	t	f	2018-08-14	2021-09-16 09:15:49.45875+00	1763	t	t	t
3242	13166	Petrol	B	2033	f	t	2018-08-26	2021-09-16 09:15:49.466647+00	1764	t	t	t
3243	13167	Petrol	B	1950	t	t	2018-08-17	2021-09-16 09:15:49.472275+00	1765	f	f	f
3244	13168	Petrol	B	699	f	f	2018-08-20	2021-09-16 09:15:49.477321+00	1766	f	t	t
3245	13169	Petrol	B	817	f	f	2018-08-30	2021-09-16 09:15:49.481578+00	1767	f	f	t
3246	13170	Petrol	B	2435	t	f	2018-08-14	2021-09-16 09:15:49.485468+00	1768	f	t	t
3247	13171	Petrol	B	1216	f	f	2018-08-22	2021-09-16 09:15:49.489284+00	1769	t	f	f
3248	13172	Petrol	B	705	f	t	2018-08-10	2021-09-16 09:15:49.496269+00	1770	t	f	t
3249	13173	Petrol	B	2443	f	f	2018-08-17	2021-09-16 09:15:49.502005+00	1771	t	t	t
3250	13174	Petrol	B	1398	t	f	2018-08-26	2021-09-16 09:15:49.506701+00	1772	f	f	t
3251	13175	Petrol	B	2471	f	t	2018-08-24	2021-09-16 09:15:49.511587+00	1773	t	t	t
3252	13176	Petrol	B	1854	t	f	2018-08-22	2021-09-16 09:15:49.518524+00	1774	t	f	t
3253	13177	Petrol	B	675	f	f	2018-08-28	2021-09-16 09:15:49.523785+00	1775	f	t	t
3254	13178	Petrol	B	1035	t	f	2018-08-30	2021-09-16 09:15:49.528181+00	1776	f	f	f
3255	13179	Petrol	B	926	t	t	2018-08-05	2021-09-16 09:15:49.532241+00	1777	t	f	f
3256	13180	Petrol	B	2171	f	t	2018-08-12	2021-09-16 09:15:49.538809+00	1778	t	t	t
3257	13181	Petrol	B	2064	t	t	2018-08-30	2021-09-16 09:15:49.542637+00	1779	t	f	f
3258	13182	Petrol	B	934	t	t	2018-08-25	2021-09-16 09:15:49.546293+00	1780	t	t	f
3259	13183	Petrol	B	1681	f	t	2018-08-07	2021-09-16 09:15:49.551902+00	1781	t	t	t
3260	13184	Petrol	B	1556	t	f	2018-08-17	2021-09-16 09:15:49.561016+00	1782	f	t	t
3261	13185	Petrol	B	2111	t	f	2018-08-22	2021-09-16 09:15:49.569813+00	1783	t	t	f
3262	13186	Petrol	B	2048	f	f	2018-08-23	2021-09-16 09:15:49.578105+00	1784	t	t	t
3263	13187	Petrol	B	507	f	f	2018-08-07	2021-09-16 09:15:49.586213+00	1785	t	f	f
3264	13188	Petrol	B	1799	t	f	2018-08-13	2021-09-16 09:15:49.594776+00	1786	f	t	f
3265	13189	Petrol	B	2292	t	f	2018-08-11	2021-09-16 09:15:49.603392+00	1787	t	f	t
3266	13190	Petrol	B	2143	f	f	2018-08-23	2021-09-16 09:15:49.61213+00	1788	f	f	t
3267	13191	Petrol	B	2198	f	t	2018-08-17	2021-09-16 09:15:49.620603+00	1789	t	t	t
3268	13192	Petrol	B	581	t	f	2018-08-08	2021-09-16 09:15:49.627536+00	1790	t	f	f
3269	13193	Petrol	B	2364	t	t	2018-08-14	2021-09-16 09:15:49.633451+00	1791	f	f	f
3270	13194	Petrol	B	533	t	f	2018-08-07	2021-09-16 09:15:49.638431+00	1792	f	f	t
3271	13195	Petrol	B	1911	t	t	2018-08-23	2021-09-16 09:15:49.642808+00	1793	f	t	t
3272	13196	Petrol	B	1559	f	f	2018-08-21	2021-09-16 09:15:49.647147+00	1794	f	t	t
3273	13197	Petrol	B	2477	f	f	2018-08-16	2021-09-16 09:15:49.651466+00	1795	t	f	f
3274	13198	Petrol	B	878	f	f	2018-08-22	2021-09-16 09:15:49.655949+00	1796	f	f	f
3275	13199	Petrol	B	1090	f	f	2018-08-04	2021-09-16 09:15:49.660401+00	1797	f	f	t
3276	13200	Petrol	B	909	f	f	2018-08-24	2021-09-16 09:15:49.664801+00	1798	t	t	f
3277	13201	Petrol	B	833	f	t	2018-08-11	2021-09-16 09:15:49.669214+00	1799	f	f	f
3278	13202	Petrol	B	1031	t	t	2018-08-05	2021-09-16 09:15:49.673581+00	1800	f	f	t
3279	13203	Petrol	B	1947	f	t	2018-08-23	2021-09-16 09:15:49.678118+00	1801	t	f	f
3280	13204	Petrol	B	1757	t	t	2018-08-18	2021-09-16 09:15:49.682432+00	1802	t	t	f
3281	13205	Petrol	B	993	f	f	2018-08-02	2021-09-16 09:15:49.686725+00	1803	t	f	t
3282	13206	Petrol	B	1078	f	t	2018-08-05	2021-09-16 09:15:49.690977+00	1804	f	t	t
3283	13207	Petrol	B	1349	f	t	2018-08-20	2021-09-16 09:15:49.695302+00	1805	f	f	t
3284	13208	Petrol	B	1801	t	f	2018-08-05	2021-09-16 09:15:49.699645+00	1806	f	t	t
3285	13209	Petrol	B	511	t	f	2018-08-03	2021-09-16 09:15:49.704016+00	1807	f	t	f
3286	13210	Petrol	B	1709	f	f	2018-08-23	2021-09-16 09:15:49.708424+00	1808	t	t	f
3287	13211	Petrol	B	973	f	t	2018-08-08	2021-09-16 09:15:49.712729+00	1809	f	t	t
3288	13212	Petrol	B	747	f	f	2018-08-19	2021-09-16 09:15:49.716944+00	1810	f	t	t
3289	13213	Petrol	B	1102	t	f	2018-08-06	2021-09-16 09:15:49.721313+00	1811	t	f	f
3290	13214	Petrol	B	1878	f	t	2018-08-15	2021-09-16 09:15:49.725818+00	1812	t	t	f
3291	13215	Petrol	B	975	f	t	2018-08-17	2021-09-16 09:15:49.731336+00	1813	f	f	f
3292	13216	Petrol	B	803	t	t	2018-08-19	2021-09-16 09:15:49.736311+00	1814	f	t	f
3293	13217	Petrol	B	2339	t	t	2018-08-30	2021-09-16 09:15:49.74092+00	1815	t	t	f
3294	13218	Petrol	B	1340	t	f	2018-08-17	2021-09-16 09:15:49.745325+00	1816	f	f	t
3295	13219	Petrol	B	1539	t	t	2018-08-16	2021-09-16 09:15:49.749931+00	1817	t	t	f
3296	13220	Petrol	B	1364	f	f	2018-08-23	2021-09-16 09:15:49.754205+00	1818	t	t	f
3297	13221	Petrol	B	1083	t	f	2018-08-26	2021-09-16 09:15:49.758984+00	1819	f	t	f
3298	13222	Petrol	B	2380	t	f	2018-08-30	2021-09-16 09:15:49.763585+00	1820	t	f	t
3299	13223	Petrol	B	2238	f	t	2018-08-25	2021-09-16 09:15:49.767986+00	1821	f	t	f
3300	13224	Petrol	B	2192	f	t	2018-08-30	2021-09-16 09:15:49.772225+00	1822	t	t	t
3301	13225	Petrol	B	1302	t	t	2018-08-17	2021-09-16 09:15:49.776365+00	1823	f	f	f
3302	13226	Petrol	B	773	t	f	2018-08-06	2021-09-16 09:15:49.780443+00	1824	t	f	f
3303	13227	Petrol	B	2107	f	t	2018-08-28	2021-09-16 09:15:49.784504+00	1825	t	f	f
3304	13228	Petrol	B	1212	t	f	2018-08-25	2021-09-16 09:15:49.788695+00	1826	f	t	t
3305	13229	Petrol	B	1090	f	f	2018-08-28	2021-09-16 09:15:49.792661+00	1827	t	f	t
3306	13230	Petrol	B	1689	f	t	2018-08-11	2021-09-16 09:15:49.796335+00	1828	t	t	t
3307	13231	Petrol	B	1390	f	f	2018-08-11	2021-09-16 09:15:49.800776+00	1829	t	f	t
3308	13232	Petrol	B	2403	t	t	2018-08-17	2021-09-16 09:15:49.804768+00	1830	f	t	f
3309	13233	Petrol	B	1525	t	f	2018-08-30	2021-09-16 09:15:49.808522+00	1831	t	f	t
3310	13234	Petrol	B	2268	f	t	2018-08-07	2021-09-16 09:15:49.812988+00	1832	t	t	t
3311	13235	Petrol	B	1261	f	f	2018-08-26	2021-09-16 09:15:49.817293+00	1833	t	t	f
3312	13236	Petrol	B	1538	f	t	2018-08-02	2021-09-16 09:15:49.821552+00	1834	t	t	t
3313	13237	Petrol	B	1536	t	f	2018-08-17	2021-09-16 09:15:49.826187+00	1835	t	t	f
3314	13238	Petrol	B	1704	f	t	2018-08-14	2021-09-16 09:15:49.831955+00	1836	f	f	t
3315	13239	Petrol	B	2018	f	t	2018-08-18	2021-09-16 09:15:49.841331+00	1837	t	t	t
3316	13240	Petrol	B	1967	f	t	2018-08-21	2021-09-16 09:15:49.850731+00	1838	f	f	t
3317	13241	Petrol	B	1884	t	f	2018-08-08	2021-09-16 09:15:49.859383+00	1839	t	t	f
3318	13242	Petrol	B	2253	t	f	2018-08-03	2021-09-16 09:15:49.86792+00	1840	f	t	t
3319	13243	Petrol	B	1969	t	f	2018-08-26	2021-09-16 09:15:49.875979+00	1841	f	t	t
3320	13244	Petrol	B	667	f	f	2018-08-03	2021-09-16 09:15:49.882017+00	1842	f	f	f
3321	13245	Petrol	B	1584	f	f	2018-09-03	2021-09-16 09:15:49.887189+00	1843	f	t	f
3322	13246	Petrol	B	577	t	f	2018-09-14	2021-09-16 09:15:49.892286+00	1844	f	t	t
3323	13247	Petrol	B	2441	f	f	2018-09-29	2021-09-16 09:15:49.897401+00	1845	f	f	t
3324	13248	Petrol	B	616	t	f	2018-09-20	2021-09-16 09:15:49.905437+00	1846	t	t	t
3325	13249	Petrol	B	1801	t	t	2018-09-12	2021-09-16 09:15:49.909818+00	1847	t	f	f
3326	13250	Petrol	B	1613	f	f	2018-09-19	2021-09-16 09:15:49.917761+00	1848	f	t	t
3327	13251	Petrol	B	1893	f	t	2018-09-01	2021-09-16 09:15:49.926351+00	1849	f	t	f
3328	13252	Petrol	B	1280	f	t	2018-09-18	2021-09-16 09:15:49.935621+00	1850	t	t	t
3329	13253	Petrol	B	1698	t	t	2018-09-26	2021-09-16 09:15:49.944755+00	1851	f	f	f
3330	13254	Petrol	B	578	f	t	2018-09-24	2021-09-16 09:15:49.953146+00	1852	f	t	f
3331	13255	Petrol	B	2085	f	f	2018-09-28	2021-09-16 09:15:49.961908+00	1853	t	f	t
3332	13256	Petrol	B	1258	f	f	2018-09-11	2021-09-16 09:15:49.97078+00	1854	t	f	t
3333	13257	Petrol	B	1150	t	t	2018-09-01	2021-09-16 09:15:49.984074+00	1855	t	f	t
3334	13258	Petrol	B	762	t	t	2018-09-15	2021-09-16 09:15:49.992942+00	1856	t	f	f
3335	13259	Petrol	B	1418	f	f	2018-09-02	2021-09-16 09:15:50.002677+00	1857	f	f	f
3336	13260	Petrol	B	1784	t	t	2018-09-05	2021-09-16 09:15:50.011893+00	1858	t	t	t
3337	13261	Petrol	B	1278	f	t	2018-09-16	2021-09-16 09:15:50.020908+00	1859	f	t	t
3338	13262	Petrol	B	1312	f	t	2018-09-12	2021-09-16 09:15:50.030865+00	1860	f	t	t
3339	13263	Petrol	B	1755	f	t	2018-09-18	2021-09-16 09:15:50.039485+00	1861	t	f	t
3340	13264	Petrol	B	954	t	t	2018-09-12	2021-09-16 09:15:50.046757+00	1862	f	f	f
3341	13265	Petrol	B	1451	f	t	2018-09-01	2021-09-16 09:15:50.052265+00	1863	t	t	f
3342	13266	Petrol	B	2485	t	f	2018-09-06	2021-09-16 09:15:50.061893+00	1864	t	t	t
3343	13267	Petrol	B	2380	f	t	2018-09-08	2021-09-16 09:15:50.067545+00	1865	f	t	t
3344	13268	Petrol	B	1573	t	t	2018-09-13	2021-09-16 09:15:50.07273+00	1866	f	t	f
3345	13269	Petrol	B	2220	f	f	2018-09-04	2021-09-16 09:15:50.077997+00	1867	t	t	f
3346	13270	Petrol	B	2070	f	t	2018-09-09	2021-09-16 09:15:50.083049+00	1868	t	f	t
3347	13271	Petrol	B	1995	f	f	2018-09-16	2021-09-16 09:15:50.088215+00	1869	t	t	t
3348	13272	Petrol	B	578	f	f	2018-09-29	2021-09-16 09:15:50.09332+00	1870	f	t	f
3349	13273	Petrol	B	1142	f	t	2018-09-17	2021-09-16 09:15:50.098455+00	1871	f	t	f
3350	13274	Petrol	B	2023	t	t	2018-09-27	2021-09-16 09:15:50.103459+00	1872	t	t	f
3351	13275	Petrol	B	2391	f	t	2018-09-15	2021-09-16 09:15:50.108184+00	1873	f	f	t
3352	13276	Petrol	B	1337	t	t	2018-09-08	2021-09-16 09:15:50.112361+00	1874	f	f	t
3353	13277	Petrol	B	2384	f	f	2018-09-16	2021-09-16 09:15:50.116432+00	1875	t	t	f
3354	13278	Petrol	B	1782	f	t	2018-09-09	2021-09-16 09:15:50.12038+00	1876	f	f	f
3355	13279	Petrol	B	2458	f	f	2018-09-30	2021-09-16 09:15:50.124401+00	1877	t	t	f
3356	13280	Petrol	B	1680	t	f	2018-09-27	2021-09-16 09:15:50.1316+00	1878	t	f	t
3357	13281	Petrol	B	1528	f	f	2018-09-05	2021-09-16 09:15:50.13879+00	1879	f	f	f
3358	13282	Petrol	B	2386	t	t	2018-09-21	2021-09-16 09:15:50.147844+00	1880	t	t	t
3359	13283	Petrol	B	531	f	f	2018-09-23	2021-09-16 09:15:50.156974+00	1881	t	f	f
3360	13284	Petrol	B	2490	f	f	2018-09-19	2021-09-16 09:15:50.166538+00	1882	f	f	t
3361	13285	Petrol	B	1493	t	t	2018-09-22	2021-09-16 09:15:50.175374+00	1883	t	f	f
3362	13286	Petrol	B	2195	t	t	2018-09-18	2021-09-16 09:15:50.184419+00	1884	f	t	f
3363	13287	Petrol	B	1004	t	t	2018-09-25	2021-09-16 09:15:50.193602+00	1885	f	f	f
3364	13288	Petrol	B	698	t	f	2018-09-21	2021-09-16 09:15:50.20266+00	1886	t	f	f
3365	13289	Petrol	B	710	f	t	2018-09-06	2021-09-16 09:15:50.211781+00	1887	f	t	f
3366	13290	Petrol	B	858	f	t	2018-09-20	2021-09-16 09:15:50.220675+00	1888	t	f	f
3367	13291	Petrol	B	892	t	f	2018-09-06	2021-09-16 09:15:50.230093+00	1889	t	t	f
3368	13292	Petrol	B	2157	t	t	2018-09-24	2021-09-16 09:15:50.240227+00	1890	t	t	t
3369	13293	Petrol	B	1227	t	t	2018-09-17	2021-09-16 09:15:50.247162+00	1891	t	f	f
3370	13294	Petrol	B	733	f	f	2018-09-04	2021-09-16 09:15:50.252389+00	1892	t	f	f
3371	13295	Petrol	B	1471	t	f	2018-09-29	2021-09-16 09:15:50.256758+00	1893	f	t	t
3372	13296	Petrol	B	1546	f	t	2018-09-21	2021-09-16 09:15:50.260904+00	1894	t	t	f
3373	13297	Petrol	B	1391	f	f	2018-09-10	2021-09-16 09:15:50.265112+00	1895	f	f	f
3374	13298	Petrol	B	1971	t	f	2018-09-11	2021-09-16 09:15:50.269132+00	1896	f	t	t
3375	13299	Petrol	B	1609	f	t	2018-09-27	2021-09-16 09:15:50.273201+00	1897	t	t	t
3376	13300	Petrol	B	1868	t	f	2018-09-26	2021-09-16 09:15:50.277211+00	1898	t	t	t
3377	13301	Petrol	B	2057	f	f	2018-09-14	2021-09-16 09:15:50.281404+00	1899	t	t	f
3378	13302	Petrol	B	774	t	f	2018-09-12	2021-09-16 09:15:50.285422+00	1900	t	f	f
3379	13303	Petrol	B	1696	t	f	2018-09-11	2021-09-16 09:15:50.289843+00	1901	f	f	f
3380	13304	Petrol	B	1794	f	f	2018-09-05	2021-09-16 09:15:50.294328+00	1902	f	t	f
3381	13305	Petrol	B	1393	t	t	2018-09-16	2021-09-16 09:15:50.302186+00	1903	t	f	f
3382	13306	Petrol	B	2042	f	f	2018-09-11	2021-09-16 09:15:50.310921+00	1904	t	f	t
3383	13307	Petrol	B	1497	f	f	2018-09-29	2021-09-16 09:15:50.319879+00	1905	f	t	f
3384	13308	Petrol	B	2152	f	f	2018-09-27	2021-09-16 09:15:50.328688+00	1906	f	t	f
3385	13309	Petrol	B	643	t	f	2018-09-01	2021-09-16 09:15:50.336949+00	1907	t	t	f
3386	13310	Petrol	B	965	t	t	2018-09-28	2021-09-16 09:15:50.345344+00	1908	t	t	f
3387	13311	Petrol	B	1177	t	t	2018-09-07	2021-09-16 09:15:50.351256+00	1909	f	t	t
3388	13312	Petrol	B	856	f	t	2018-09-13	2021-09-16 09:15:50.356437+00	1910	f	f	f
3389	13313	Petrol	B	2473	f	t	2018-09-12	2021-09-16 09:15:50.363295+00	1911	f	t	t
3390	13314	Petrol	B	1112	f	f	2018-09-30	2021-09-16 09:15:50.368637+00	1912	t	t	f
3391	13315	Petrol	B	1104	f	t	2018-09-18	2021-09-16 09:15:50.373162+00	1913	t	t	f
3392	13316	Petrol	B	765	f	f	2018-09-04	2021-09-16 09:15:50.377255+00	1914	t	t	t
3393	13317	Petrol	B	2173	t	t	2018-09-25	2021-09-16 09:15:50.380975+00	1915	t	f	t
3394	13318	Petrol	B	1897	t	f	2018-09-14	2021-09-16 09:15:50.385383+00	1916	t	f	t
3395	13319	Petrol	B	1402	f	t	2018-09-20	2021-09-16 09:15:50.389783+00	1917	f	t	t
3396	13320	Petrol	B	1088	f	t	2018-09-06	2021-09-16 09:15:50.394219+00	1918	t	t	f
3397	13321	Petrol	B	1890	f	t	2018-09-07	2021-09-16 09:15:50.402155+00	1919	f	t	f
3398	13322	Petrol	B	526	f	f	2018-09-06	2021-09-16 09:15:50.411373+00	1920	t	f	t
3399	13323	Petrol	B	553	t	t	2018-09-06	2021-09-16 09:15:50.421337+00	1921	f	t	t
3400	13324	Petrol	B	1612	f	f	2018-09-14	2021-09-16 09:15:50.432835+00	1922	t	t	f
3401	13325	Petrol	B	1892	t	t	2018-09-01	2021-09-16 09:15:50.438959+00	1923	f	f	f
3402	13326	Petrol	B	1680	f	f	2018-09-07	2021-09-16 09:15:50.44391+00	1924	f	f	t
3403	13327	Petrol	B	999	f	t	2018-09-06	2021-09-16 09:15:50.448576+00	1925	t	t	f
3404	13328	Petrol	B	690	f	f	2018-09-09	2021-09-16 09:15:50.452812+00	1926	f	f	f
3405	13329	Petrol	B	1015	t	f	2018-09-05	2021-09-16 09:15:50.457427+00	1927	t	f	f
3406	13330	Petrol	B	2080	t	f	2018-09-04	2021-09-16 09:15:50.461498+00	1928	t	t	f
3407	13331	Petrol	B	1009	t	t	2018-09-16	2021-09-16 09:15:50.46651+00	1929	t	f	f
3408	13332	Petrol	B	1616	t	f	2018-09-15	2021-09-16 09:15:50.475041+00	1930	t	t	f
3409	13333	Petrol	B	630	t	f	2018-09-19	2021-09-16 09:15:50.48382+00	1931	f	f	t
3410	13334	Petrol	B	840	t	f	2018-09-16	2021-09-16 09:15:50.492658+00	1932	f	t	t
3411	13335	Petrol	B	2201	t	f	2018-09-14	2021-09-16 09:15:50.501253+00	1933	t	f	t
3412	13336	Petrol	B	2354	f	t	2018-09-10	2021-09-16 09:15:50.510313+00	1934	t	t	t
3413	13337	Petrol	B	1008	f	t	2018-09-02	2021-09-16 09:15:50.518459+00	1935	t	f	f
3414	13338	Petrol	B	1715	t	t	2018-09-12	2021-09-16 09:15:50.524077+00	1936	t	t	f
3415	13339	Petrol	B	1496	t	f	2018-09-27	2021-09-16 09:15:50.529034+00	1937	t	t	f
3416	13340	Petrol	B	2314	f	t	2018-09-03	2021-09-16 09:15:50.536698+00	1938	f	f	f
3417	13341	Petrol	B	758	t	f	2018-09-29	2021-09-16 09:15:50.545559+00	1939	f	f	t
3418	13342	Petrol	B	1427	t	f	2018-09-01	2021-09-16 09:15:50.55145+00	1940	f	f	t
3419	13343	Petrol	B	1101	f	f	2018-09-22	2021-09-16 09:15:50.556555+00	1941	t	t	t
3420	13344	Petrol	B	843	t	f	2018-09-06	2021-09-16 09:15:50.565642+00	1942	t	t	f
3421	13345	Petrol	B	1977	f	f	2018-09-24	2021-09-16 09:15:50.575379+00	1943	t	t	t
3422	13346	Petrol	B	1569	t	f	2018-09-21	2021-09-16 09:15:50.582846+00	1944	t	f	t
3423	13347	Petrol	B	1533	t	t	2018-09-15	2021-09-16 09:15:50.588966+00	1945	f	f	f
3424	13348	Petrol	B	1147	t	t	2018-09-23	2021-09-16 09:15:50.596844+00	1946	t	t	t
3425	13349	Petrol	B	1413	t	f	2018-09-04	2021-09-16 09:15:50.603749+00	1947	t	f	t
3426	13350	Petrol	B	1362	f	f	2018-09-05	2021-09-16 09:15:50.610757+00	1948	f	f	t
3427	13351	Petrol	B	1900	f	f	2018-09-24	2021-09-16 09:15:50.617949+00	1949	f	f	f
3428	13352	Petrol	B	1791	f	t	2018-09-04	2021-09-16 09:15:50.624645+00	1950	f	f	f
3429	13353	Petrol	B	1593	t	f	2018-09-14	2021-09-16 09:15:50.63167+00	1951	t	t	f
3430	13354	Petrol	B	2115	f	t	2018-09-02	2021-09-16 09:15:50.638397+00	1952	t	t	t
3431	13355	Petrol	B	693	t	t	2018-09-06	2021-09-16 09:15:50.645164+00	1953	t	f	t
3432	13356	Petrol	B	1939	f	f	2018-09-01	2021-09-16 09:15:50.6518+00	1954	f	t	t
3433	13357	Petrol	B	1191	f	f	2018-09-23	2021-09-16 09:15:50.658713+00	1955	t	f	t
3434	13358	Petrol	B	2272	t	t	2018-09-06	2021-09-16 09:15:50.665148+00	1956	f	t	f
3435	13359	Petrol	B	2134	f	f	2018-09-18	2021-09-16 09:15:50.676091+00	1957	f	f	t
3436	13360	Petrol	B	1357	t	t	2018-09-20	2021-09-16 09:15:50.689699+00	1958	f	t	f
3437	13361	Petrol	C	1067	t	f	2018-09-13	2021-09-16 09:15:50.702578+00	1959	t	f	t
3438	13362	Petrol	C	1636	t	f	2018-09-25	2021-09-16 09:15:50.715349+00	1960	f	f	f
3439	13363	Petrol	C	1986	t	t	2018-09-27	2021-09-16 09:15:50.728622+00	1961	f	f	t
3440	13364	Petrol	C	969	f	f	2018-09-16	2021-09-16 09:15:50.735704+00	1962	f	f	f
3441	13365	Petrol	C	789	f	t	2018-09-30	2021-09-16 09:15:50.742478+00	1963	f	f	t
3442	13366	Petrol	C	1116	t	f	2018-09-26	2021-09-16 09:15:50.749445+00	1964	t	f	t
3443	13367	Petrol	C	1195	f	t	2018-09-25	2021-09-16 09:15:50.756693+00	1965	t	f	t
3444	13368	Petrol	C	1180	f	t	2018-09-28	2021-09-16 09:15:50.76322+00	1966	f	f	t
3445	13369	Petrol	C	696	t	f	2018-09-22	2021-09-16 09:15:50.769605+00	1967	f	t	f
3446	13370	Petrol	C	1105	t	t	2018-09-10	2021-09-16 09:15:50.776232+00	1968	f	t	t
3447	13371	Petrol	C	1007	f	t	2018-10-16	2021-09-16 09:15:50.782888+00	1969	f	t	f
3448	13372	Petrol	C	585	t	f	2018-10-29	2021-09-16 09:15:50.78809+00	1970	f	t	f
3449	13373	Petrol	C	1384	t	f	2018-10-30	2021-09-16 09:15:50.792334+00	1971	t	f	f
3450	13374	Petrol	C	519	t	f	2018-10-30	2021-09-16 09:15:50.796456+00	1972	t	t	f
3451	13375	Petrol	C	1013	t	f	2018-10-11	2021-09-16 09:15:50.800387+00	1973	f	t	f
3452	13376	Petrol	C	695	f	t	2018-10-05	2021-09-16 09:15:50.812362+00	1974	t	f	t
3453	13377	Petrol	C	1184	f	f	2018-10-28	2021-09-16 09:15:50.823498+00	1975	t	t	f
3454	13378	Petrol	C	673	t	f	2018-10-30	2021-09-16 09:15:50.829996+00	1976	t	f	f
3455	13379	Petrol	C	2097	f	f	2018-10-16	2021-09-16 09:15:50.835534+00	1977	f	t	f
3456	13380	Petrol	C	2105	f	f	2018-10-20	2021-09-16 09:15:50.840945+00	1978	f	t	f
3457	13381	Petrol	C	555	t	f	2018-10-01	2021-09-16 09:15:50.846914+00	1979	t	f	t
3458	13382	Petrol	C	558	t	t	2018-10-16	2021-09-16 09:15:50.856314+00	1980	f	t	t
3459	13383	Petrol	C	640	t	t	2018-10-23	2021-09-16 09:15:50.865495+00	1981	f	t	f
3460	13384	Petrol	C	2012	f	f	2018-10-20	2021-09-16 09:15:50.876305+00	1982	t	f	f
3461	13385	Petrol	C	914	f	t	2018-10-16	2021-09-16 09:15:50.884815+00	1983	f	t	t
3462	13386	Petrol	C	1479	t	t	2018-10-08	2021-09-16 09:15:50.890768+00	1984	f	f	t
3463	13387	Petrol	C	2122	f	f	2018-10-30	2021-09-16 09:15:50.902545+00	1985	t	f	f
3464	13388	Petrol	C	948	t	t	2018-10-16	2021-09-16 09:15:50.907669+00	1986	t	t	f
3465	13389	Petrol	C	2371	t	f	2018-10-26	2021-09-16 09:15:50.91262+00	1987	t	f	t
3466	13390	Petrol	C	542	t	f	2018-10-06	2021-09-16 09:15:50.91904+00	1988	t	f	t
3467	13391	Petrol	C	2145	t	f	2018-10-03	2021-09-16 09:15:50.924494+00	1989	f	f	t
3468	13392	Petrol	C	1675	t	t	2018-10-06	2021-09-16 09:15:50.92922+00	1990	f	t	f
3469	13393	Petrol	C	825	t	t	2018-10-23	2021-09-16 09:15:50.934+00	1991	f	f	f
3470	13394	Petrol	C	2018	t	t	2018-10-24	2021-09-16 09:15:50.93892+00	1992	f	f	f
3471	13395	Petrol	C	1352	f	t	2018-10-12	2021-09-16 09:15:50.949185+00	1993	t	t	f
3472	13396	Diesel	C	1433	t	t	2018-10-21	2021-09-16 09:15:50.956661+00	1994	f	f	t
3473	13397	Diesel	C	1866	t	t	2018-10-03	2021-09-16 09:15:50.962768+00	1995	t	f	t
3474	13398	Diesel	C	1772	f	f	2018-10-24	2021-09-16 09:15:50.96878+00	1996	f	f	t
3475	13399	Diesel	C	1428	f	t	2018-10-29	2021-09-16 09:15:50.974722+00	1997	t	t	t
3476	13400	Diesel	C	1773	f	f	2018-10-16	2021-09-16 09:15:50.980865+00	1998	f	t	t
3477	13401	Diesel	C	1951	f	f	2018-10-30	2021-09-16 09:15:50.987059+00	1999	f	f	f
3478	13402	Diesel	C	670	f	f	2018-10-10	2021-09-16 09:15:50.994619+00	2000	f	f	t
3479	13403	Diesel	C	965	t	t	2018-10-29	2021-09-16 09:15:51.001912+00	2001	f	f	f
3480	13404	Diesel	C	1780	f	t	2018-10-29	2021-09-16 09:15:51.006985+00	2002	t	f	f
3481	13405	Diesel	C	782	f	f	2018-10-12	2021-09-16 09:15:51.012368+00	2003	f	t	f
3482	13406	Diesel	C	2200	t	t	2018-10-10	2021-09-16 09:15:51.017226+00	2004	f	t	f
3483	13407	Diesel	C	2424	t	t	2018-10-24	2021-09-16 09:15:51.022029+00	2005	f	f	t
3484	13408	Diesel	C	2104	t	f	2018-10-02	2021-09-16 09:15:51.027112+00	2006	t	t	f
3485	13409	Diesel	C	852	t	t	2018-10-07	2021-09-16 09:15:51.031203+00	2007	t	f	t
3486	13410	Diesel	C	2126	f	t	2018-10-26	2021-09-16 09:15:51.03575+00	2008	f	t	t
3487	13411	Diesel	C	2396	t	t	2018-10-06	2021-09-16 09:15:51.040309+00	2009	t	f	f
3488	13412	Diesel	C	1658	t	f	2018-10-16	2021-09-16 09:15:51.048646+00	2010	t	t	t
3489	13413	Diesel	C	2264	t	f	2018-10-04	2021-09-16 09:15:51.058068+00	2011	t	t	t
3490	13414	Diesel	C	2393	t	t	2018-10-19	2021-09-16 09:15:51.067468+00	2012	f	f	t
3491	13415	Diesel	C	586	t	t	2018-10-19	2021-09-16 09:15:51.07602+00	2013	t	t	t
3492	13416	Diesel	C	874	t	f	2018-10-20	2021-09-16 09:15:51.082578+00	2014	f	f	t
3493	13417	Diesel	C	944	t	t	2018-10-05	2021-09-16 09:15:51.08788+00	2015	t	f	t
3494	13418	Diesel	C	1279	t	t	2018-10-01	2021-09-16 09:15:51.09294+00	2016	f	t	f
3495	13419	Diesel	C	2074	f	f	2018-10-01	2021-09-16 09:15:51.097605+00	2017	t	f	t
3496	13420	Diesel	C	1706	f	t	2018-10-27	2021-09-16 09:15:51.102516+00	2018	t	f	t
3497	13421	Diesel	C	1179	f	f	2018-10-22	2021-09-16 09:15:51.107994+00	2019	f	f	t
3498	13422	Diesel	C	1571	t	t	2018-10-04	2021-09-16 09:15:51.113236+00	2020	t	t	f
3499	13423	Diesel	C	1465	t	t	2018-10-10	2021-09-16 09:15:51.11809+00	2021	t	t	t
3500	13424	Diesel	C	1557	f	t	2018-10-06	2021-09-16 09:15:51.122974+00	2022	t	t	t
3501	13425	Diesel	C	832	t	f	2018-10-02	2021-09-16 09:15:51.127867+00	2023	t	f	t
3502	13426	Diesel	C	1146	f	f	2018-10-04	2021-09-16 09:15:51.132603+00	2024	f	t	f
3503	13427	Diesel	C	1577	t	f	2018-10-16	2021-09-16 09:15:51.137874+00	2025	t	f	f
3504	13428	Diesel	C	2356	t	t	2018-10-30	2021-09-16 09:15:51.14242+00	2026	t	f	t
3505	13429	Diesel	C	1437	f	f	2018-10-29	2021-09-16 09:15:51.14736+00	2027	t	f	f
3506	13430	Diesel	C	2436	t	t	2018-10-24	2021-09-16 09:15:51.152395+00	2028	t	f	t
3507	13431	Diesel	C	1427	f	t	2018-10-24	2021-09-16 09:15:51.157453+00	2029	t	f	f
3508	13432	Diesel	C	1949	f	f	2018-10-11	2021-09-16 09:15:51.162627+00	2030	t	t	t
3509	13433	Diesel	C	1875	t	f	2018-10-01	2021-09-16 09:15:51.167683+00	2031	f	f	f
3510	13434	Diesel	C	2167	t	t	2018-10-06	2021-09-16 09:15:51.172599+00	2032	t	f	t
3511	13435	Diesel	C	1170	t	f	2018-10-22	2021-09-16 09:15:51.177685+00	2033	f	f	t
3512	13436	Diesel	C	684	t	t	2018-10-12	2021-09-16 09:15:51.182952+00	2034	t	f	f
3513	13437	Diesel	C	2148	f	t	2018-10-26	2021-09-16 09:15:51.188305+00	2035	t	f	f
3514	13438	Diesel	C	635	t	t	2018-10-22	2021-09-16 09:15:51.193628+00	2036	t	t	f
3515	13439	Diesel	C	925	t	f	2018-10-18	2021-09-16 09:15:51.198865+00	2037	t	f	t
3516	13440	Diesel	C	709	t	t	2018-10-27	2021-09-16 09:15:51.203971+00	2038	t	t	f
3517	13441	Diesel	C	2075	f	f	2018-10-26	2021-09-16 09:15:51.209544+00	2039	t	f	t
3518	13442	Diesel	C	602	t	t	2018-10-30	2021-09-16 09:15:51.214876+00	2040	t	f	t
3519	13443	Diesel	C	2130	t	f	2018-10-21	2021-09-16 09:15:51.219837+00	2041	f	t	f
3520	13444	Diesel	C	1959	t	t	2018-10-17	2021-09-16 09:15:51.224751+00	2042	f	f	t
3521	13445	Diesel	C	780	f	t	2018-10-23	2021-09-16 09:15:51.230685+00	2043	f	f	t
3522	13446	Diesel	C	2126	f	t	2018-10-30	2021-09-16 09:15:51.235562+00	2044	t	f	t
3523	13447	Diesel	C	1768	t	f	2018-10-04	2021-09-16 09:15:51.240497+00	2045	f	t	f
3524	13448	Diesel	C	1637	f	f	2018-10-10	2021-09-16 09:15:51.24674+00	2046	t	t	f
3525	13449	Diesel	C	1889	f	t	2018-10-28	2021-09-16 09:15:51.252501+00	2047	t	t	f
3526	13450	Diesel	C	2146	t	f	2018-10-11	2021-09-16 09:15:51.257709+00	2048	t	t	t
3527	13451	Diesel	C	1355	t	f	2018-10-03	2021-09-16 09:15:51.263063+00	2049	t	f	f
3528	13452	Diesel	C	1082	f	t	2018-10-15	2021-09-16 09:15:51.268202+00	2050	t	t	f
3529	13453	Diesel	C	2394	f	t	2018-10-29	2021-09-16 09:15:51.272913+00	2051	t	f	t
3530	13454	Diesel	C	926	t	t	2018-10-17	2021-09-16 09:15:51.277322+00	2052	t	t	t
3531	13455	Diesel	C	892	f	t	2018-10-24	2021-09-16 09:15:51.282851+00	2053	f	t	t
3532	13456	Diesel	C	994	f	f	2018-10-02	2021-09-16 09:15:51.288029+00	2054	t	f	t
3533	13457	Diesel	C	2260	f	t	2018-10-02	2021-09-16 09:15:51.29303+00	2055	f	f	t
3534	13458	Diesel	C	1149	t	t	2018-10-08	2021-09-16 09:15:51.29813+00	2056	t	t	f
3535	13459	Diesel	C	2283	t	f	2018-10-03	2021-09-16 09:15:51.303037+00	2057	f	f	t
3536	13460	Diesel	C	1663	f	t	2018-10-16	2021-09-16 09:15:51.308109+00	2058	f	t	t
3537	13461	Diesel	C	2197	t	t	2018-10-24	2021-09-16 09:15:51.313351+00	2059	f	t	t
3538	13462	Diesel	C	1669	f	f	2018-10-05	2021-09-16 09:15:51.318172+00	2060	f	t	t
3539	13463	Diesel	C	1547	f	f	2018-10-25	2021-09-16 09:15:51.322742+00	2061	t	f	f
3540	13464	Diesel	C	899	f	f	2018-10-12	2021-09-16 09:15:51.327668+00	2062	f	f	t
3541	13465	Diesel	C	2439	f	t	2018-10-18	2021-09-16 09:15:51.3328+00	2063	f	t	t
3542	13466	Diesel	C	1867	f	t	2018-11-07	2021-09-16 09:15:51.337667+00	2064	f	t	f
3543	13467	Diesel	C	898	t	t	2018-11-03	2021-09-16 09:15:51.342751+00	2065	f	f	t
3544	13468	Diesel	C	962	t	t	2018-11-10	2021-09-16 09:15:51.348025+00	2066	t	f	f
3545	13469	Diesel	C	1875	t	t	2018-11-18	2021-09-16 09:15:51.353886+00	2067	t	f	f
3546	13470	Diesel	C	1157	f	f	2018-11-28	2021-09-16 09:15:51.35857+00	2068	t	f	t
3547	13471	Diesel	C	2376	f	t	2018-11-01	2021-09-16 09:15:51.363033+00	2069	f	t	f
3548	13472	Diesel	C	1523	t	f	2018-11-22	2021-09-16 09:15:51.36885+00	2070	t	f	t
3549	13473	Diesel	C	1850	t	f	2018-11-17	2021-09-16 09:15:51.37567+00	2071	t	f	f
3550	13474	Diesel	C	1828	t	t	2018-11-24	2021-09-16 09:15:51.384938+00	2072	t	t	f
3551	13475	Diesel	C	1140	f	f	2018-11-02	2021-09-16 09:15:51.39049+00	2073	t	f	f
3552	13476	Diesel	C	1208	t	f	2018-11-14	2021-09-16 09:15:51.396264+00	2074	t	f	t
3553	13477	Diesel	C	1716	f	f	2018-11-30	2021-09-16 09:15:51.4033+00	2075	t	f	t
3554	13478	Diesel	C	731	f	t	2018-11-21	2021-09-16 09:15:51.410643+00	2076	f	t	f
3555	13479	Diesel	C	1128	t	t	2018-11-16	2021-09-16 09:15:51.417617+00	2077	f	t	f
3556	13480	Diesel	C	1541	t	f	2018-11-27	2021-09-16 09:15:51.423378+00	2078	f	t	f
3557	13481	Diesel	C	2498	f	t	2018-11-02	2021-09-16 09:15:51.429193+00	2079	t	f	f
3558	13482	Diesel	C	2125	f	f	2018-11-13	2021-09-16 09:15:51.436183+00	2080	t	t	t
3559	13483	Diesel	C	2130	f	f	2018-11-27	2021-09-16 09:15:51.442013+00	2081	t	f	t
3560	13484	Diesel	C	863	t	t	2018-11-19	2021-09-16 09:15:51.448879+00	2082	t	t	f
3561	13485	Diesel	C	1635	t	t	2018-11-13	2021-09-16 09:15:51.456267+00	2083	f	f	t
3562	13486	Diesel	C	1403	t	f	2018-11-20	2021-09-16 09:15:51.462614+00	2084	f	f	f
3563	13487	Diesel	C	2470	f	t	2018-11-23	2021-09-16 09:15:51.470123+00	2085	t	t	t
3564	13488	Diesel	C	822	t	t	2018-11-15	2021-09-16 09:15:51.477361+00	2086	f	t	t
3565	13489	Diesel	C	2054	f	t	2018-11-03	2021-09-16 09:15:51.484801+00	2087	f	t	t
3566	13490	Diesel	C	2322	f	t	2018-11-24	2021-09-16 09:15:51.492678+00	2088	f	f	f
3567	13491	Diesel	C	950	t	t	2018-11-29	2021-09-16 09:15:51.501255+00	2089	t	t	t
3568	13492	Diesel	C	2026	f	t	2018-11-06	2021-09-16 09:15:51.508482+00	2090	f	t	f
3569	13493	Diesel	C	2000	f	t	2018-11-26	2021-09-16 09:15:51.515892+00	2091	f	t	t
3570	13494	Diesel	C	935	f	f	2018-11-23	2021-09-16 09:15:51.522864+00	2092	t	f	t
3571	13495	Diesel	C	1506	f	t	2018-11-21	2021-09-16 09:15:51.528907+00	2093	t	t	t
3572	13496	Diesel	C	520	f	t	2018-11-04	2021-09-16 09:15:51.535246+00	2094	f	t	f
3573	13497	Diesel	C	759	t	f	2018-11-12	2021-09-16 09:15:51.542213+00	2095	f	t	f
3574	13498	Diesel	C	2338	f	t	2018-11-18	2021-09-16 09:15:51.548934+00	2096	t	f	t
3575	13499	Diesel	C	1957	t	t	2018-11-13	2021-09-16 09:15:51.554905+00	2097	f	f	t
3576	13500	Diesel	C	2201	t	t	2018-11-04	2021-09-16 09:15:51.561251+00	2098	t	t	f
3577	13501	Diesel	C	1021	t	f	2018-11-16	2021-09-16 09:15:51.567756+00	2099	t	t	t
3578	13502	Diesel	C	1738	f	f	2018-11-06	2021-09-16 09:15:51.575007+00	2100	t	f	f
3579	13503	Diesel	C	629	t	t	2018-11-23	2021-09-16 09:15:51.582577+00	2101	f	t	f
3580	13504	Diesel	C	714	t	t	2018-11-03	2021-09-16 09:15:51.592516+00	2102	f	f	t
3581	13505	Diesel	C	1461	f	f	2018-11-22	2021-09-16 09:15:51.598933+00	2103	t	f	f
3582	13506	Diesel	C	2477	t	t	2018-11-26	2021-09-16 09:15:51.606742+00	2104	f	t	t
3583	13507	Diesel	C	1275	t	f	2018-11-06	2021-09-16 09:15:51.614252+00	2105	f	f	t
3584	13508	Diesel	C	1937	t	t	2018-11-15	2021-09-16 09:15:51.624424+00	2106	f	f	t
3585	13509	Diesel	C	1961	f	f	2018-11-22	2021-09-16 09:15:51.63539+00	2107	t	f	f
3586	13510	Diesel	C	2482	t	f	2018-11-01	2021-09-16 09:15:51.643087+00	2108	t	f	t
3587	13511	Diesel	C	2128	t	f	2018-11-03	2021-09-16 09:15:51.652129+00	2109	f	t	t
3588	13512	Diesel	C	1981	f	t	2018-11-01	2021-09-16 09:15:51.659951+00	2110	t	f	f
3589	13513	Diesel	C	1564	t	f	2018-11-10	2021-09-16 09:15:51.6686+00	2111	t	t	f
3590	13514	Diesel	C	2380	t	f	2018-11-04	2021-09-16 09:15:51.676465+00	2112	f	t	t
3591	13515	Diesel	C	867	t	t	2018-11-13	2021-09-16 09:15:51.683312+00	2113	t	f	t
3592	13516	Diesel	C	698	f	t	2018-11-27	2021-09-16 09:15:51.68979+00	2114	f	t	f
3593	13517	Diesel	C	1548	f	t	2018-11-08	2021-09-16 09:15:51.696253+00	2115	f	f	t
3594	13518	Diesel	C	2083	f	t	2018-11-10	2021-09-16 09:15:51.7025+00	2116	f	t	f
3595	13519	Diesel	C	757	t	t	2018-11-25	2021-09-16 09:15:51.710406+00	2117	f	f	f
3596	13520	Diesel	C	754	t	t	2018-11-08	2021-09-16 09:15:51.71973+00	2118	f	t	t
3597	13521	Diesel	C	2449	f	t	2018-11-03	2021-09-16 09:15:51.726478+00	2119	f	t	f
3598	13522	Diesel	C	600	f	t	2018-11-29	2021-09-16 09:15:51.73291+00	2120	t	f	f
3599	13523	Diesel	C	974	f	t	2018-11-29	2021-09-16 09:15:51.740473+00	2121	f	f	f
3600	13524	Diesel	C	1370	t	t	2018-11-29	2021-09-16 09:15:51.746502+00	2122	f	f	t
3601	13525	Diesel	C	1348	t	f	2018-11-06	2021-09-16 09:15:51.752924+00	2123	f	f	t
3602	13526	Diesel	C	834	t	t	2018-11-11	2021-09-16 09:15:51.758896+00	2124	f	f	t
3603	13527	Diesel	C	2464	f	f	2018-11-03	2021-09-16 09:15:51.765402+00	2125	f	t	t
3604	13528	Diesel	C	546	t	f	2018-11-01	2021-09-16 09:15:51.772408+00	2126	f	f	t
3605	13529	Diesel	C	676	t	f	2018-11-24	2021-09-16 09:15:51.778433+00	2127	t	t	f
3606	13530	Diesel	C	1487	t	f	2018-11-02	2021-09-16 09:15:51.785076+00	2128	t	f	f
3607	13531	Diesel	C	2296	f	f	2018-11-01	2021-09-16 09:15:51.791873+00	2129	f	t	f
3608	13532	Diesel	C	2365	f	f	2018-11-19	2021-09-16 09:15:51.798469+00	2130	f	f	t
3609	13533	Diesel	C	831	f	t	2018-11-10	2021-09-16 09:15:51.804755+00	2131	f	t	f
3610	13534	Diesel	C	2295	t	f	2018-11-12	2021-09-16 09:15:51.812716+00	2132	t	f	t
3611	13535	Diesel	C	1709	t	t	2018-11-02	2021-09-16 09:15:51.819555+00	2133	t	f	t
3612	13536	Diesel	C	2464	t	f	2018-11-18	2021-09-16 09:15:51.825737+00	2134	t	t	t
3613	13537	Diesel	C	1827	t	f	2018-11-21	2021-09-16 09:15:51.831939+00	2135	t	t	t
3614	13538	Diesel	C	1284	t	t	2018-11-06	2021-09-16 09:15:51.83797+00	2136	f	f	f
3615	13539	Diesel	C	1936	t	f	2018-11-03	2021-09-16 09:15:51.843987+00	2137	f	f	t
3616	13540	Diesel	C	1331	t	f	2018-11-20	2021-09-16 09:15:51.853206+00	2138	t	f	f
3617	13541	Diesel	C	2221	f	f	2018-11-04	2021-09-16 09:15:51.85918+00	2139	f	t	f
3618	13542	Diesel	C	1034	f	t	2018-11-02	2021-09-16 09:15:51.866149+00	2140	f	t	f
3619	13543	Diesel	C	637	f	f	2018-11-08	2021-09-16 09:15:51.872372+00	2141	t	f	t
3620	13544	Diesel	C	1579	t	f	2018-11-10	2021-09-16 09:15:51.878962+00	2142	t	t	t
\.


--
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: narayanac
--

COPY public.auth_group (id, name) FROM stdin;
\.


--
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: public; Owner: narayanac
--

COPY public.auth_group_permissions (id, group_id, permission_id) FROM stdin;
\.


--
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: narayanac
--

COPY public.auth_permission (id, name, content_type_id, codename) FROM stdin;
1	Can add log entry	1	add_logentry
2	Can change log entry	1	change_logentry
3	Can delete log entry	1	delete_logentry
4	Can view log entry	1	view_logentry
5	Can add permission	2	add_permission
6	Can change permission	2	change_permission
7	Can delete permission	2	delete_permission
8	Can view permission	2	view_permission
9	Can add group	3	add_group
10	Can change group	3	change_group
11	Can delete group	3	delete_group
12	Can view group	3	view_group
13	Can add user	4	add_user
14	Can change user	4	change_user
15	Can delete user	4	delete_user
16	Can view user	4	view_user
17	Can add content type	5	add_contenttype
18	Can change content type	5	change_contenttype
19	Can delete content type	5	delete_contenttype
20	Can view content type	5	view_contenttype
21	Can add session	6	add_session
22	Can change session	6	change_session
23	Can delete session	6	delete_session
24	Can view session	6	view_session
25	Can add customer	7	add_customer
26	Can change customer	7	change_customer
27	Can delete customer	7	delete_customer
28	Can view customer	7	view_customer
29	Can add customer insurance data	8	add_customerinsurancedata
30	Can change customer insurance data	8	change_customerinsurancedata
31	Can delete customer insurance data	8	delete_customerinsurancedata
32	Can view customer insurance data	8	view_customerinsurancedata
\.


--
-- Data for Name: auth_user; Type: TABLE DATA; Schema: public; Owner: narayanac
--

COPY public.auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) FROM stdin;
1	pbkdf2_sha256$260000$AvbDUho5H4vLmYXEqREYuQ$IEiCx5uL15qFB6/mqRxLQPYww6bYg8/eHDuHyoUNmw4=	2021-09-15 20:05:30.384823+00	t	narayanac				t	t	2021-09-14 21:57:42.038206+00
\.


--
-- Data for Name: auth_user_groups; Type: TABLE DATA; Schema: public; Owner: narayanac
--

COPY public.auth_user_groups (id, user_id, group_id) FROM stdin;
\.


--
-- Data for Name: auth_user_user_permissions; Type: TABLE DATA; Schema: public; Owner: narayanac
--

COPY public.auth_user_user_permissions (id, user_id, permission_id) FROM stdin;
\.


--
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: public; Owner: narayanac
--

COPY public.django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM stdin;
1	2021-09-15 20:06:48.58574+00	1	Customer object (1)	1	[{"added": {}}]	7	1
2	2021-09-15 20:07:07.62022+00	2	Customer object (2)	1	[{"added": {}}]	7	1
3	2021-09-15 20:29:14.440062+00	1	12345 - 0011	1	[{"added": {}}]	8	1
4	2021-09-15 20:29:56.568105+00	2	11223 - 1122	1	[{"added": {}}]	8	1
5	2021-09-16 08:56:58.884828+00	6	403	3		7	1
6	2021-09-16 08:56:58.887687+00	5	402	3		7	1
7	2021-09-16 08:56:58.889891+00	4	401	3		7	1
8	2021-09-16 08:56:58.892463+00	3	400	3		7	1
9	2021-09-16 09:08:38.216772+00	1	12345 - 0011	3		8	1
10	2021-09-16 09:08:38.221054+00	2	11223 - 1122	3		8	1
11	2021-09-16 09:08:38.223107+00	21	400 - 12345	3		8	1
12	2021-09-16 09:08:38.226123+00	22	401 - 12346	3		8	1
13	2021-09-16 09:08:38.230569+00	23	402 - 12347	3		8	1
14	2021-09-16 09:08:38.233742+00	24	403 - 12348	3		8	1
15	2021-09-16 09:08:38.236461+00	25	404 - 12349	3		8	1
16	2021-09-16 09:08:38.239026+00	26	405 - 12350	3		8	1
17	2021-09-16 09:08:38.241489+00	27	406 - 12351	3		8	1
18	2021-09-16 09:08:38.243621+00	28	407 - 12352	3		8	1
19	2021-09-16 09:08:38.246249+00	29	408 - 12353	3		8	1
20	2021-09-16 09:08:38.248954+00	30	409 - 12354	3		8	1
21	2021-09-16 09:08:38.251096+00	31	410 - 12355	3		8	1
22	2021-09-16 09:08:38.253303+00	32	411 - 12356	3		8	1
23	2021-09-16 09:08:38.256134+00	33	412 - 12357	3		8	1
24	2021-09-16 09:08:38.260665+00	34	413 - 12358	3		8	1
25	2021-09-16 09:08:38.264825+00	35	414 - 12359	3		8	1
26	2021-09-16 09:08:38.267277+00	36	415 - 12360	3		8	1
27	2021-09-16 09:08:38.26958+00	37	416 - 12361	3		8	1
28	2021-09-16 09:08:38.271578+00	38	417 - 12362	3		8	1
29	2021-09-16 09:08:38.273718+00	39	418 - 420	3		8	1
30	2021-09-16 09:08:38.275669+00	40	419 - 12364	3		8	1
31	2021-09-16 09:08:38.277513+00	41	420 - 12365	3		8	1
32	2021-09-16 09:08:38.27926+00	42	420 - 12366	3		8	1
33	2021-09-16 09:08:38.281075+00	43	420 - 12367	3		8	1
34	2021-09-16 09:08:38.28288+00	44	420 - 12368	3		8	1
35	2021-09-16 09:08:38.284862+00	45	420 - 12369	3		8	1
36	2021-09-16 09:08:38.286778+00	46	420 - 12370	3		8	1
37	2021-09-16 09:08:38.288551+00	47	420 - 12371	3		8	1
38	2021-09-16 09:08:38.290408+00	48	420 - 12372	3		8	1
39	2021-09-16 09:08:38.292423+00	49	420 - 12373	3		8	1
40	2021-09-16 09:08:38.294628+00	50	420 - 12374	3		8	1
41	2021-09-16 09:08:38.296736+00	51	420 - 12375	3		8	1
42	2021-09-16 09:08:38.299011+00	52	420 - 840	3		8	1
43	2021-09-16 09:08:38.301071+00	53	420 - 12377	3		8	1
44	2021-09-16 09:08:38.303399+00	54	420 - 12378	3		8	1
45	2021-09-16 09:08:38.308666+00	55	420 - 12379	3		8	1
46	2021-09-16 09:08:38.310826+00	56	420 - 12380	3		8	1
47	2021-09-16 09:08:38.313227+00	57	420 - 12381	3		8	1
48	2021-09-16 09:08:38.31603+00	58	420 - 12382	3		8	1
49	2021-09-16 09:08:38.320417+00	59	420 - 12383	3		8	1
50	2021-09-16 09:08:38.325695+00	60	420 - 12384	3		8	1
51	2021-09-16 09:08:38.329384+00	61	420 - 12385	3		8	1
52	2021-09-16 09:08:38.333205+00	62	420 - 12386	3		8	1
53	2021-09-16 09:08:38.337122+00	63	420 - 12387	3		8	1
54	2021-09-16 09:08:38.34087+00	64	420 - 12388	3		8	1
55	2021-09-16 09:08:38.343957+00	65	420 - 12389	3		8	1
56	2021-09-16 09:08:38.349382+00	66	420 - 12390	3		8	1
57	2021-09-16 09:08:38.352004+00	67	420 - 12391	3		8	1
58	2021-09-16 09:08:38.35468+00	68	420 - 12392	3		8	1
59	2021-09-16 09:08:38.357413+00	69	420 - 12393	3		8	1
60	2021-09-16 09:08:38.359872+00	70	420 - 12394	3		8	1
61	2021-09-16 09:08:38.362696+00	71	420 - 12395	3		8	1
62	2021-09-16 09:08:38.36527+00	72	420 - 12396	3		8	1
63	2021-09-16 09:08:38.367595+00	73	420 - 12397	3		8	1
64	2021-09-16 09:08:38.369827+00	74	420 - 12398	3		8	1
65	2021-09-16 09:08:38.371992+00	75	420 - 12399	3		8	1
66	2021-09-16 09:08:38.374441+00	76	420 - 12400	3		8	1
67	2021-09-16 09:08:38.377269+00	77	420 - 12401	3		8	1
68	2021-09-16 09:08:38.379834+00	78	420 - 12402	3		8	1
69	2021-09-16 09:08:38.382401+00	79	420 - 12403	3		8	1
70	2021-09-16 09:08:38.384944+00	80	420 - 12404	3		8	1
71	2021-09-16 09:08:38.38768+00	81	420 - 12405	3		8	1
72	2021-09-16 09:08:38.390579+00	82	420 - 12406	3		8	1
73	2021-09-16 09:08:38.392844+00	83	420 - 12407	3		8	1
74	2021-09-16 09:08:38.395016+00	84	420 - 12408	3		8	1
75	2021-09-16 09:08:38.397541+00	85	420 - 12409	3		8	1
76	2021-09-16 09:08:38.399876+00	86	420 - 12410	3		8	1
77	2021-09-16 09:08:38.402164+00	87	420 - 12411	3		8	1
78	2021-09-16 09:08:38.404443+00	88	420 - 12412	3		8	1
79	2021-09-16 09:08:38.407026+00	89	420 - 12413	3		8	1
80	2021-09-16 09:08:38.409819+00	90	420 - 12414	3		8	1
81	2021-09-16 09:08:38.412996+00	91	420 - 12415	3		8	1
82	2021-09-16 09:08:38.415881+00	92	420 - 12416	3		8	1
83	2021-09-16 09:08:38.418181+00	93	420 - 12417	3		8	1
84	2021-09-16 09:08:38.420707+00	94	420 - 12418	3		8	1
85	2021-09-16 09:08:38.424647+00	95	420 - 12419	3		8	1
86	2021-09-16 09:08:38.42816+00	96	420 - 12420	3		8	1
87	2021-09-16 09:08:38.431608+00	97	420 - 12421	3		8	1
88	2021-09-16 09:08:38.435189+00	98	420 - 12422	3		8	1
89	2021-09-16 09:08:38.441765+00	99	420 - 12423	3		8	1
90	2021-09-16 09:08:38.445706+00	100	420 - 12424	3		8	1
91	2021-09-16 09:08:38.44983+00	101	420 - 12425	3		8	1
92	2021-09-16 09:08:38.454046+00	102	420 - 12426	3		8	1
93	2021-09-16 09:08:38.458774+00	103	420 - 12427	3		8	1
94	2021-09-16 09:08:38.462522+00	104	420 - 12428	3		8	1
95	2021-09-16 09:08:38.466445+00	105	420 - 12429	3		8	1
96	2021-09-16 09:08:38.469079+00	106	420 - 12430	3		8	1
97	2021-09-16 09:08:38.471652+00	107	420 - 12431	3		8	1
98	2021-09-16 09:08:38.474194+00	108	420 - 12432	3		8	1
99	2021-09-16 09:08:38.476592+00	109	420 - 12433	3		8	1
100	2021-09-16 09:08:38.47927+00	110	420 - 12434	3		8	1
101	2021-09-16 09:08:38.481597+00	111	420 - 12435	3		8	1
102	2021-09-16 09:08:38.483623+00	112	420 - 12436	3		8	1
103	2021-09-16 09:08:38.489639+00	113	420 - 12437	3		8	1
104	2021-09-16 09:08:38.491794+00	114	420 - 12438	3		8	1
105	2021-09-16 09:08:38.493965+00	115	420 - 12439	3		8	1
106	2021-09-16 09:08:38.49612+00	116	420 - 12440	3		8	1
107	2021-09-16 09:08:38.498371+00	117	420 - 12441	3		8	1
108	2021-09-16 09:08:38.501159+00	118	420 - 12442	3		8	1
109	2021-09-16 09:08:58.186229+00	119	420 - 12443	3		8	1
110	2021-09-16 09:08:58.190199+00	120	420 - 12444	3		8	1
111	2021-09-16 09:08:58.192312+00	121	420 - 12445	3		8	1
112	2021-09-16 09:08:58.194613+00	122	420 - 12446	3		8	1
113	2021-09-16 09:08:58.196777+00	123	420 - 12447	3		8	1
114	2021-09-16 09:08:58.199056+00	124	420 - 12448	3		8	1
115	2021-09-16 09:08:58.201365+00	125	420 - 12449	3		8	1
116	2021-09-16 09:08:58.203665+00	126	420 - 12450	3		8	1
117	2021-09-16 09:08:58.205515+00	127	420 - 12451	3		8	1
118	2021-09-16 09:08:58.207267+00	128	420 - 12452	3		8	1
119	2021-09-16 09:08:58.209278+00	129	420 - 12453	3		8	1
120	2021-09-16 09:08:58.211131+00	130	420 - 12454	3		8	1
121	2021-09-16 09:08:58.21312+00	131	420 - 12455	3		8	1
122	2021-09-16 09:08:58.21512+00	132	420 - 12456	3		8	1
123	2021-09-16 09:08:58.21713+00	133	420 - 12457	3		8	1
124	2021-09-16 09:08:58.218914+00	134	420 - 12458	3		8	1
125	2021-09-16 09:08:58.220581+00	135	420 - 12459	3		8	1
126	2021-09-16 09:08:58.22235+00	136	420 - 12460	3		8	1
127	2021-09-16 09:08:58.224161+00	137	420 - 12461	3		8	1
128	2021-09-16 09:08:58.226055+00	138	420 - 12462	3		8	1
129	2021-09-16 09:08:58.228151+00	139	420 - 12463	3		8	1
130	2021-09-16 09:08:58.230204+00	140	420 - 12464	3		8	1
131	2021-09-16 09:08:58.23205+00	141	420 - 12465	3		8	1
132	2021-09-16 09:08:58.234185+00	142	420 - 12466	3		8	1
133	2021-09-16 09:08:58.236098+00	143	420 - 12467	3		8	1
134	2021-09-16 09:08:58.23847+00	144	420 - 12468	3		8	1
135	2021-09-16 09:08:58.240797+00	145	420 - 12469	3		8	1
136	2021-09-16 09:08:58.243239+00	146	420 - 12470	3		8	1
137	2021-09-16 09:08:58.244977+00	147	420 - 12471	3		8	1
138	2021-09-16 09:08:58.247279+00	148	420 - 12472	3		8	1
139	2021-09-16 09:08:58.249557+00	149	420 - 12473	3		8	1
140	2021-09-16 09:08:58.251486+00	150	420 - 12474	3		8	1
141	2021-09-16 09:08:58.253672+00	151	420 - 12475	3		8	1
142	2021-09-16 09:08:58.255831+00	152	420 - 12476	3		8	1
143	2021-09-16 09:08:58.258041+00	153	420 - 12477	3		8	1
144	2021-09-16 09:08:58.260114+00	154	420 - 12478	3		8	1
145	2021-09-16 09:08:58.26211+00	155	420 - 12479	3		8	1
146	2021-09-16 09:08:58.264542+00	156	420 - 12480	3		8	1
147	2021-09-16 09:08:58.266983+00	157	420 - 12481	3		8	1
148	2021-09-16 09:08:58.269188+00	158	420 - 12482	3		8	1
149	2021-09-16 09:08:58.2711+00	159	420 - 12483	3		8	1
150	2021-09-16 09:08:58.273249+00	160	420 - 12484	3		8	1
151	2021-09-16 09:08:58.276666+00	161	420 - 12485	3		8	1
152	2021-09-16 09:08:58.27975+00	162	420 - 12486	3		8	1
153	2021-09-16 09:08:58.282763+00	163	420 - 12487	3		8	1
154	2021-09-16 09:08:58.286214+00	164	420 - 12488	3		8	1
155	2021-09-16 09:08:58.289381+00	165	420 - 12489	3		8	1
156	2021-09-16 09:08:58.292621+00	166	420 - 12490	3		8	1
157	2021-09-16 09:08:58.296097+00	167	420 - 12491	3		8	1
158	2021-09-16 09:08:58.299768+00	168	420 - 12492	3		8	1
159	2021-09-16 09:08:58.303506+00	169	420 - 12493	3		8	1
160	2021-09-16 09:08:58.307263+00	170	420 - 12494	3		8	1
161	2021-09-16 09:08:58.312496+00	171	420 - 12495	3		8	1
162	2021-09-16 09:08:58.315831+00	172	420 - 12496	3		8	1
163	2021-09-16 09:08:58.318816+00	173	420 - 12497	3		8	1
164	2021-09-16 09:08:58.321274+00	174	420 - 12498	3		8	1
165	2021-09-16 09:08:58.32366+00	175	420 - 12499	3		8	1
166	2021-09-16 09:08:58.325797+00	176	420 - 12500	3		8	1
167	2021-09-16 09:08:58.329052+00	177	420 - 12501	3		8	1
168	2021-09-16 09:08:58.332623+00	178	420 - 12502	3		8	1
169	2021-09-16 09:08:58.335363+00	179	420 - 12503	3		8	1
170	2021-09-16 09:08:58.337724+00	180	420 - 12504	3		8	1
171	2021-09-16 09:08:58.339647+00	181	420 - 12505	3		8	1
172	2021-09-16 09:08:58.341666+00	182	420 - 12506	3		8	1
173	2021-09-16 09:08:58.344066+00	183	420 - 12507	3		8	1
174	2021-09-16 09:08:58.346467+00	184	420 - 12508	3		8	1
175	2021-09-16 09:08:58.348766+00	185	420 - 12509	3		8	1
176	2021-09-16 09:08:58.351209+00	186	420 - 12510	3		8	1
177	2021-09-16 09:08:58.353536+00	187	420 - 12511	3		8	1
178	2021-09-16 09:08:58.35603+00	188	420 - 12512	3		8	1
179	2021-09-16 09:08:58.358606+00	189	420 - 12513	3		8	1
180	2021-09-16 09:08:58.361022+00	190	420 - 12514	3		8	1
181	2021-09-16 09:08:58.363365+00	191	420 - 12515	3		8	1
182	2021-09-16 09:08:58.365889+00	192	420 - 12516	3		8	1
183	2021-09-16 09:08:58.368319+00	193	420 - 12517	3		8	1
184	2021-09-16 09:08:58.371506+00	194	420 - 12518	3		8	1
185	2021-09-16 09:08:58.374326+00	195	420 - 12519	3		8	1
186	2021-09-16 09:08:58.377223+00	196	420 - 12520	3		8	1
187	2021-09-16 09:08:58.379835+00	197	420 - 12521	3		8	1
188	2021-09-16 09:08:58.382392+00	198	420 - 12522	3		8	1
189	2021-09-16 09:08:58.38471+00	199	420 - 12523	3		8	1
190	2021-09-16 09:08:58.386994+00	200	420 - 12524	3		8	1
191	2021-09-16 09:08:58.389414+00	201	420 - 12525	3		8	1
192	2021-09-16 09:08:58.391627+00	202	420 - 12526	3		8	1
193	2021-09-16 09:08:58.394247+00	203	420 - 12527	3		8	1
194	2021-09-16 09:08:58.39633+00	204	420 - 12528	3		8	1
195	2021-09-16 09:08:58.398799+00	205	420 - 12529	3		8	1
196	2021-09-16 09:08:58.400764+00	206	420 - 12530	3		8	1
197	2021-09-16 09:08:58.402909+00	207	420 - 12531	3		8	1
198	2021-09-16 09:08:58.406543+00	208	420 - 12532	3		8	1
199	2021-09-16 09:08:58.410137+00	209	420 - 12533	3		8	1
200	2021-09-16 09:08:58.414423+00	210	420 - 12534	3		8	1
201	2021-09-16 09:08:58.418808+00	211	420 - 12535	3		8	1
202	2021-09-16 09:08:58.423053+00	212	420 - 12536	3		8	1
203	2021-09-16 09:08:58.428742+00	213	420 - 12537	3		8	1
204	2021-09-16 09:08:58.432466+00	214	420 - 12538	3		8	1
205	2021-09-16 09:08:58.435584+00	215	420 - 12539	3		8	1
206	2021-09-16 09:08:58.438688+00	216	420 - 12540	3		8	1
207	2021-09-16 09:08:58.441187+00	217	420 - 12541	3		8	1
208	2021-09-16 09:08:58.443926+00	218	420 - 12542	3		8	1
209	2021-09-16 09:09:13.11311+00	219	420 - 12543	3		8	1
210	2021-09-16 09:09:13.117405+00	220	420 - 12544	3		8	1
211	2021-09-16 09:09:13.119598+00	221	420 - 12545	3		8	1
212	2021-09-16 09:09:13.121798+00	222	420 - 12546	3		8	1
213	2021-09-16 09:09:13.123894+00	223	420 - 12547	3		8	1
214	2021-09-16 09:09:13.126401+00	224	420 - 12548	3		8	1
215	2021-09-16 09:09:13.129056+00	225	420 - 12549	3		8	1
216	2021-09-16 09:09:13.131036+00	226	420 - 12550	3		8	1
217	2021-09-16 09:09:13.132929+00	227	420 - 12551	3		8	1
218	2021-09-16 09:09:13.134903+00	228	420 - 12552	3		8	1
219	2021-09-16 09:09:13.137029+00	229	420 - 12553	3		8	1
220	2021-09-16 09:09:13.138799+00	230	420 - 12554	3		8	1
221	2021-09-16 09:09:13.140789+00	231	420 - 12555	3		8	1
222	2021-09-16 09:09:13.143274+00	232	420 - 12556	3		8	1
223	2021-09-16 09:09:13.145415+00	233	420 - 12557	3		8	1
224	2021-09-16 09:09:13.147162+00	234	420 - 12558	3		8	1
225	2021-09-16 09:09:13.149109+00	235	420 - 12559	3		8	1
226	2021-09-16 09:09:13.151008+00	236	420 - 12560	3		8	1
227	2021-09-16 09:09:13.153072+00	237	420 - 12561	3		8	1
228	2021-09-16 09:09:13.155027+00	238	420 - 12562	3		8	1
229	2021-09-16 09:09:13.157062+00	239	420 - 12563	3		8	1
230	2021-09-16 09:09:13.159114+00	240	420 - 12564	3		8	1
231	2021-09-16 09:09:13.161059+00	241	420 - 12565	3		8	1
232	2021-09-16 09:09:13.163211+00	242	420 - 12566	3		8	1
233	2021-09-16 09:09:13.166075+00	243	420 - 12567	3		8	1
234	2021-09-16 09:09:13.168255+00	244	420 - 12568	3		8	1
235	2021-09-16 09:09:13.170266+00	245	420 - 12569	3		8	1
236	2021-09-16 09:09:13.172135+00	246	420 - 12570	3		8	1
237	2021-09-16 09:09:13.174142+00	247	420 - 12571	3		8	1
238	2021-09-16 09:09:13.17617+00	248	420 - 12572	3		8	1
239	2021-09-16 09:09:13.178979+00	249	420 - 12573	3		8	1
240	2021-09-16 09:09:13.181615+00	250	420 - 12574	3		8	1
241	2021-09-16 09:09:13.185218+00	251	420 - 12575	3		8	1
242	2021-09-16 09:09:13.188307+00	252	420 - 12576	3		8	1
243	2021-09-16 09:09:13.191421+00	253	420 - 12577	3		8	1
244	2021-09-16 09:09:13.19512+00	254	420 - 12578	3		8	1
245	2021-09-16 09:09:13.198761+00	255	420 - 12579	3		8	1
246	2021-09-16 09:09:13.202475+00	256	420 - 12580	3		8	1
247	2021-09-16 09:09:13.206289+00	257	420 - 12581	3		8	1
248	2021-09-16 09:09:13.210762+00	258	420 - 12582	3		8	1
249	2021-09-16 09:09:13.215968+00	259	420 - 12583	3		8	1
250	2021-09-16 09:09:13.220435+00	260	420 - 12584	3		8	1
251	2021-09-16 09:09:13.22339+00	261	420 - 12585	3		8	1
252	2021-09-16 09:09:13.226583+00	262	420 - 12586	3		8	1
253	2021-09-16 09:09:13.229202+00	263	420 - 12587	3		8	1
254	2021-09-16 09:09:13.232185+00	264	420 - 12588	3		8	1
255	2021-09-16 09:09:13.234472+00	265	420 - 12589	3		8	1
256	2021-09-16 09:09:13.236495+00	266	420 - 12590	3		8	1
257	2021-09-16 09:09:13.238461+00	267	420 - 12591	3		8	1
258	2021-09-16 09:09:13.24052+00	268	420 - 12592	3		8	1
259	2021-09-16 09:09:13.242424+00	269	420 - 12593	3		8	1
260	2021-09-16 09:09:13.244515+00	270	420 - 12594	3		8	1
261	2021-09-16 09:09:13.246569+00	271	420 - 12595	3		8	1
262	2021-09-16 09:09:13.248967+00	272	420 - 12596	3		8	1
263	2021-09-16 09:09:13.251331+00	273	420 - 12597	3		8	1
264	2021-09-16 09:09:13.253464+00	274	420 - 12598	3		8	1
265	2021-09-16 09:09:13.255883+00	275	420 - 12599	3		8	1
266	2021-09-16 09:09:13.258017+00	276	420 - 12600	3		8	1
267	2021-09-16 09:09:13.260465+00	277	420 - 12601	3		8	1
268	2021-09-16 09:09:13.262598+00	278	420 - 12602	3		8	1
269	2021-09-16 09:09:13.264723+00	279	420 - 12603	3		8	1
270	2021-09-16 09:09:13.266856+00	280	420 - 12604	3		8	1
271	2021-09-16 09:09:13.268772+00	281	420 - 12605	3		8	1
272	2021-09-16 09:09:13.27066+00	282	420 - 12606	3		8	1
273	2021-09-16 09:09:13.272578+00	283	420 - 12607	3		8	1
274	2021-09-16 09:09:13.274486+00	284	420 - 12608	3		8	1
275	2021-09-16 09:09:13.27636+00	285	420 - 12609	3		8	1
276	2021-09-16 09:09:13.278406+00	286	420 - 12610	3		8	1
277	2021-09-16 09:09:13.280615+00	287	420 - 12611	3		8	1
278	2021-09-16 09:09:13.282685+00	288	420 - 12612	3		8	1
279	2021-09-16 09:09:13.285007+00	289	420 - 12613	3		8	1
280	2021-09-16 09:09:13.287317+00	290	420 - 12614	3		8	1
281	2021-09-16 09:09:13.289755+00	291	420 - 12615	3		8	1
282	2021-09-16 09:09:13.292039+00	292	420 - 12616	3		8	1
283	2021-09-16 09:09:13.294471+00	293	420 - 12617	3		8	1
284	2021-09-16 09:09:13.296407+00	294	420 - 12618	3		8	1
285	2021-09-16 09:09:13.298587+00	295	420 - 12619	3		8	1
286	2021-09-16 09:09:13.300842+00	296	420 - 12620	3		8	1
287	2021-09-16 09:09:13.302812+00	297	420 - 12621	3		8	1
288	2021-09-16 09:09:13.305014+00	298	420 - 12622	3		8	1
289	2021-09-16 09:09:13.307261+00	299	420 - 12623	3		8	1
290	2021-09-16 09:09:13.309443+00	300	420 - 12624	3		8	1
291	2021-09-16 09:09:13.311638+00	301	420 - 12625	3		8	1
292	2021-09-16 09:09:13.313941+00	302	420 - 12626	3		8	1
293	2021-09-16 09:09:13.316285+00	303	420 - 12627	3		8	1
294	2021-09-16 09:09:13.31868+00	304	420 - 12628	3		8	1
295	2021-09-16 09:09:13.320758+00	305	420 - 12629	3		8	1
296	2021-09-16 09:09:13.322613+00	306	420 - 12630	3		8	1
297	2021-09-16 09:09:13.32437+00	307	420 - 12631	3		8	1
298	2021-09-16 09:09:13.326281+00	308	420 - 12632	3		8	1
299	2021-09-16 09:09:13.328345+00	309	420 - 12633	3		8	1
300	2021-09-16 09:09:13.330154+00	310	420 - 12634	3		8	1
301	2021-09-16 09:09:13.331908+00	311	420 - 12635	3		8	1
302	2021-09-16 09:09:13.3336+00	312	420 - 12636	3		8	1
303	2021-09-16 09:09:13.335549+00	313	420 - 12637	3		8	1
304	2021-09-16 09:09:13.337391+00	314	420 - 12638	3		8	1
305	2021-09-16 09:09:13.339147+00	315	420 - 12639	3		8	1
306	2021-09-16 09:09:13.340856+00	316	420 - 12640	3		8	1
307	2021-09-16 09:09:13.342711+00	317	420 - 12641	3		8	1
308	2021-09-16 09:09:13.34432+00	318	420 - 12642	3		8	1
\.


--
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: public; Owner: narayanac
--

COPY public.django_content_type (id, app_label, model) FROM stdin;
1	admin	logentry
2	auth	permission
3	auth	group
4	auth	user
5	contenttypes	contenttype
6	sessions	session
7	api	customer
8	api	customerinsurancedata
\.


--
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: public; Owner: narayanac
--

COPY public.django_migrations (id, app, name, applied) FROM stdin;
1	contenttypes	0001_initial	2021-09-14 21:29:32.436845+00
2	auth	0001_initial	2021-09-14 21:29:32.565628+00
3	admin	0001_initial	2021-09-14 21:29:32.608838+00
4	admin	0002_logentry_remove_auto_add	2021-09-14 21:29:32.618034+00
5	admin	0003_logentry_add_action_flag_choices	2021-09-14 21:29:32.641882+00
6	contenttypes	0002_remove_content_type_name	2021-09-14 21:29:32.663364+00
7	auth	0002_alter_permission_name_max_length	2021-09-14 21:29:32.676401+00
8	auth	0003_alter_user_email_max_length	2021-09-14 21:29:32.68554+00
9	auth	0004_alter_user_username_opts	2021-09-14 21:29:32.696566+00
10	auth	0005_alter_user_last_login_null	2021-09-14 21:29:32.70568+00
11	auth	0006_require_contenttypes_0002	2021-09-14 21:29:32.70861+00
12	auth	0007_alter_validators_add_error_messages	2021-09-14 21:29:32.716754+00
13	auth	0008_alter_user_username_max_length	2021-09-14 21:29:32.732493+00
14	auth	0009_alter_user_last_name_max_length	2021-09-14 21:29:32.741682+00
15	auth	0010_alter_group_name_max_length	2021-09-14 21:29:32.751687+00
16	auth	0011_update_proxy_permissions	2021-09-14 21:29:32.764263+00
17	auth	0012_alter_user_first_name_max_length	2021-09-14 21:29:32.773393+00
18	sessions	0001_initial	2021-09-14 21:29:32.793437+00
19	api	0001_initial	2021-09-14 23:10:05.407047+00
20	api	0002_customerinsurancedata	2021-09-14 23:19:04.100618+00
21	api	0003_alter_customerinsurancedata_purchase_date	2021-09-14 23:21:51.494557+00
22	api	0004_auto_20210916_0853	2021-09-16 08:54:02.055492+00
23	api	0005_remove_customerinsurancedata_slug	2021-09-16 08:55:06.964116+00
24	api	0006_auto_20210916_0905	2021-09-16 09:05:09.052408+00
\.


--
-- Data for Name: django_session; Type: TABLE DATA; Schema: public; Owner: narayanac
--

COPY public.django_session (session_key, session_data, expire_date) FROM stdin;
l90lhflne20duketpywmu0xfgaadjd6a	.eJxVjDsOwyAQRO9CHSEw5pcyvc-AFnYJTiKQjF1FuXtsyUVSzrw382YBtrWErdMSZmRXJtnlt4uQnlQPgA-o98ZTq-syR34o_KSdTw3pdTvdv4MCvexrK1KOIlsaTDQRFeRRkdF78KidFKPGwRktsorkKVtBUktpXSKwyhvLPl_seTez:1mQb9u:46tOggk_-Mv7OoAARD_PxbfVZLSXNGk2iqCSnxJOsTc	2021-09-29 20:05:30.387752+00
\.


--
-- Name: api_customer_id_seq; Type: SEQUENCE SET; Schema: public; Owner: narayanac
--

SELECT pg_catalog.setval('public.api_customer_id_seq', 2142, true);


--
-- Name: api_customerinsurancedata_id_seq; Type: SEQUENCE SET; Schema: public; Owner: narayanac
--

SELECT pg_catalog.setval('public.api_customerinsurancedata_id_seq', 3620, true);


--
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: narayanac
--

SELECT pg_catalog.setval('public.auth_group_id_seq', 1, false);


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: narayanac
--

SELECT pg_catalog.setval('public.auth_group_permissions_id_seq', 1, false);


--
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: narayanac
--

SELECT pg_catalog.setval('public.auth_permission_id_seq', 32, true);


--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: narayanac
--

SELECT pg_catalog.setval('public.auth_user_groups_id_seq', 1, false);


--
-- Name: auth_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: narayanac
--

SELECT pg_catalog.setval('public.auth_user_id_seq', 1, true);


--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: narayanac
--

SELECT pg_catalog.setval('public.auth_user_user_permissions_id_seq', 1, false);


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: narayanac
--

SELECT pg_catalog.setval('public.django_admin_log_id_seq', 308, true);


--
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: narayanac
--

SELECT pg_catalog.setval('public.django_content_type_id_seq', 8, true);


--
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: narayanac
--

SELECT pg_catalog.setval('public.django_migrations_id_seq', 24, true);


--
-- Name: api_customer api_customer_customer_id_key; Type: CONSTRAINT; Schema: public; Owner: narayanac
--

ALTER TABLE ONLY public.api_customer
    ADD CONSTRAINT api_customer_customer_id_key UNIQUE (customer_id);


--
-- Name: api_customer api_customer_pkey; Type: CONSTRAINT; Schema: public; Owner: narayanac
--

ALTER TABLE ONLY public.api_customer
    ADD CONSTRAINT api_customer_pkey PRIMARY KEY (id);


--
-- Name: api_customerinsurancedata api_customerinsurancedata_pkey; Type: CONSTRAINT; Schema: public; Owner: narayanac
--

ALTER TABLE ONLY public.api_customerinsurancedata
    ADD CONSTRAINT api_customerinsurancedata_pkey PRIMARY KEY (id);


--
-- Name: auth_group auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: narayanac
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- Name: auth_group_permissions auth_group_permissions_group_id_permission_id_0cd325b0_uniq; Type: CONSTRAINT; Schema: public; Owner: narayanac
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq UNIQUE (group_id, permission_id);


--
-- Name: auth_group_permissions auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: narayanac
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: narayanac
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_permission auth_permission_content_type_id_codename_01ab375a_uniq; Type: CONSTRAINT; Schema: public; Owner: narayanac
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq UNIQUE (content_type_id, codename);


--
-- Name: auth_permission auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: narayanac
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups auth_user_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: narayanac
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups auth_user_groups_user_id_group_id_94350c0c_uniq; Type: CONSTRAINT; Schema: public; Owner: narayanac
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_group_id_94350c0c_uniq UNIQUE (user_id, group_id);


--
-- Name: auth_user auth_user_pkey; Type: CONSTRAINT; Schema: public; Owner: narayanac
--

ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions auth_user_user_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: narayanac
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions auth_user_user_permissions_user_id_permission_id_14a6b632_uniq; Type: CONSTRAINT; Schema: public; Owner: narayanac
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_permission_id_14a6b632_uniq UNIQUE (user_id, permission_id);


--
-- Name: auth_user auth_user_username_key; Type: CONSTRAINT; Schema: public; Owner: narayanac
--

ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_username_key UNIQUE (username);


--
-- Name: django_admin_log django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: narayanac
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- Name: django_content_type django_content_type_app_label_model_76bd3d3b_uniq; Type: CONSTRAINT; Schema: public; Owner: narayanac
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq UNIQUE (app_label, model);


--
-- Name: django_content_type django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: narayanac
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- Name: django_migrations django_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: narayanac
--

ALTER TABLE ONLY public.django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- Name: django_session django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: narayanac
--

ALTER TABLE ONLY public.django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- Name: api_customer_customer_id_435ac343_like; Type: INDEX; Schema: public; Owner: narayanac
--

CREATE INDEX api_customer_customer_id_435ac343_like ON public.api_customer USING btree (customer_id varchar_pattern_ops);


--
-- Name: api_customerinsurancedata_customer_id_eb57dd0e; Type: INDEX; Schema: public; Owner: narayanac
--

CREATE INDEX api_customerinsurancedata_customer_id_eb57dd0e ON public.api_customerinsurancedata USING btree (customer_id);


--
-- Name: auth_group_name_a6ea08ec_like; Type: INDEX; Schema: public; Owner: narayanac
--

CREATE INDEX auth_group_name_a6ea08ec_like ON public.auth_group USING btree (name varchar_pattern_ops);


--
-- Name: auth_group_permissions_group_id_b120cbf9; Type: INDEX; Schema: public; Owner: narayanac
--

CREATE INDEX auth_group_permissions_group_id_b120cbf9 ON public.auth_group_permissions USING btree (group_id);


--
-- Name: auth_group_permissions_permission_id_84c5c92e; Type: INDEX; Schema: public; Owner: narayanac
--

CREATE INDEX auth_group_permissions_permission_id_84c5c92e ON public.auth_group_permissions USING btree (permission_id);


--
-- Name: auth_permission_content_type_id_2f476e4b; Type: INDEX; Schema: public; Owner: narayanac
--

CREATE INDEX auth_permission_content_type_id_2f476e4b ON public.auth_permission USING btree (content_type_id);


--
-- Name: auth_user_groups_group_id_97559544; Type: INDEX; Schema: public; Owner: narayanac
--

CREATE INDEX auth_user_groups_group_id_97559544 ON public.auth_user_groups USING btree (group_id);


--
-- Name: auth_user_groups_user_id_6a12ed8b; Type: INDEX; Schema: public; Owner: narayanac
--

CREATE INDEX auth_user_groups_user_id_6a12ed8b ON public.auth_user_groups USING btree (user_id);


--
-- Name: auth_user_user_permissions_permission_id_1fbb5f2c; Type: INDEX; Schema: public; Owner: narayanac
--

CREATE INDEX auth_user_user_permissions_permission_id_1fbb5f2c ON public.auth_user_user_permissions USING btree (permission_id);


--
-- Name: auth_user_user_permissions_user_id_a95ead1b; Type: INDEX; Schema: public; Owner: narayanac
--

CREATE INDEX auth_user_user_permissions_user_id_a95ead1b ON public.auth_user_user_permissions USING btree (user_id);


--
-- Name: auth_user_username_6821ab7c_like; Type: INDEX; Schema: public; Owner: narayanac
--

CREATE INDEX auth_user_username_6821ab7c_like ON public.auth_user USING btree (username varchar_pattern_ops);


--
-- Name: django_admin_log_content_type_id_c4bce8eb; Type: INDEX; Schema: public; Owner: narayanac
--

CREATE INDEX django_admin_log_content_type_id_c4bce8eb ON public.django_admin_log USING btree (content_type_id);


--
-- Name: django_admin_log_user_id_c564eba6; Type: INDEX; Schema: public; Owner: narayanac
--

CREATE INDEX django_admin_log_user_id_c564eba6 ON public.django_admin_log USING btree (user_id);


--
-- Name: django_session_expire_date_a5c62663; Type: INDEX; Schema: public; Owner: narayanac
--

CREATE INDEX django_session_expire_date_a5c62663 ON public.django_session USING btree (expire_date);


--
-- Name: django_session_session_key_c0390e0f_like; Type: INDEX; Schema: public; Owner: narayanac
--

CREATE INDEX django_session_session_key_c0390e0f_like ON public.django_session USING btree (session_key varchar_pattern_ops);


--
-- Name: api_customerinsurancedata api_customerinsuranc_customer_id_eb57dd0e_fk_api_custo; Type: FK CONSTRAINT; Schema: public; Owner: narayanac
--

ALTER TABLE ONLY public.api_customerinsurancedata
    ADD CONSTRAINT api_customerinsuranc_customer_id_eb57dd0e_fk_api_custo FOREIGN KEY (customer_id) REFERENCES public.api_customer(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions auth_group_permissio_permission_id_84c5c92e_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: narayanac
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions auth_group_permissions_group_id_b120cbf9_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: narayanac
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_permission auth_permission_content_type_id_2f476e4b_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: narayanac
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups auth_user_groups_group_id_97559544_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: narayanac
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_group_id_97559544_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups auth_user_groups_user_id_6a12ed8b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: narayanac
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_6a12ed8b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: narayanac
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: narayanac
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_content_type_id_c4bce8eb_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: narayanac
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_user_id_c564eba6_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: narayanac
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_c564eba6_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- PostgreSQL database dump complete
--

