import React, { Component } from 'react';
import Paper from '@material-ui/core/Paper';
import {
  Chart,
  BarSeries,
  Title,
  ArgumentAxis,
  ValueAxis,
} from '@devexpress/dx-react-chart-material-ui';
import { Animation } from '@devexpress/dx-react-chart';

import Container from '@material-ui/core/Container';

import axios from 'axios';


class WelcomeChart extends Component {
  constructor(props) {
    super(props);
    this.state = {
      chart_data: [],
    };
  }

  componentDidMount() {
    axios.get(`http://127.0.0.1:8000/api/chartdata`)
      .then(res => {
        console.log(res, 'data')
        const chart_data = res.data;
        this.setState({ chart_data });
      })
  }
  

  render() {
    console.log('here')
    return (
      <Container>
        <Paper>
            <Chart
              data={this.state.chart_data}
            >
              <ArgumentAxis />
              <ValueAxis max={12} />

              <BarSeries
                valueField="policies_purchased"
                argumentField="month"
              />
              <Title text="Policies brought over time" />
              <Animation />
            </Chart>
            
        </Paper>
        </Container>
    );
  }
}

export default WelcomeChart;
