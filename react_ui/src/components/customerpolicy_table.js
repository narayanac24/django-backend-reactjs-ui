import React, { Component } from 'react';
import { DataGrid } from '@material-ui/data-grid';
import Container from '@material-ui/core/Container';

import axios from 'axios';


const columns = [
  { field: 'id', headerName: 'ID', width: 90 },
  {
    field: 'policy_id',
    headerName: 'Policy ID',
    width: 150,
    editable: true,
  },
  {
    field: 'purchase_date',
    headerName: 'Date Of Purchase',
    width: 150,
  },
  {
    field: 'customer_id',
    headerName: 'Customer ID',
    type: 'number',
    width: 110,
    editable: true,
    renderCell: (params) => {
      console.log(params, 'param')
      return <div className="rowitem">{params.row.customer.customer_id}</div>;
    },
  },
  {
    field: 'fuel',
    headerName: 'Fuel Type',
    description: 'This column has a value getter and is not sortable.',
    sortable: false,
    width: 160,

  },
  {
    field: 'vehicle_segment',
    headerName: 'Vehicle Segment',
    width: 110,
    editable: true,
  },
  {
    field: 'premium',
    headerName: 'Premium',
    type: 'number',
    width: 110,
    editable: true,
  },
  {
    field: 'bodily_injury_liability',
    headerName: 'Bodily Injury Liability',
    width: 110,
    editable: true,
  },
  {
    field: 'personal_injury_protection',
    headerName: 'Personal Injury Protection',
    width: 110,
    editable: true,
  },
  {
    field: 'property_damage_liability',
    headerName: 'Property Damage Liability',
    width: 110,
    editable: true,
  },
  {
    field: 'collision',
    headerName: 'Collision',
    width: 110,
    editable: true,
  },
  {
    field: 'comprehensive',
    headerName: 'Comprehensive',
    width: 110,
    editable: true,
  },
  {
    field: 'customer.gender',
    headerName: 'Customer Gender',
    type: 'number',
    width: 110,
    editable: true,
    renderCell: (params) => {
      console.log(params, 'param')
      return <div className="rowitem">{params.row.customer.gender}</div>;
    },
  },
  {
    field: 'customer.income_group',
    headerName: 'Customer Income Group',
    type: 'number',
    width: 110,
    editable: true,
    renderCell: (params) => {
      console.log(params, 'param')
      return <div className="rowitem">{params.row.customer.income_group}</div>;
    },
  },
  {
    field: 'customer.region',
    headerName: 'Customer Region',
    width: 110,
    editable: true,
    renderCell: (params) => {
      console.log(params, 'param')
      return <div className="rowitem">{params.row.customer.region}</div>;
    },
  },
  {
    field: 'customer.marital_status',
    headerName: 'Marital Status',
    type: 'number',
    width: 110,
    editable: true,
    renderCell: (params) => {
      console.log(params, 'param')
      return <div className="rowitem">{params.row.customer.marital_status}</div>;
    },
  },
];

class CustomerPolicy extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
    };
  }

  componentDidMount() {
    axios.get(`http://127.0.0.1:8000/api/policy`)
      .then(res => {
        console.log(res, 'response data')
        const data = res.data;
        this.setState({ data });
      })
  }

  render() {
    return (

      <Container>
        <div>
            <h1>Customer Insurance Policy details</h1>
         </div>
        <div style={{ height: 400, width: '100%' }}>
          <DataGrid
            rows={this.state.data}
            columns={columns}
            pageSize={20}
          />
        </div>
      </Container>
    );
  }
}

export default CustomerPolicy;