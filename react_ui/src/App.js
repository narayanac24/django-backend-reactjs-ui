import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import WelcomeChart from './components/welcome';
import CustomerPolicy from './components/customerpolicy_table';
import { createBrowserHistory } from 'history';

export const history = createBrowserHistory();


class App extends Component {
  render() {
    return (
      <Router history={history}>
        <Switch>
          <div className="App">
            <Route exact path='/' component={WelcomeChart} />
            <Route path='/customer-policy' component={CustomerPolicy} />
          </div>
        </Switch>
      </Router>
    );
  }
}

export default App;