from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'policy$', views.CustomerPolicy.as_view()),
    url(r'chartdata$', views.BarChartData.as_view()),
]
