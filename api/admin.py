from django.contrib import admin
from .models import Customer, CustomerInsuranceData

# Register your models here.
admin.site.register(Customer)
admin.site.register(CustomerInsuranceData)
