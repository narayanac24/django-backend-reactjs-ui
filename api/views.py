from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

from .serializers import CustomerPolicySerializer
from .models import CustomerInsuranceData, Customer

import csv
import datetime


class CustomerPolicy(APIView):
    """
    This class is used for fetching or adding data
    of customer's policy data.
    """

    def get(self, request):
        """
        GET request to fetch.
        """
        data = CustomerInsuranceData.objects.all()
        serialize = CustomerPolicySerializer(data, many=True)
        return Response(serialize.data, status=200)


class BarChartData(APIView):
    """
    This class is used for fetching data for the
    bar chart to show policies taken in a month
    """

    def get(self, request):
        """   
        GET request to fetch.
        """

        data = []
        months = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
        for month in months:
            details = dict()
            details['month'] = month
            details['policies_purchased'] = CustomerInsuranceData.objects.filter(purchase_date__month=month).count()
            data.append(details)
        return Response(data, status=200)


def boolean_validation(data):
    if data == '1':
        return True
    else:
        return False


def import_csv_file():
    
    with open('insurance_data.csv', 'r') as f:
        reader = csv.reader(f)
        your_list = list(reader)

    for i in your_list[1:]:
        print(i)
        date = datetime.datetime.strptime(i[1], '%m/%d/%Y') 
        print(date)
        try:
            customer_data = Customer.objects.get(customer_id=i[2])
        except:
            customer_data = Customer()
            customer_data.gender = i[11]
            customer_data.customer_id = i[2]
            customer_data.region = i[13]
            customer_data.income_group = i[12]
            customer_data.marital_status = boolean_validation(i[14])
            customer_data.save()

        if customer_data:
            customer_policy = CustomerInsuranceData()
            customer_policy.purchase_date = date
            customer_policy.policy_id = i[0]
            customer_policy.fuel = i[3]
            customer_policy.customer = customer_data
            customer_policy.vehicle_segment = i[4]
            customer_policy.premium = i[5]
            customer_policy.bodily_injury_liability = boolean_validation(i[6])
            customer_policy.personal_injury_protection = boolean_validation(i[7])
            customer_policy.property_damage_liability = boolean_validation(i[8])
            customer_policy.collision = boolean_validation(i[9])
            customer_policy.comprehensive = boolean_validation(i[10])
            customer_policy.save()
        
        print(customer_data, customer_policy, 'added')
    
    return 'sucess'



