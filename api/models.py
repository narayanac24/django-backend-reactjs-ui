from django.db import models
from django.template.defaultfilters import slugify
from django.contrib.auth.models import User
from django.urls import reverse


class Customer(models.Model):
    # I am not adding a user ForeignKey connection to the auth_users as we have limited data in the CSV and limited time
    customer_id = models.CharField(max_length=255, unique=True)
    gender_opt = (("Male", "Male"), ("Female", "Female"))
    gender = models.CharField(max_length=255, choices=gender_opt)
    region_opt = (("North", "North"), ("South", "South"), ("East", "East"), ("West", "West"))
    region = models.CharField(max_length=255, choices=region_opt)
    income_opt = (("0-$25K", "0-$25K"), ("$25K-$70k", "$25K-$70k"), (">$70k", ">$70k"))
    income_group = models.CharField(max_length=255, choices=income_opt)
    marital_status = models.BooleanField(default=False)
    created_on = models.DateTimeField(auto_now_add=True)
    

    def __str__(self):
        return self.customer_id





class CustomerInsuranceData(models.Model):
    policy_id = models.CharField(max_length=255)
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE)

    fuel_opt = (("1", "CNG"), ("2", "Petrol"), ("3", "Diesel"))
    fuel = models.CharField(max_length=255, choices=fuel_opt)

    vehicle_segment = models.CharField(max_length=255)
    premium = models.IntegerField()

    bodily_injury_liability = models.BooleanField(default=False)
    personal_injury_protection = models.BooleanField(default=False)
    property_damage_liability = models.BooleanField(default=False)
    collision = models.BooleanField(default=False)
    comprehensive = models.BooleanField(default=False)

    purchase_date = models.DateField()

    created_on = models.DateTimeField(auto_now_add=True)


    def __str__(self):
        return (f"{self.customer.customer_id} - {self.policy_id}")

    class Meta:
        ordering = ['created_on']
