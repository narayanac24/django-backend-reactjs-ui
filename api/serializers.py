from .models import CustomerInsuranceData, Customer
from rest_framework import serializers


class CustomerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Customer
        exclude = ('created_on',)


class CustomerPolicySerializer(serializers.ModelSerializer):
    customer = CustomerSerializer()
    class Meta:
        model = CustomerInsuranceData
        fields = '__all__'
